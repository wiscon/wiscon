### lib/wiscondbpaths.pm
  # Sets up some path information.

### Installation
  # cat >> ~/wisconlocal/modperl.conf 
  # PerlRequire /var/home/simonm/wiscondb/lib/wiscondbpaths.pm
  # Include /var/home/simonm/wiscondb/etc/httpd-modperl.conf

package wiscondbpaths;

my $paths_file = $INC{ ( grep { /wiscondbpaths/ } keys %INC )[0] };

my $lib_dir = $paths_file;
   $lib_dir =~ s{/[^/]+$}{};

my $root_dir = $lib_dir;
   $root_dir =~ s{/[^/]+$}{};

my $local_dir = $root_dir;
   $local_dir =~ s{/[^/]+$}{};
   $local_dir .= '/wisconlocal';

use lib;
lib->import( $lib_dir );

$WisCon::Paths{ install } = $root_dir;
$WisCon::Paths{ perllib } = $lib_dir;
$WisCon::Paths{ webconf } = $local_dir;

1;
