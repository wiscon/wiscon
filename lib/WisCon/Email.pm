package WisCon::Email;

use strict;
use warnings;

use Mail::Mailer;
use Regexp::Common qw[Email::Address];
use Email::Address;
use Net::validMX;    #http://search.cpan.org/~kmcgrail/Net-validMX-2.2.0/lib/Net/validMX.pm

######################################################################
# Eventually move to database or a file?
# This is the list of forwarders as of October 2015
my %forwarders = (
		academic => 'academic@wiscon.net',
		appdev => 'appdev@wiscon.net',
		artshow => 'artshow@wiscon.net',
		dealers => 'dealers@wiscon.net',
		flagged => 'flagged@wiscon.net',
		info => 'communications@sf3.org',
		newsletter => 'newsletter@wiscon.net',
		program => 'panels@wiscon.net',
		party => 'parties@wiscon.net',
		readings => 'readings@wiscon.net',
		registration => 'registration@wiscon.net',
		support => 'support@wiscon.net'	,
);

# Return email address based on type
sub get_forwarder {
	my $type = shift;
	my $forwarder =  $forwarders{$type};
	return $forwarder if $forwarder;
  }

sub validate_email {
    my $email = shift;
    $email =~ /^$RE{Email}{Address}$/ or return;
    my ( $status, $reason, $sanitized ) = Net::validMX::check_email_and_mx( $email );
    $status or return;
    return $sanitized;
  }


# given an input to, from, subject, body and optional cc, sends email
# does not check for banned users or NOMAIL requests
sub send_email {
	my ($to, $from, $subject, $body, $cc) = @_;
	my $mailer = Mail::Mailer->new;
	# Comment the first $to and Uncomment the $to and $bcc in production
	#	  "To"=>$to,
	#     "Bcc"=>'wiscon@piglet.org',

    $mailer->open({
# Comment the following line in production
	  "To"=> 'wiscon@piglet.org',
 # Uncomment the following two lines in production
	  #	  "To"=>$to,
	  #   "Bcc"=>'wiscon@piglet.org',
      "Cc"=>$cc,
      "From"=>$from,
      "Return-Path"=>$forwarders{"support"},
      "Subject"=>$subject
});
    print $mailer $body;
    if ($mailer->close) {
      return 1;
    } else {
      return 0;
    }
}

# accepts an idUsers, finds the appropriate idPersons & calls it
sub send_email_idUsers {
	my $idUsers = shift;
	my $person = WisCon::Person->find_user($idUsers);
	&send_email_idPersons($person->id, @_);
}

# accepts an idPersons, checks to see if that user has been banned before sending the mail
sub send_email_idPersons {
	my $idPersons = shift;
	my $person = WisCon::Person->fetch($idPersons);
	# confirm that person has not been banned
	return "couldn't find idPersons $idPersons" unless $person;
	return $person->admin_name . " has been banned" if $person->is_banned;
	# this would also be a good place to confirm that user has not requested NOMAIL from us
    my $address = $person->email;
	my $sent = send_email($address, @_);
	return "mail sent to $address" if $sent;
	return "mail to $address failed";
}

######################################################################

1;
