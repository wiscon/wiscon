package WisCon::Person;

=pod

A class representing a person (personal profile).  

=cut

use Carp;
use Data::Dumper;
use strict;
use warnings;
use WisCon::Assignment;
use WisCon::Authentication;
use WisCon::DateTime;
use WisCon::ProgramItem;
use WisCon::SQLEngine;
use List::MoreUtils qw(uniq);

#################################################################
### Fields

my $sqldb = undef;

my %cache_by_user_id;
my %cache_by_person_id;

#################################################################
### Constructors and factory methods

=pod

=item ->fetch($person_id)

Factory method; Reads the person with the specified ID from the database.

=cut

sub fetch {
        my $class = shift;
        my ( $person_id ) = @_;

        # TODO __DAVID__ Include user ID in query
        #       my $user_id = WisCon::Authentication::current_user_id();
        #       unless ($user_id) {
        #               die "no user ID";
        #       }

        my $records = sqldb()->fetch_select(
                table => 'Persons',
                where => { idPersons => $person_id }
        );

        my $record = $records->[0];

        return WisCon::Person->new( record => $record );
}

#################################################################
### Utility class methods

sub sqldb {
        my $class = shift;
        if (not $sqldb or (not $sqldb->get_dbh())) {
                $sqldb = WisCon::SQLEngine::sqldb();
        }
        return $sqldb;
}

sub persons_from_records {
    my $class = shift;
    my ( $records ) = @_;
    my @persons = ();

    foreach my $record (@$records) {
        push( @persons, WisCon::Person->new( record => $record ) );
    }

    return @persons;
}


=pod

=item ->find_by_user($user_id)

Factory method; Reads the people with the specified user IDs from the database
and returns a list of Persons. 

=cut

sub find_by_user {
    my $class = shift;
    my ( $user_id ) = @_;

    my $records = sqldb()->fetch_select(
        tables => [ 'Persons', 'UserAuthority' ],
        where  => {
            'Persons.idPersons'    => \'UserAuthority.idPersons',
            'UserAuthority.idUsers' => "$user_id",
            'UserAuthority.AuthorityCode' => 'identity',
        }
    );

    return WisCon::Person->persons_from_records($records);
}

=pod

=item ->get_records_by_keywords($keywordString)

Factory method; Reads the people who match any of the keywords from
the database and returns a list of rows (join on Person and User).

=cut

sub get_records_by_keywords {
    my $class = shift;
    my ($keywords) = @_;

    my $where = "";
    my @like_matches = ( 'Users.ProfileEmail', 'Persons.LastName', 'Persons.FirstName', 'Persons.MiddleName', 'Persons.PublicName', 'Persons.BadgeName', 'Persons.City', 'Persons.PersonEmail', 'Persons.Company' );
    my @exact_matches = ( 'Users.idUsers', 'Persons.idPersons' );
    my @words = split(' ', $keywords);
    my $or_clause;
    # use placeholders to protect against bogus user input
    my @placeholders;
    foreach my $word (@words) {
      $or_clause = '';
      foreach my $field (@like_matches) {
	  $or_clause .= ($or_clause ? ' or ' : '') . qq($field like ?);
	  push @placeholders, "%$word%";
      }
      foreach my $field (@exact_matches) {
	  $or_clause .= ($or_clause ? ' or ' : '') . qq($field = ?);
	  push @placeholders, $word;
      }
      $or_clause = "($or_clause)" if $or_clause;
      $where .= ($where ? ' and ' : '') . $or_clause;
    }
    $where = "where $where" if $where;
    return sqldb()->fetch_select(sql=>["select Persons.*,Users.* from Persons LEFT JOIN UserAuthority on UserAuthority.idPersons=Persons.idPersons and UserAuthority.AuthorityCode='identity' LEFT JOIN Users on Users.idUsers=UserAuthority.idUsers $where order by LastName, FirstName, MiddleName", @placeholders]);
}

=pod

=item ->get_records_by_program_names($keywordString)

Factory method; Reads the people whose program_names match any of the
keywords from the database and returns a list of rows (join on Person
and User).

=cut

sub get_records_by_program_names {
    my $class = shift;
    my ($keywords) = @_;

    my $where = "";
    my @name_matches = ( 'Persons.LastName', 'Persons.FirstName', 'Persons.MiddleName' );
    my @words = split(' ', $keywords);
    my $names_clause;
    # use placeholders to protect against bogus user input
    my @placeholders;
    foreach my $word (@words) {
      $names_clause = '';
      foreach my $field (@name_matches) {
	  $names_clause .= ($names_clause ? ' or ' : '') . qq($field like ?);
	  push @placeholders, "%$word%";
      }
      $names_clause = "($names_clause)" if $names_clause;
      $where .= ($where ? ' and ' : '') . $names_clause;
    }
    my $public_clause;
    foreach my $word (@words) {
      $public_clause .= ($public_clause ? ' and ' : '') . qq(Persons.PublicName like ?);
      push @placeholders, "%$word%";
    }
    $where = "where ($where and (Persons.PublicName='' or Persons.PublicName is null)) or ($public_clause)" if $where;
    my $sql = "select Persons.*,Users.* from Persons LEFT JOIN UserAuthority on UserAuthority.idPersons=Persons.idPersons and UserAuthority.AuthorityCode='identity' LEFT JOIN Users on Users.idUsers=UserAuthority.idUsers $where order by LastName, FirstName, MiddleName";
    return sqldb()->fetch_select(sql=>[$sql, @placeholders]);
}

=pod

=item ->find_all_by_keywords($keywordString)

Factory method; Reads the people who match any of the keywords from
the database and returns a list of Persons.

=cut
sub find_all_by_keywords {
    my $class = shift;
    my ( $keywordString ) = @_;

    my $records;
    if (WisCon::Authentication::current_user_has_role( 'program' )||
		WisCon::Authentication::current_user_has_role( 'devel' )  || 
		WisCon::Authentication::current_user_has_role( 'reg' )){
	$records = WisCon::Person->get_records_by_keywords($keywordString);
    } else {
	$records = WisCon::Person->get_records_by_program_names($keywordString);
    }

    return WisCon::Person->persons_from_records($records);
}

=pod

=item ->count_by_user($user_id)

Counts the people with the specified user IDs from the database. 

=cut

sub count_by_user {
        my $class = shift;
        my ( $user_id ) = @_;
# this double-counts because we have roles in that table now; what calls it?
        my $query = "select count(*) as count from UserAuthority where idUsers = $user_id;";
        my $records = sqldb()->fetch_select( sql => [$query] );
        return $records->[0]{count};
}

=pod

=item ->find_user($user_id)

Factory method; returns a single person object representing the identity 
of the given user (i.e. is connected by UserAuthority with AuthorityCode 
"identity").  

=cut

sub find_user {
    my $class = shift;
    my $user_id = shift;

    if ($class->is_cached()) {
	return $cache_by_user_id{$user_id};
    } else {
	my $ids = sqldb()->fetch_select(table => 'UserAuthority', where  => {'idUsers' => $user_id, 'AuthorityCode'=>'identity'}, order => 'idPersons');
	my $id;

	if (scalar(@$ids) > 1) {
		__alertMultiplePersons($user_id, $ids);
	    $id = shift(@$ids);
	} elsif (scalar(@$ids) == 1) {
# we found the right person
	    $id = shift(@$ids);
	} else {
# if no persons, find or make new link
	    $id = WisCon::Person->update_missing_links($user_id);
	    $id = { 'idPersons' => $id } if $id;
	}
	my $person = WisCon::Person->fetch($id->{idPersons});
	$person->{user_id} = $user_id;
	return $person;
    }
}

=pod

=item ->find_by_user_email($user_id)

Factory method; Reads the persons whose emails match the profile email
of the specified user from the database and returns a list of Persons.


=cut

sub find_by_user_email {
        my $class = shift;
        my ( $user_id ) = @_;

        my $records = sqldb()->fetch_select(
                tables => [ 'Persons', 'Users' ],
                where  => {
                        'Persons.PersonEmail' => \'Users.ProfileEmail',
                        'Users.idUsers'        => "$user_id"
                }
        );

        return WisCon::Person->persons_from_records($records);

}

=pod

=item ->new(record => $record)

Creates a new, empty Person object with the specified database record.

=cut

sub new {
        my $class = shift;
        my %args  = @_;
        my $self  = { record => $args{record} };
        bless $self, $class;
        return $self;
}

#################################################################
### Utility class methods

=pod

=item ->fetch_all

This returns a list of all Persons.

=cut

sub fetch_all {
        my $class = shift;
        $class->create_cache();
        return values %cache_by_person_id;
}

=pod

=item ->fetch_all_users

This returns a list of all Persons with an associated User record.

=cut

sub fetch_all_users {
        my $class = shift;
        $class->create_cache();
        return values %cache_by_user_id;
}

=pod

=item ->create_cache()

This creates an in-memory cache of all the Person records in the 
system, including their associated user records.

=cut

sub create_cache {
        my $class = shift;
        return if $class->is_cached();

        my $people = sqldb()->fetch_select(table=>'Persons');
        for (@$people) {
                my $person = $class->new(record=>$_) or die "Could not create object";
                $cache_by_person_id{$_->{idPersons}} = $person;
        }
        my $users = sqldb()->fetch_select(sql=>"select Users.*,UserAuthority.idPersons from Users LEFT JOIN UserAuthority on Users.idUsers=UserAuthority.idUsers and UserAuthority.AuthorityCode='identity'");
        for (@$users) {
                my $idPersons = $_->{idPersons} or next;
                delete $_->{idPersons};
                my $person = $cache_by_person_id{$idPersons} or next;
                $person->{user} = $_;
                $cache_by_user_id{$_->{idUsers}} = $person;
        }
}

=pod

=item ->is_cached()

Returns 1 if a cache of persons has been loaded into memory. 

=cut

sub is_cached {
        my $class = shift;
        if ((keys %cache_by_person_id) and (keys %cache_by_user_id)) {
                return 1;
        } else {
                return 0;
        }
}

=pod

=item ->is_registered()

Returns SaleItemName if person has a membership to the current or input convention

Used to search for orderlines with 'complete' status, then we checked for Distributions
instead, to catch pay-by-check orders awaiting receipt.
Now we use the registrations method, which returns new, incomplete,
complete and reserved convention membership registrations

=cut

sub is_registered {
    my $person = shift;
    my $idConventions = shift;
    my @registrations = $person->registrations($idConventions);
    return $registrations[0]->{SaleItemName} . ( $registrations[0]->{LineStatus} eq "complete" ? ' paid' : ' awaiting payment' ) if scalar(@registrations);
	return 0;
}

=pod

=item ->is_guest()

Returns 1 if person is a Guest of Honor to the current or input convention
(input 'past' for any convention)

=cut

sub is_guest {
    my $person = shift;
    my $idConventions = shift || WisCon::Convention->current->id;
    my $guest_data;
    if ($idConventions eq 'past') {
	$guest_data = 
	    $sqldb->fetch_select(
				 sql => [ 'select * from Guests where idPersons=? and QualifiesAsPastGuestFlag=1', $person->id ]
				 );
    } else {
	$guest_data = 
	    $sqldb->fetch_select(
				 sql => [ 'select * from Guests where idPersons=? and idConventions=?', $person->id, $idConventions ]
				 );
    }
    return scalar(@$guest_data);
}

=pod

=item ->is_tiptree()

Returns 1 if person is a Tiptree Winner (special case for WC35)

=cut

sub is_tiptree {
    my $person = shift;
    my $idConventions = shift || WisCon::Convention->current->id;
    my $guest_data;
 	$guest_data = 
	$sqldb->fetch_select(
		sql => [ 'select * from Guests where idPersons=? and GuestType like "Tiptree%"', $person->id ]
	);
	return scalar(@$guest_data);
}


sub may_order {
    my $person = shift;
    my $idConventions = shift || WisCon::Convention->current->id;
    my $sql;
    if ($person->is_guest('past')) {
		$sql = 'select * from CatalogItems where idConventions=? and SaleItemName="PastGuest"';
    } else {
		$sql = 'select * from CatalogItems where idConventions=? and ((PublicFlag=1 and ConventionMembershipFlag=1) or SaleItemName="Supporting")';
    }
	$sql .= ' order by OrderBy';
    my $possible_orders = 
	$sqldb->fetch_select(
		sql => [ $sql, $idConventions  ]
	);
    return @$possible_orders;
}

sub may_purchase {
    my $person = shift;
    my $sql = 'select * from CatalogItems where StartDate < ? and EndDate > ? and ( (ConventionMembershipFlag=0 and SaleItemName != "Dessert" and ((DonationFlag is null) or (DonationFlag = 0))) or ';
    my $possible_orders;
		if ($person->is_guest()) {
			$sql .= '(SaleItemName="Guest" and ConventionMembershipFlag=1))';
		}
    elsif ($person->is_guest('past')) {
			$sql .= '(SaleItemName="PastGuest" and ConventionMembershipFlag=1))';
    } else {
			$sql .= '(SaleItemName not in ("Guest","PastGuest") and ConventionMembershipFlag=1))';
		}
		$possible_orders = 
			$sqldb->fetch_select(
				sql => [ $sql, WisCon::DateTime::now(), WisCon::DateTime::now(),  ]
			);
    return @$possible_orders;
}

sub has_applied_to_deal {
	my $person = shift;
	my $applications = $sqldb->fetch_select(
		sql => [ 'select * from DealerApplications where idConventions = ? and idPersons = ?', WisCon::Convention->submittable->{idConventions}, $person->id,  ]
	);
	return @$applications
}

sub has_table {
	my $person = shift;
	my $convention = shift || WisCon::Convention->submittable;
	my $applications = $sqldb->fetch_select(
		sql => [ 'select * from DealerApplications where idConventions = ? and idPersons = ?', $convention->{idConventions}, $person->id,  ]
	);
	return unless @$applications;
	return 0 if $applications->[0]->{'ApplicationStatus'} eq 'rejected';
	return $applications->[0]->{'ApplicationStatus'}
}

sub has_applied_for_art_show {
	my $person = shift;
	my $applications = $sqldb->fetch_select(
		sql => [ 'select * from ArtShowApplications where idConventions = ? and idPersons = ?', WisCon::Convention->submittable->{idConventions}, $person->id,  ]
	);
	return @$applications	
}

sub space_applied_for_art_show {
	my $application = shift;
	my $format = shift;
	my $space;
	if ($application->{'RequestedQuarterTables'}) {
		if ($application->{'RequestedQuarterTables'} == 4) {
			$space .= '1 table';
		} elsif ($application->{'RequestedQuarterTables'} == 2) {
			$space .= ($format eq 'text' ? '1/2' : '&frac12;') . ' table';
		} else {
			$space .= ($format eq 'text' ?  $application->{'RequestedQuarterTables'} . '/4' : '&frac' . $application->{'RequestedQuarterTables'} . '4;') . ' table';
		}
	}	
	if ($application->{'RequestedHalfPanels'}) {
		$space .= ($format eq 'text' ? ' and ' : ' &amp; ') if $space;
		my $panels = int($application->{'RequestedHalfPanels'} / 2);
		$space .= ($panels == 0 ? '' : $panels . ' ') . (($panels * 2) == $application->{'RequestedHalfPanels'} ? '' : ($format eq 'text' ? '1/2' : '&frac12;')) . ' panel' . ($panels eq 0 ? '' : 's');
	}
	if ($application->{'RequestedPrintshopFlag'}) {
		$space .= ($format eq 'text' ? ' and ' : ' &amp; ') if $space;
		$space .= 'print shop'
	}
	$space .= 'no space' unless $space;
	$space .= ' <small>more space please</small>' if  $application->{'RequestedMoreSpaceFlag'};
	return $space;
}

sub space_approved_for_art_show {
	my $application = shift;
	my $space;
	if ($application->{'NumQuarterTables'}) {
		if ($application->{'NumQuarterTables'} == 4) {
			$space .= '1 table';
		} elsif ($application->{'NumQuarterTables'} == 2) {
			$space .= '&frac12; table';
		} else {
			$space .= '&frac' . $application->{'NumQuarterTables'} . '4; table';
		}
	}	
	if ($application->{'NumHalfPanels'}) {
		$space .= ', ' if $space;
		my $panels = int($application->{'NumHalfPanels'} / 2);
		$space .= $panels . (($panels * 2) == $application->{'NumHalfPanels'} ? '' : '&frac12;') . ' panels';
	}
	if ($application->{'ApprovedPrintShopFlag'}) {
		$space .= ' &amp; ' if $space;
		$space .= 'print shop'
	}
	$space .= 'no space' unless $space;
	return $space;
}

sub space_waitlisted_for_art_show {
	my $application = shift;
	my $space;
	if ($application->{'WaitlistedQuarterTables'}) {
		if ($application->{'WaitlistedQuarterTables'} == 4) {
			$space .= '1 table';
		} elsif ($application->{'WaitlistedQuarterTables'} == 2) {
			$space .= '&frac12; table';
		} else {
			$space .= '&frac' . $application->{'WaitlistedQuarterTables'} . '4; table';
		}
	}	
	if ($application->{'WaitlistedHalfPanels'}) {
		$space .= ', ' if $space;
		my $panels = int($application->{'WaitlistedHalfPanels'} / 2);
		$space .= $panels . (($panels * 2) == $application->{'WaitlistedHalfPanels'} ? '' : '&frac12;') . ' panels';
	}
	return $space;
}

=pod

=item ->find_missing_links($user_id)

Finds all Person records such that the PersonEmail for that Person
matches the ProfileEmail of the specified user, and no UserAuthority
record yet exists for that Person.

=cut

sub find_missing_links {
    my $class = shift;
    my $user_id = shift;

    # TODO __DAVID__ Is there a cleaner way to do subselects?

    my $query = "select 
                        a.idPersons
                 from
                        Persons a,
                        Users b
                 where
                        b.idUsers = $user_id 
                        and a.PersonEmail = b.ProfileEmail
                        and a.idPersons not in
                                (select
                                        idPersons
                                from
                                        UserAuthority);";

    my $results = sqldb()->fetch_select( sql => [$query] );

    my $person_ids = [];
    foreach my $result (@$results) {
	push( @$person_ids, $$result{"idPersons"} );
    }

    return $person_ids;
}

=pod

=item ->update_missing_links($user_id)

Finds any Person records without UserAuthority records whose email matches
the email of the specified user, and creates matching UserAuthority records.

Returns the id of the first Person record found or created.

=cut

sub update_missing_links {
        my $class = shift;
        my ( $user_id ) = @_;

        my $ids = WisCon::Person->find_missing_links( $user_id );
	my $id;
	if (scalar(@$ids) > 1) {
		__alertMultiplePersons($user_id, $ids);
	    $id = shift(@$ids);
	} elsif (scalar(@$ids) == 1) {
# we found the right person
	    $id = shift(@$ids);
	} else {
#  make & save a new person to return.
	    my $user = 	sqldb()->fetch_one(table => 'Users', where  => {'idUsers' => $user_id});
	    my $person = WisCon::Person->new( record => {'PersonEmail',$user->{ProfileEmail}, 'ActiveFlag',1} );
	    $person->save($user_id);
	    $id = $person->id();
	}
	WisCon::Person->insert_link( $user_id, $id );

        return $id;
}

=pod

=item ->insert_link($user_id, $person_id)

Inserts an "identity" record in the UserAuthority table, connecting the
specified user and person IDs (unless one already exists).

=cut

sub insert_link {
        my $class = shift;
        my ( $user_id, $person_id ) = @_;
	my $link = sqldb()->fetch_one(table => 'UserAuthority', where  => {'idUsers' => $user_id, 'idPersons' => $person_id, 'AuthorityCode' => 'identity'});
	unless ($link) {
	    WisCon::SQLEngine::log_insert(sqldb(),
					  table  => 'UserAuthority',
					  sequence => 'idUserAuthority',
					  values => {
					      'idUsers'        => "$user_id",
					      'idPersons'     => "$person_id",
					      'AuthorityCode' => 'identity'
					      }
					  );
	  }
}

#################################################################
### Instance methods

=pod

=item ->preferences(isAdmin)

Returns an array of hashrefs representing this users' current preferences

=cut

# pass in boolean $isAdmin to show Preferences that are not visible to the end user

sub preferences {
    my $self = shift;
	my $isAdmin =   @_;
    my $data = $sqldb->fetch_select(	
	sql => ['select RefPreferenceTypes.*, MyPrefs.idPersonalPreferences, MyPrefs.idPersons, MyPrefs.PreferenceValue, MyPrefs.PreferenceType, MyPrefs.PreferenceText 
	from RefPreferenceTypes 
	Left join  (select idPersonalPreferences, idPersons, PreferenceValue, PreferenceType, PreferenceText from PersonalPreferences
                    where  end_date is null and idpersons=?) as MyPrefs
	on RefPreferenceTypes.idPreferenceTypes = MyPrefs.PreferenceType
	where ( RefPreferenceTypes.VisibleToUserFlag=1 or 1=? )
	order by RefPreferenceTypes.OrderBy, RefPreferenceTypes.idPreferenceTypes' ,$self->id(),$isAdmin ]);

    return @$data;
}

=pod

=item ->is_banned()

Returns an boolean letting us know whether this person has been banned from WisCon

=cut

sub is_banned {
    my $self = shift;
    my $data = $sqldb->fetch_select(	
	sql => ['select idPersonalPreferences, idPersons from PersonalPreferences,RefPreferenceTypes
                    where PersonalPreferences.end_date is null and PersonalPreferences.PreferenceType=RefPreferenceTypes.idPreferenceTypes 
					and PersonalPreferences.idpersons=? and RefPreferenceTypes.Code="banned"', $self->id() ]);
    return scalar @$data;
}

=pod

=item ->has_special()

Returns an boolean letting us know whether this person has a
special badge in the current convention

=cut

sub has_special {
    my $self = shift;
    my $data = $sqldb->fetch_select(	
                table => 'SpecialBadges',
                where => { idPersons => $self->id, idConventions => WisCon::Convention->current->id }
                );
    return scalar @$data;
}

=pod

=item ->people_conflicts()

Returns a list of idPersons with whom this Person may not be scheduled

=cut

sub people_conflicts {
    my $self = shift;
    my $from_data = $sqldb->fetch_select(	
	sql => ['select idPersonsFrom from PeopleConnections 
		where (EndDate = "0000-00-00" or EndDate is null) and ConnectionType = "notwith" and idPersonsTo = ?', $self->id() ]);
    my $to_data = $sqldb->fetch_select(	
		sql => ['select idPersonsTo from PeopleConnections 
			where (EndDate = "0000-00-00" or EndDate is null) and ConnectionType = "notwith" and idPersonsFrom = ?', $self->id() ]);
	my %data;
	foreach (@$from_data) {
		$data{$_->{idPersonsFrom}} = 1;
	}
	foreach (@$to_data) {
		$data{$_->{idPersonsTo}} = 1;
	}
    return (keys %data);
}

=pod

=item ->orders()

Returns an array of hashrefs representing this users' current 
orders (or orders for the input convention id)

=cut

sub orders {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select Orders.*,OrderLines.*,CatalogItems.* from Orders 
				join OrderLines on Orders.idOrders = OrderLines.idOrders 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				where Orders.idPersons = ? and (CatalogItems.idConventions = ? or isnull(CatalogItems.idConventions)) 
				and Orders.OrderStatus in ("incomplete", "complete", "reserved")', $self->id(), $convention_id ]
					);
    return @$data;
}

sub pending_orders {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select Orders.*,OrderLines.*,CatalogItems.* from Orders 
				join OrderLines on Orders.idOrders = OrderLines.idOrders 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				where Orders.idPersons = ? and (CatalogItems.idConventions = ? or isnull(CatalogItems.idConventions)) 
				and Orders.OrderStatus = "new"', $self->id(), $convention_id ]
					);
    return @$data;
}

sub cancelled_orders {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select Orders.*,OrderLines.*,CatalogItems.* from Orders 
				join OrderLines on Orders.idOrders = OrderLines.idOrders 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				where Orders.idPersons = ? and (CatalogItems.idConventions = ? or isnull(CatalogItems.idConventions)) 
				and Orders.OrderStatus = "cancelled"', $self->id(), $convention_id ]
					);
    return @$data;
}

sub refunded_orders {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select Orders.*,OrderLines.*,CatalogItems.* from Orders 
				join OrderLines on Orders.idOrders = OrderLines.idOrders 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				where Orders.idPersons = ? and (CatalogItems.idConventions = ? or isnull(CatalogItems.idConventions)) 
				and Orders.OrderStatus = "refunded"', $self->id(), $convention_id ]
					);
    return @$data;
}

# paid order lines
sub order_lines {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select OrderLines.*,CatalogItems.* from OrderLines 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				join Distributions on Distributions.idOrderLines = OrderLines.idOrderLines 
				where CatalogItems.idConventions = ? and idPersons=?', $convention_id, $self->id ]
					);
    return @$data;
}

sub order_in_progress {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
	sql => [ 'select Orders.*,OrderLines.*,CatalogItems.* from Orders 
				join OrderLines on Orders.idOrders = OrderLines.idOrders 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				where Orders.idPersons = ? and (CatalogItems.idConventions = ? or isnull(CatalogItems.idConventions)) and Orders.OrderStatus = "new" 
				and Orders.idOrders in (select max(idOrders) from Orders where Orders.idPersons = ? and Orders.OrderStatus = "new") 
				and Orders.idOrders not in (select OrderLines.idOrders from OrderLines join Distributions on OrderLines.idOrderLines=Distributions.idOrderLines 
				join FinancialTransactions on Distributions.idFinancialTransactions=FinancialTransactions.idFinancialTransactions 
				where FinancialTransactions.idPersons = ?)', $self->id(), $convention_id, $self->id(), $self->id() ]
					);
    return @$data;
}


=pod

=item ->registrations()

Returns an array of hashrefs representing this users' current 
registrations.  
OrderStatus and LineStatus may be cancelled, refunded, reserved, complete, incomplete or new

=cut

sub registrations {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
	my $status = shift;
	my $statuses = '"incomplete", "complete", "reserved", "new"';
	if ($status) { $statuses = '"' . $status . '"'; }
    my $data = $sqldb->fetch_select(
        sql => [ 'select CatalogItems.*,OrderLines.LineStatus,OrderLines.idOrders,Orders.idPersons as OrderPerson from OrderLines 
                  join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				  join Orders on OrderLines.idOrders = Orders.idOrders
                  where CatalogItems.idConventions = ? 
                  and ConventionMembershipFlag=1 and OrderLines.idPersons=? 
                  and OrderLines.LineStatus in (' . $statuses . ')', $convention_id, $self->id  ]
        );
    return @$data;
}

sub desserts {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
	my $status = shift;
	my $statuses = '"incomplete", "complete", "reserved", "new"';
	if ($status) { $statuses = '"' . $status . '"'; }
    my $data = $sqldb->fetch_select(
        sql => [ 'select CatalogItems.*,
				OrderLines.LineStatus,
				OrderLines.idOrders,
				OrderLines.idPersons,
				OrderLines.LineAmt,
				Orders.idPersons as OrderPerson 
				from OrderLines 
                  join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				  join Orders on OrderLines.idOrders = Orders.idOrders
                  
	              where CatalogItems.idConventions = ? 
                  and SaleItemName like "%Dessert%" and OrderLines.idPersons=? 
                  and OrderLines.LineStatus in (' . $statuses . ')', $convention_id, $self->id  ]
        );
    return @$data;
}

sub owes {
    my $self = shift;
    my $convention_id = shift || WisCon::Convention->current->{idConventions};
    my $data = $sqldb->fetch_select(
        sql => [ 'select CatalogItems.*
				from OrderLines 
				join CatalogItems on OrderLines.idCatalog = CatalogItems.idCatalog 
				join Orders on OrderLines.idOrders = Orders.idOrders                  
				where CatalogItems.idConventions = ? 
				and OrderLines.idPersons=? 
                  and OrderLines.LineStatus in ("incomplete", "reserved", "new")', $convention_id, $self->id  ]
        );
    return scalar(@$data);
}

sub financial_transactions {
    my $self = shift;	
    my $data = $sqldb->fetch_select(
        sql => [ 'select * from FinancialTransactions 
					where idPersons=? order by TransactionDate desc', $self->id  ]
        );
    return @$data;
}

=pod

=item ->assignments()

Returns an array of hashrefs representing this users' current 
assignments.  

=cut

sub assignments {
	my $self = shift;
	my $type = shift;
	# If we haven't calculated already, now check the user's assignments
	if (not defined($self->{assignments})) {
	    my $data = sqldb()->fetch_select(
  	        sql => [ 'select ProgramItems.*,Assignments.*,Slots.*,Locations.*,TimePeriods.*  from Assignments join ProgramItems on Assignments.idItems = ProgramItems.idItems and Assignments.idPersons = ? left join Slots on ProgramItems.idSlots=Slots.idSlots left join Locations on Slots.idLocations=Locations.idLocations left join TimePeriods on Slots.idTimePeriods=TimePeriods.idTimePeriods where ProgramItems.idConventions=? and ProgramItems.ItemStatus<>"deferred" and ProgramItems.ItemStatus<>"rejected" order by TimePeriods.StartDateTime', $self->id, WisCon::Convention->current->id ]
					     );
		push(@{ $self->{assignments} }, @$data) if ($data);
	}
	if ($type) {
		my @subset = grep { $_->{AssignmentType} eq $type } @{ $self->{assignments} };
		return @subset;
	} else {
		my @full = @{ $self->{assignments} };
		return @full;
	}
}

=pod

=item ->num_assignments()

Report the number of assignments this Person has.  

=cut

sub num_assignments {
        my $self = shift;
        return scalar( $self->assignments(@_) );
}

=pod

=item ->assign(item=>$item,type=>$type,status=>$status)

Assigns this person to the specified item, where the item arguments can 
be either an item ID number or a ProgramItem object.  Default type is 
"participant", but may specify "moderator" or "other" instead.  
Default status is "auto".  

=cut

sub assign {
        my $self = shift;
        my %args = @_;

        my $item = $args{item};
        die "Invalid item argument" unless (ref($item) =~ m/ProgramItem/);
        my $idItems = $item->id;
        $idItems =~ m/^\d+$/ or croak "Invalid program item argument $item";

        my $type = $args{type} || "participant";
        die "Invalid type [$type]" unless WisCon::Assignment->is_valid_type($type);

        my $status = $args{status} || 'auto';
        die "Invalid status [$status]" unless WisCon::Assignment->is_valid_status($status);

        die "No person id in ".Dumper($self) unless ($self->id());

        $self->assignments(); # ensure that past assignments are checked
        my $values = { 'idItems' => $idItems, 
                       'idPersons'     => $self->id(), 
                       'AssignmentType' => $type,
                       'AssignmentStatus' => $status };
        my $rc = WisCon::SQLEngine::log_insert(sqldb(),
                                               sequence => 'idAssignments', 
                                               table => 'Assignments', 
                                               values => $values);
        if ($rc) {
                push(@{ $self->{assignments} },$values);
                # also mark assignment in the item object
                $item->register_assignment($values);
        }
        return $rc;
}

=pod

=item ->total_interest()

Report the total interest this user has expressed in all items, 
equivalent to the number of items that the user has expressed 
maximum interest in.  

=cut

sub total_interest {
        my $self = shift;
        return undef unless ($self->user_id);
        return $self->{total_interest} if (defined($self->{total_interest}));
        my $total = WisCon::ProgramItem->user_total_interest($self->user_id);
        $self->{total_interest} = $total;
        return $total;
}

=pod

=item ->num_mod_flags()

Report the number of items that this user has expressed interest 
in moderating.  

=cut

sub num_mod_flags {
        my $self = shift;
        return undef unless ($self->user_id);
        return $self->{num_mod_flags} if (defined($self->{num_mod_flags}));
        my $num = WisCon::ProgramItem->user_mod_flags($self->user_id);
        $self->{num_mod_flags} = $num;
        return $num;
}

=pod

=item ->user_record()

Returns a hashref of the row from the User table associated with 
this person.  

=cut

sub user_record {
        my $self = shift;
        if (not $self->{user}) {
                my $user = sqldb()->fetch_one(sql=>["select Users.* from Users,UserAuthority where Users.idUsers=UserAuthority.idUsers and UserAuthority.idPersons=? and UserAuthority.AuthorityCode='identity'", $self->id]);
                $self->{user} = $user;
                $cache_by_user_id{$self->id} = $self;
        }
        return $self->{user};
}

=pod

=item ->user_id()

Returns the ID of the identity user for this person.  

=cut

sub user_id {
        my $self = shift;
        if (not exists($self->{user_id})) {
                my $user = $self->user_record();
                $self->{user_id} = ($user) ? $user->{idUsers} : undef;
        }
        return $self->{user_id};
}

=pod

=item ->save($user_id)

Writes this Person to the database. If it is an insert and ActiveFlag is not explicitly set, default it to 1

=cut

# add check for identity or admin/devel privileges!!

sub save {
        my $self = shift;
        my $user_id = shift;
        my $person_id = $self->id();
        if ($person_id) {
                WisCon::SQLEngine::log_update(sqldb(),
                        table  => 'Persons',
                        key=>'idPersons',
                        id  => $person_id,
                        values => $self->{record}
                );
        } else {
				my $vals = $self->{record};
				if (!$vals->{ActiveFlag}) {
					$vals ->{ActiveFlag} = 1;
				}
                WisCon::SQLEngine::do_insert(sqldb(),
                        table  => 'Persons',
                        sequence => 'idPersons',
                        values => $vals
                );
                my $query = "select last_insert_id() as new_id;";
                my $result = sqldb()->fetch_one( sql => [$query] );
                my $new_id = $$result{"new_id"};
                $self->set_val('idPersons', $new_id);
                WisCon::Person->insert_link($user_id, $new_id, sqldb()) if $user_id;
        }
}

=pod

=item ->scheduleId(conferenceId)

Retrieves the schedule id of the person, if there is one, otherwise creates a new one.
conferenceId is an optional param, if blank will return the scheduleId for the current conference.

=cut

sub scheduleId {
	my $self = shift;
	my $conf_id = shift;
	my $person_id = $self->id();
	
	$conf_id = $conf_id || WisCon::Convention->current->id;
	
	my $pers_schedId = sqldb()->fetch_select(
			table => 'PersonalSchedules',
			columns => 'idSchedules',
			where => { 'idPersons' => $person_id, 'idConventions' => $conf_id },
		);
	if (!@$pers_schedId) {
			#add new scheduleId
			#TODO Figure out how to get this to return the scheduleId and log
			$pers_schedId = sqldb()->do_insert(
				table => 'PersonalSchedules',
				values => { 'idPersons' => $person_id, 'idConventions' => $conf_id },
			);
		}
		
	$pers_schedId = $pers_schedId->[0]->{idSchedules};
	return $pers_schedId;
}

#################################################################
### Accessors

sub get_val {
        my $self = shift;
        my $col  = shift;
        return $self->{record}{$col};
}

sub set_val {
        my $self    = shift;
        my $col     = shift;
        my $new_val = shift;
        $self->{record}{$col} = $new_val;
}

sub first_middle_last {
        my $self        = shift;
		my @names;
		push(@names, $self->first_name()) if $self->first_name();
		push(@names, $self->middle_name()) if $self->middle_name();
		push(@names, $self->last_name()) if $self->last_name();
        my $full_name   = join(' ', @names);

        return $full_name;
}

sub last_first_middle {
        my $self        = shift;
		my @names;
		push(@names, $self->last_name()) if $self->last_name();
		push(@names, $self->first_name()) if $self->first_name();
		push(@names, $self->middle_name()) if $self->middle_name();
        my $full_name   = join(' ', @names);

        return $full_name;
}

sub full_name {
        my $self        = shift;
        my $full_name   = $self->first_middle_last();
		my @names;
		push(@names, $self->public_name()) if $self->public_name() && ($self->public_name() ne $full_name);
		push(@names, $self->badge_name()) if $self->badge_name() && ($self->badge_name() ne $full_name);
        if (scalar @names) {
                @names = uniq(@names);
                $full_name .= " (" . join(', ', @names) . ")";
        }

        return $full_name || 'No name given';
}

sub id {
        my $self = shift;
        return $self->get_val('idPersons');
}

sub last_name {
        my $self = shift;
        return $self->get_val('LastName');
}

sub set_last_name {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'LastName', $new_val );
}

sub first_name {
        my $self = shift;
        return $self->get_val('FirstName');
}

sub set_first_name {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'FirstName', $new_val );
}

sub middle_name {
        my $self = shift;
        return $self->get_val('MiddleName');
}

sub set_middle_name {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'MiddleName', $new_val );
}

sub public_name {
        my $self = shift;
        return $self->get_val('PublicName');
}

sub set_public_name {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'PublicName', $new_val );
}

sub program_name {
    my $self = shift;
    return 'ANONYMOUS' if $self->{PublicAnonymousFlag};
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    return 'ANONYMOUS' if $self->anonymous();
    return $self->admin_name();
}

sub book_name {
    my $self = shift;
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    return $self->public_name || $self->first_middle_last();
}

sub link_name {
    my $self = shift;
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    return '<a href="/bios#' . join('_', split(' ', $self->program_name)) . '">' . $self->program_name . '</a>';
}

sub badge_name {
    my $self = shift;
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    return $self->get_val('BadgeName') || $self->book_name();
}

sub admin_name {
    my $self = shift;
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    return $self->public_name ||  $self->first_middle_last();
}

# pass in anonymous flag to sort ANONYMOUSly
# use when listing by program_name
sub sorting_name {
    my $self = shift;
    if (ref($self) ne 'WisCon::Person') {
		$self = WisCon::Person->fetch($self->{'idPersons'});
    }
    my $flag = shift;
    return 'ANONYMOUS' if $flag && ($flag eq 'anon') && $self->anonymous();
    return uc($self->get_val('ReportAlphaSort')) if $self->get_val('ReportAlphaSort');
 	my $nickname = $self->public_name;
	$nickname = ($self->public_name || $self->get_val('BadgeName')) if $flag && ($flag eq 'badge');
    if ($nickname) {
        my @words = split(" ", $nickname);
        my $word = $words[-1];
        $word .= " " . $nickname unless $word eq $nickname;
        return uc($word);
    }
    return uc($self->last_first_middle());
}

sub anonymous {
        my $self = shift;
        return $self->get_val('PublicAnonymousFlag');
}

sub set_anonymous {
        my $self = shift;
        my $new_val = shift;
        $self->set_val( 'PublicAnonymousFlag', $new_val );
}

# fall back to the login email, if no PersonEmail has been supplied
sub eCube_email {
        my $self = shift;
	    my $record = $sqldb->fetch_one(sql => ['select PreferenceText from PersonalPreferences
	                    where PreferenceType = 9 and PreferenceValue = 1 
						and PersonalPreferences.idpersons=?', $self->id() ]);
	    return $record->{'PreferenceText'};
}

# fall back to the login email, if no PersonEmail has been supplied
sub email {
        my $self = shift;
		return $self->get_val('PersonEmail') || $self->user_email;
}

# this is the new hotness, i.e., the correct method to use to
# extract a login email for a given Person; we prefer to use
# login email because we have validated their receipt thereof

sub user_email {
   	my $self = shift;
    my $record = $self->user_record;
	return $record->{'ProfileEmail'};
}

sub set_email {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'PersonEmail', $new_val );
}

sub website {
        my $self = shift;
        return $self->get_val('PersonUrl');
}

sub set_website {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'PersonUrl', $new_val );
}

sub short_bio {
        my $self = shift;
        return $self->get_val('ShortBio');
}

sub set_short_bio {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'ShortBio', $new_val );
}

sub long_bio {
        my $self = shift;
        return $self->get_val('LongBio');
}

sub set_long_bio {
        my $self    = shift;
        my $new_val = shift;
        $self->set_val( 'LongBio', $new_val );
}

######################################################################
# Private methods
sub __alertMultiplePersons {
		my ($user_id, $ids) = @_;
		my $to = WisCon::Email::get_forwarder('appdev');
		my $from = WisCon::Email::get_forwarder('support');
		my $subject = "WisCon User $user_id Has Multiple Identities";
		my $body = "Too many identity links for user $user_id\n\n" . Data::Dumper::Dumper( $ids );
		#($to, $from, $subject, $body, $cc)
		WisCon::Email::send_email($to,$from,$subject,$body);

}

######################################################################

1;
