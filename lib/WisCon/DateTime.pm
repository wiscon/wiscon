package WisCon::DateTime;

use DateTime;

######################################################################

sub now {
    my ($sc,$mn,$hr,$dd,$mo,$yr) = localtime();
    $mo++; $yr += 1900;
    return sprintf("%4d-%02d-%02d %02d:%02d:%02d",$yr,$mo,$dd,$hr,$mn,$sc);
}

# calculated from Convention EndDate

sub days {
	my $monday = WisCon::Convention->current->{EndDate};
	my ($year, $month, $day) = split('-', $monday);
	my $tues_day = $day + 1;
	my $tues_mon = $month;
# if we find ourselves in June, increment Tuesday's month,
# set Tuesday's day to the 1st
	if ($tues_day == 32) {
		$tues_day = 1;
		$tues_mon++;
	}
	my %months;
	$months{'05'} = 'May';
	$months{'06'} = 'June';
	
    return ( { 'num' => 0, 'day' => 'Wednesday', 'date' => join(' ', $months{$month}, $day - 5), 'db' => join('-', $year, $month, $day - 5) },
	     { 'num' => 1, 'day' => 'Thursday', 'date' => join(' ', $months{$month}, $day - 4), 'db' => join('-', $year, $month, $day - 4) },
	     { 'num' => 2, 'day' => 'Friday', 'date' => join(' ', $months{$month}, $day - 3), 'db' => join('-', $year, $month, $day - 3) },
	     { 'num' => 3, 'day' => 'Saturday', 'date' => join(' ', $months{$month}, $day - 2), 'db' => join('-', $year, $month, $day - 2) },
	     { 'num' => 4, 'day' => 'Sunday', 'date' => join(' ', $months{$month}, $day - 1), 'db' => join('-', $year, $month, $day - 1) },
	     { 'num' => 5, 'day' => 'Monday', 'date' => join(' ', $months{$month}, $day), 'db' => $monday },
	     { 'num' => 6, 'day' => 'Tuesday', 'date' => join(' ', $months{$tues_mon}, $tues_day), 'db' => join('-', $year, $tues_mon, $tues_day) }, );
}

sub times {
    return ( { 'num' => 0, 'hours' => '8:30 am to 10:00 am', start => '08:30:00', mid => '09:15:00', end => '10:00:00', flag => 'Exclude830Flag' },
	     { 'num' => 1, 'hours' => '10:00 am to 12 Noon', start => '10:00:00', mid => '11:00:00', end => '12:00:00', flag => 'Exclude10amFlag' },
	     { 'num' => 2, 'hours' => '12 Noon to 1:00 pm', start => '12:00:00', mid => '12:30:00', end => '13:00:00', do_not_schedule => 1 },
	     { 'num' => 3, 'hours' => '1:00 pm to 2:30 pm', start => '13:00:00', mid => '13:45:00', end => '14:30:00', flag => 'Exclude1pmFlag' },
	     { 'num' => 4, 'hours' => '2:30 pm to 4:00 pm', start => '14:30:00', mid => '15:15:00', end => '16:00:00', flag => 'Exclude230Flag' },
	     { 'num' => 5, 'hours' => '4:00 pm to 5:30 pm', start => '16:00:00', mid => '16:45:00', end => '17:30:00', flag => 'Exclude4pmFlag' },
	     { 'num' => 6, 'hours' => '5:30 pm to 7:00 pm', start => '17:30:00', mid => '18:15:00', end => '19:00:00', do_not_schedule => 1 },
	     { 'num' => 7, 'hours' => '7:00 pm to 9:00 pm', start => '19:00:00', mid => '20:00:00', end => '21:00:00', flag => 'Exclude7pmFlag' },
	     { 'num' => 8, 'hours' => '9:00 pm to 10:30 pm', start => '21:00:00', mid => '21:45:00', end => '22:30:00', flag => 'Exclude9pmFlag' },
	     { 'num' => 9, 'hours' => '10:30 pm to Midnight', start => '22:30:00', mid => '23:15:00', end => '23:59:00', flag => 'Exclude1030Flag' },
	     { 'num' => 10, 'hours' => 'After Midnight', start => '23:59:00', mid => '02:00:00', end => '04:00:00', flag => 'ExcludeMidnightFlag' },);
}

sub special_events {
    return ( { 'num' => 0, 'name' => 'Tiptree Auction', flag => 'ExcludeAuctionflag' }, 
             { 'num' => 1, 'name' => 'The Gathering', flag => 'ExcludeGatheringFlag' },
             { 'num' => 2, 'name' => 'Writer\'s Workshop', flag => 'ExcludeWorkshopFlag' },
             { 'num' => 3, 'name' => 'Sign-Out!', flag => 'ExcludeSignoutFlag' },
             { 'num' => 4, 'name' => 'Guest of Honor Speeches', flag => 'ExcludeGOHFlag'}, );
}

sub make_datetime {
    my $date = shift;
    my ($day, $hour) = split(' ', $date);
    $day = $date unless $day;
    my ($y, $m, $d) = split('-', $day);
    my ($h, $mm) = split(':', $hour);
    $dt = DateTime->new( year   => ($y == 0 ? 2016 : $y),
			 month  => ($m == 0 ? 1 : $m),
			 day    => ($d == 0 ? 1 : $d),
			 hour   => $h || 0,
			 minute => $mm || 0,
			 );
    return $dt;
}

sub ISOformat {
	my $date = shift;
	my $zone = shift;
# default to Central Daylight Time, expected timezone of con
	$zone = '-0500' unless $zone;
    $date =~ s/ /T/;
    $date .= $zone if $date;
    return $date;
}

# call with datetime
sub dayname {
    my $day = make_datetime(shift);
    return $day->month_abbr . ' ' . $day->day . ', ' . $day->year;
}

sub dayrange {
    my $start = make_datetime(shift);
    my $end = make_datetime(shift);
    my $showday = shift;
    return ($showday ? $start->day_name . ', ' : '') . $start->month_abbr . ' ' . $start->day . ' - ' . ($showday ? $end->day_name . ', ' : '') . (($start->month_abbr ne $end->month_abbr || $showday) ? $end->month_abbr . ' ' : '') . $end->day . ', ' . $start->year;
}

# call with datetime, datetime, ['abbr' | 'full']
# returns pretty formatted time span
# includes day name if optional 3rd argument is 'abbr' or 'full'
sub descriptor {
  my $start = make_datetime(shift);
  my $end = make_datetime(shift);
  my $showday = shift;
  my $start_clock = sprintf( '%d:%02d', ($start->hour % 12 ) || 12, $start->minute );
  my $end_clock = sprintf( '%d:%02d', ($end->hour % 12 ) || 12, $end->minute );
  if ($start->day eq $end->day) {
      return ($showday eq 'abbr' ? $start->day_abbr . ', ' : ($showday eq 'full' ? $start->day_name . ', ' : '')) . $start_clock . (($end->hour < 12 && $start->hour < 12) || ($end->hour > 11 && $start->hour > 11) ? '' : ($start->hour > 11 ? ' pm' : ' am')) . "&ndash;$end_clock" . ($end->hour > 11 ? ' pm' : ' am');
  }
  return ($showday eq 'full' ? $start->day_name : $start->day_abbr) . ', ' . $start_clock . ($start->hour > 11 ? ' pm' : ' am') . "&ndash;" . ($showday eq 'full' ? $end->day_name : $end->day_abbr) . ', ' . $end_clock . ($end->hour > 11 ? ' pm' : ' am');
}

######################################################################

1;
