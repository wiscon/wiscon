package WisCon::Ref;

=pod

A class representing any reference table.

=cut

use Carp;
use Data::Dumper;
use strict;
use warnings;
use WisCon::SQLEngine;

# local database SQLEngine object
my $sqldb = undef;

#################################################################
### Constructors and factory methods

=pod

=item ->all()

Returns a list of all items in the reference table, ordered by DisplayOrder if the column exists. 

=cut

sub all {
	my $class = shift;
	my $table = shift;
	my $records = $class->sqldb()->fetch_select(table => 'Ref' . $table, order => (has_display_order($class, $table) ? 'DisplayOrder' : "id" . $table));
	my @all = map { WisCon::Ref->fetch($table, id=>$_->{"id" . $table}) } @$records;
	return @all;
}

my %has_display_order_cache = ();

sub has_display_order {
	my $class = shift;
	my $table = shift;
	
	if ( exists $has_display_order_cache{ $table } ) {
		return $has_display_order_cache{ $table }
	}
	
	my @columns = $class->sqldb()->detect_table( 'Ref' . $table );

	$has_display_order_cache{ $table } = grep { $_->{name} eq 'DisplayOrder' } @columns;
}

sub hash {
    my $class = shift;
    my $table = shift;
    my @rows = WisCon::Ref->all($table);
    my $hash;
    foreach my $row (@rows) {
	$hash->{$row->id()} = $row;
    }
    return $hash;
}


=pod

=item ->fetch($table, id=>$item_id)

Factory method; Reads the item with the specified ID from the 
Tracks table.

=cut

sub fetch {
	my $class = shift;
	my $table = shift;
# take this out when we fix this field!!
	my $idstring = 'id' . $table;
	my %args = @_;
	$args{id} and $args{id} =~ m/^\d+$/ or croak "No id argument";
	my $record = $class->sqldb()->fetch_one(table => 'Ref' . $table,
						where => { $idstring => "$args{id}" });
	return undef unless ($record);
	my $self = WisCon::Ref->new( $table, record => $record );
	return $self;
}

=pod

=item ->new(record => $record)

Creates a new, empty Track object with the specified database record.

=cut

sub new {
	my $class = shift;
	my $table = shift;
	my %args  = @_;
	my $record = $args{record};
	my $self  = { record => $record, table => $table };
	bless $self, $class;
	return $self;
}

#################################################################
### Utility class methods

sub sqldb {
	my $class = shift;
	if (not $sqldb or (not $sqldb->get_dbh())) {
		$sqldb = WisCon::SQLEngine::sqldb();
	}
	return $sqldb;
}


#################################################################
### Accessors

sub get_val {
	my $self = shift;
	my $col  = shift;
	return $self->{record}{$col};
}

sub set_val {
	my $self    = shift;
	my $col     = shift;
	my $new_val = shift;
	$self->{record}{$col} = $new_val;
}

sub id {
	my $self = shift;
	return $self->get_val('id' . $self->{'table'});
}

sub name {
	my $self = shift;
	my $namestring = 'Code';
	if ($self->{'table'} eq 'ProgramFormats') {
	    $namestring = 'Title';
	} elsif ($self->{'table'} eq 'ProgramTracks')  {
	    $namestring = 'Name';
	} elsif ($self->{'table'} eq 'RankingTypes')  {
	    $namestring = 'Description';
	}
	return $self->get_val($namestring);
}

######################################################################

1;
