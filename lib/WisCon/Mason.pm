package WisCon::Mason;

use strict;
use warnings;

use URI;
use HTML::Mason;

######################################################################

*HTML::Mason::Commands::build_url = \&WisCon::Mason::make_url;
*HTML::Mason::Commands::redirect = \&WisCon::Mason::redirect;

*HTML::Mason::Commands::html_escape = \&WisCon::Mason::html_escape;
*HTML::Mason::Commands::uri_escape = \&WisCon::Mason::uri_escape;

*HTML::Mason::Commands::csrf_token_field = \&WisCon::Mason::csrf_token_field;
*HTML::Mason::Commands::csrf_token_is_required = \&WisCon::Mason::csrf_token_is_required;

######################################################################

sub make_url {
	my $link = shift;
	$link =~ s/(index)?[.]mhtml$//;
	if ( scalar @_ ) {
		$link = URI->new( $link );
		$link->query_form( @_ ) 
	}
	return "$link";
}

######################################################################

sub send_redirect {
	my ( $location, $status ) = @_;

	$HTML::Mason::Commands::r->status( $status || 302 );
	$HTML::Mason::Commands::m->redirect( $location );
}

sub redirect {
	send_redirect( make_url( @_ ), 302 );
}

sub permanent_redirect {
	send_redirect( make_url( @_ ), 301 );
}

######################################################################

sub html_escape {
	HTML::Mason::Escapes::html_entities_escape(\$_[0]);
}

use URI::Escape;

######################################################################

# In a form:
#   <% csrf_token_field() |n %>

# In a link:
#   <a href="/page?action=foobar&<% csrf_token_param() |n %>">Do It</a>

# In action handler:
#    csrf_token_is_required();

sub csrf_param_name {
	'_auth'
}

sub csrf_token_field {
	qq{<input type="hidden" name="} . html_escape( csrf_param_name() ) . qq{" value="} . html_escape( csrf_get_token() ) . qq{">}
}

sub csrf_token_param {
	csrf_param_name() . '=' . csrf_get_token()
}

sub csrf_token_info {
	csrf_param_name() => csrf_get_token()
}

sub csrf_token_is_required {
	csrf_check_token() or csrf_punish_failure()
}

sub csrf_submitted_token {
	$HTML::Mason::Commands::m->top_args()->{ csrf_param_name() }
}

sub csrf_check_token {
	csrf_submitted_token() eq csrf_current_token()
}

sub csrf_punish_failure {
	warn "CSRF Failure: ", csrf_submitted_token(), ' vs ', csrf_current_token();
	redirect( '/error', msg => 'A server-side action was invoked without the necessary authorization. Please notify the site administrator and provide the address of the form or link you just clicked on so we can fix it. (CSRF token is ' . ( csrf_submitted_token() ? 'incorrect' : 'missing' ) . '.)' )
}

sub csrf_current_token {
	WisCon::Session::session()->{ _csrf_token }
}

sub csrf_get_token {
	WisCon::Session::session()->{ _csrf_token } ||= csrf_generate_token()
}

use Apache::Session::Generate::MD5;

sub csrf_generate_token {
	Apache::Session::Generate::MD5::generate()
}

1;
