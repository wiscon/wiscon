package WisCon::Authentication;

use strict;

use Digest::MD5 qw( md5_hex );

use WisCon::Session;
use WisCon::SQLEngine;

sub session { &WisCon::Session }

######################################################################

sub attempt_authentication {
	my ( $email, $password, $remember ) = @_;
	
	if ( ! $email and ! $password ) {
		return "Please enter your email address and password"
	}
	
	my $user = WisCon::SQLEngine->sqldb()->fetch_one( 
		table => 'Users', 
		where => { ProfileEmail => $email },
	);
	
    if ( ! $user or $user->{ProfilePassword} ne md5_hex( $password ) ) {
		return "The requested email and password combination could not be found.";
    }

	return $user;
}

sub process_authentication {
	my ( $email, $password, $remember ) = @_;
	my $result = attempt_authentication( $email, $password, $remember );
	if ( ! ref $result ) {
		return $result;
	} else {
		my $user = $result;
		my $session = session();
		
		$session->{ "isLoggedIn" } = 1;
		$session->{ "id" } = $user->{idUsers};
		$session->{ "email" } = $email;
		$session->{ "rememberMe" } = $remember;
	
		return $user;
	}
	
}

######################################################################

sub log_out {
	my $session = session();
	
	$session->{ "isLoggedIn" } = 0;
	$session->{"id"} = undef unless $session->{"rememberMe"};
	$session->{"email"} = undef unless $session->{"rememberMe"};
}

######################################################################

sub is_logged_in {
	my $session = session()
	  or return;
	
    $session->{isLoggedIn}
}

sub current_user_id {
	my $session = session()
	  or return;
	
	$session->{isLoggedIn} 
	  or return;
    
	$session->{id}
}

#  returns a Person object.

sub current_user {
	my $session = session()
	  or return;
	
	$session->{isLoggedIn} 
	  or return;
    
	WisCon::Person->find_user(current_user_id());
}

sub current_user_email {
	my $session = session()
	  or return;
    
	$session->{isLoggedIn} 
	  or return;
    
	$session->{email}
}

######################################################################

sub roles_for_user_id {
	my ( $user_id, $current, @named_roles ) = @_;
	
	my $roles = WisCon::SQLEngine->sqldb()->fetch_select( 
		table => 'Roles',
		columns => '*, ( StartDate <= now() and ( EndDate >= now() or EndDate is null or EndDate = "0000-00-00 00:00:00" ) ) as is_current',
		where => {
			idUsers => $user_id,
			( ( scalar @named_roles ) ? ( RoleType => \@named_roles ) : () ),
		},
		order => 'is_current desc, StartDate desc, EndDate desc',
		having => {
			( ( defined $current ) ? ( is_current => $current ) : () ),
		}
	);
}

sub current_user_must_have_role {
	my ( $role ) = @_;
	
	my $user_id = current_user_id();
	
	if ( ! $user_id ) {
		WisCon::Mason::redirect( '/account/login', msg => 'You must log in to access this feature' );
	} elsif ( ! scalar @{ roles_for_user_id( $user_id, 1, $role ) } ) {
		WisCon::Mason::redirect( '/msg', msg => 'You do not have permission to access this feature' );
	} else {
		return 1;
	}
	
}

sub current_user_has_role {
	my ( $role ) = @_;
	my $user_id = current_user_id();
	
	(( $user_id ) && (  scalar @{ roles_for_user_id( $user_id, 1, $role ) } )) ? 1 : 0;
}

sub current_user_may_login_as {
	my $user_id = current_user_id();
	return () unless $user_id;
	my $persons = WisCon::Authorization::persons_for_user_id($user_id);
	if (scalar @$persons) {
		return map($_->{idPersons}, @$persons);
	}
	return ();
}

######################################################################

1;
