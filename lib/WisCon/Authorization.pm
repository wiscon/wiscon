package WisCon::Authorization;

use strict;

use WisCon::SQLEngine;

######################################################################

sub tables_for_user_id {
	my ( $user_id ) = @_;
	# first lets check to see if anyone over whom input user has family or other authority has a table
	my $persons = persons_for_user_id($user_id);
	my @tables;
	if (scalar @$persons) {
		foreach (@$persons) {
			my $other_person = WisCon::Person->fetch($_->{'idPersons'});
			my $has_table = $other_person->has_table;
			if ($has_table) {
				push @tables, $has_table;
			}
		}
	}
	# now lets see if input user own person (identity relationship) has a table
	my $person = WisCon::Person->find_user( $user_id );
	my $has_table = $person->has_table;
	if ($has_table) {
		push @tables, $has_table;
	}
	return @tables;
}

sub persons_for_user_id {
	my ( $user_id ) = @_;
	
	my $authorities = WisCon::SQLEngine->sqldb()->fetch_select( 
		table => 'UserAuthority',
		columns => '*, ( StartTimestamp <= now() and ( EndTimestamp >= now() or EndTimestamp is null or EndTimestamp = "0000-00-00 00:00:00" ) ) as is_current',
		where => {
			idUsers => $user_id,
			AuthorityCode => [ 'other', 'family' ],
		},
		order => 'is_current desc, StartTimestamp desc, EndTimestamp desc',
		having => {  is_current => 1 }
	);
}

######################################################################

1;
