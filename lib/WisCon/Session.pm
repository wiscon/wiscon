package WisCon::Session;

use strict;
use warnings;

use Apache;
use Apache::Cookie;
use Apache::Session::File;

######################################################################

{
	package WisCon;
	use vars qw( %Session );
	sub Session { &WisCon::Session::session }
}

######################################################################

sub session {
	unless ( $WisCon::Session{ _session_id } ) {
		establish_session()
	}
	
	if ( ! scalar @_ ) {
		\%WisCon::Session
	} elsif ( scalar @_ == 1 ) {
		if ( ! ref( $_[0] ) ) {
			$WisCon::Session{ $_[0] }
		} else {
			@WisCon::Session{ @{ $_[0] } }
		}
	} else {
		while ( @_ ) {
			$WisCon::Session{ shift } = shift
		}
	}
}

######################################################################

sub establish_session {
	
	my $r = Apache->request();
	
	my %c = Apache::Cookie->fetch;
	my $session_id = exists $c{wiscon_session} ? $c{wiscon_session}->value : undef;
 
	eval {
		# 2009-03-11 Simon: if the directory doesn't exist, create it.
		my $session_dir = '/tmp/sessions';
		( -e $session_dir ) or mkdir( $session_dir );
	    tie %WisCon::Session, 'Apache::Session::File', $session_id, {
			Directory => $session_dir,
			LockDirectory => $session_dir,
	    };
	};
 	
	if ( $@ and $@ !~ /Object does not exist/ ) {
	    # die $@; # return error
	    die $@; 
		# 2009-03-11 Simon: made this fatal; if session is broken, don't hide it
	}
	
	Apache::Cookie->new( $r,
		name => 'wiscon_session',
		value => $WisCon::Session{_session_id},
		path => '/',
		expires => '+1d',
	)->bake;
	
	$r->register_cleanup( \&release_session );
	
	return;
}

sub release_session {
	untie %WisCon::Session;
}

######################################################################

1;

=head1 NAME

WisCon::Session - Set up Apache::Session for WisCon


=head1 SYNOPSIS

Store information for the current visitor in either of these ways:

  WisCon::Session->{ 'keyname' } = 'value';

  WisCon::Session( 'keyname' => 'value' );

Later, perhaps on another page, retreive it in either of these ways:

  $value = WisCon::Session->{ 'keyname' };

  $value = WisCon::Session( 'keyname' );


=head1 REFERENCE

The first time you call WisCon::Session() within a particular request, it calls establish_session, which arranges to have release_session called at the end of the current request.


=cut
