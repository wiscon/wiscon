package WisCon::Assignment;

=pod

A class representing the assignment of a person to a program item.  
This includes both successful and unsuccessful assignments.  

=cut

use strict;
use warnings;
use vars qw( $AUTOLOAD );
use Carp;
use Data::Dumper;
use WisCon::DateTime;
use WisCon::Person;
use WisCon::ProgramItem;
use WisCon::SQLEngine;

my $sqldb = WisCon::SQLEngine::sqldb();

my $status_recs = $sqldb->fetch_select(sql => "select Code from RefAssignmentStatus");

my @states = [];
my %valid_states = ();
for (@$status_recs) {
    my $status = $_->{Code};
    $states[++$#states] = $status;
    $valid_states{$status} = 1;
}

my $type_recs = $sqldb->fetch_select(sql => "select Code from RefAssignmentTypes");

my @types;
my %valid_types = ();
for (@$type_recs) {
    my $type = $_->{Code};
    $types[++$#types] = $type;
    $valid_types{$type} = 1;
}

my %field_def = ( item => "object", 
		  person => "object", 
		  type => join("|", @types), 
		  success => "0|1",
		  freelance => "0|1");


my %period_of_id = ();

#################################################################
### Constructors and factory methods

=pod

=item ->new(item => $item, person => $person, type => "participant")

Creates a new, empty Assignment object.  It requires a ProgramItem 
object argument.  Optional arguments include person which takes 
a Person object, and type which takes a type string of 
"participant" (default) or any of the types defined in RefAssignmentTypes.  

=cut

sub new {
	my $class = shift;
	my %args  = @_;
	if ($args{item} =~ m/^\d+$/) {
		$args{item} = WisCon::ProgramItem->fetch(id=>$args{item});
	}
	die "No item object" unless (ref($args{item}));
	for (keys %args) { 
		die "Unknown new assignment key '$_'" unless ($field_def{$_});
	}
	die "Invalid person for new assignment" if ($args{person} and not ref($args{person}));
	$args{type} ||= "participant";
	my $self  = { %args };
	bless $self, $class;
	return $self;
}

#################################################################
### Utility class methods

=pod

=item ->is_valid_status("status")

Returns 1 if the specified string is a valid assignment status,
otherwise undefined;

=cut

sub is_valid_status {
    my $class = shift;
    my $status = shift;

    my $valid = $valid_states{$status};

    return $valid;
}

=pod

=item ->is_valid_type("type")

Returns 1 if the specified string is a valid assignment type,
otherwise undefined;

=cut

sub is_valid_type {
    my $class = shift;
    my $type = shift;

    my $valid = $valid_types{$type};

    return $valid;
}

=pod

=item ->load_periods()

Loads data on the intersection of hardcoded times (used in the 
PersonAvailability table) and TimePeriods (used in scheduling), 
finding intersections.  

=cut

sub load_periods {
	my $class = shift;

	my @times = WisCon::DateTime->times();
	my $tmin = '05:00:00'; # treat times before this as next day

	# first load the basic period data
	my @periods = @{ $sqldb->fetch_select(sql=>['select *,time(StartDateTime) as start,time(EndDateTime) as end from TimePeriods where idConventions=? order by StartDateTime', WisCon::Convention->current->id]) };

	# now fill in with the proper exclusion flags for each period
	for my $period (@periods) {
		$period_of_id{ $period->{idTimePeriods} } = $period;
		$period->{flags} = [];
		my ($p_start,$p_end) = ($period->{start},$period->{end});
		$p_start =~ s/^(\d\d):/($1+24).":"/e if (($p_start cmp $tmin) < 0);
		$p_end =~ s/^(\d\d):/($1+24).":"/e if (($p_end cmp $tmin) < 0);
		for my $time (@times) {
			my ($t_start,$t_end) = ($time->{start},$time->{end});
			$t_start =~ s/^(\d\d):/($1+24).":"/e if (($t_start cmp $tmin) < 0);
			$t_end =~ s/^(\d\d):/($1+24).":"/e if (($t_end cmp $tmin) < 0);
			unless ( (($t_start cmp $p_end) >= 0) or (($t_end cmp $p_start) <= 0) ) {
				push(@{ $period->{flags} }, $time->{flag});
			}
		}
	}
	# key of the PersonAvailability column code, value of item name
	my %special = ("Gathering" => "The Gathering", 
		       "Workshop" => "Writer's Workshop",
		       "Auction" => "Tiptree Auction",
		       "Signout" => "The SignOut",
		       "GOH" => "Guest of Honor Speeches and Tiptree Ceremony");
	for my $key (keys %special) {
		my $flag = "Exclude" . $key . "Flag";
		my $specials = $sqldb->fetch_select(sql=>["select TimePeriods.* from ProgramItems, Slots, TimePeriods where ProgramItems.idSlots  = Slots.idSlots and Slots.idTimePeriods = TimePeriods.idTimePeriods and ProgramItems.ItemName = ? and ProgramItems.idConventions=?",$special{$key}, WisCon::Convention->current->id]);
		for my $special (@$specials) {
			for my $period (@periods) {
				unless ( (($special->{StartDateTime} cmp $period->{EndDateTime}) >= 0) or (($special->{EndDateTime} cmp $period->{StartDateTime}) <= 0) ) {
					push(@{ $period->{flags} }, $flag);
				}
			} # end of for my $period (@periods)
		} # end of for my $special (@$specials)
	} # end of for my $key (keys %special)
} # end of sub load_periods

sub period_of_id {
	my $class = shift;
	my $id = shift;
	$class->load_periods unless (%period_of_id);
	return $period_of_id{$id};
}

=pod

=item ->check_availability(person=>$person,item=>$item)

Returns 1 if the person is available for the given scheduled item.  

=cut

sub check_availability {
	my $class = shift;
	my @exclusions = $class->check_exclusions(@_);
	return (@exclusions) ? 0 : 1;
}

=pod

=item ->check_exclusions(person=>$person,item=>$item)

Returns an array of the exclusions that fail for the given combination 
of person and scheduled item.  

=cut

sub check_exclusions {
	my $class = shift;
	my %args = @_;
	my $person = $args{person};
	ref($person) or die "No person arg";
	my $item = $args{item};
	ref($item) or die "No item arg";
	if (not $item->get_val('idSlots')) {
		warn "Exclusions of unscheduled item ".$item->id;
		return ();
	}

	my $data = $sqldb->fetch_select(table=>'PersonAvailability',where=>{idPersons=>$person->id, idConventions=>WisCon::Convention->current->id});
	die "Multiple Availability for idPerson=".$person->id if (@$data > 1);
	my $availability = shift(@$data);

	my $idSlots = $item->get_val('idSlots');
	my $timedata = $sqldb->fetch_one(sql=>["select TimePeriods.idTimePeriods from Slots,TimePeriods where Slots.idTimePeriods = TimePeriods.idTimePeriods and Slots.idSlots=?",$item->get_val('idSlots')]);
	my $idTimePeriods = $timedata->{idTimePeriods};
	my $period = $class->period_of_id($idTimePeriods) or die;

	my @exclusions = ();
	for (@{ $period->{flags} }) {
		push(@exclusions,$_) if ($availability->{$_});
	}
	
	push(@exclusions,"ArrivalDate") if ($availability->{ArrivalDate} and ($period->{EndDateTime} cmp $availability->{ArrivalDate}) < 0);
	push(@exclusions,"DepartureDate") if ($availability->{DepartureDate} and ($period->{StartDateTime} cmp $availability->{DepartureDate}) > 0);
	return @exclusions;
}

=pod

=item ->auto_assign_all_items

This does auto-assignment of participants and moderators for 
all possible items.  
...

=cut

sub auto_assign_all_items {
	my $class = shift;
	my %args = @_;
	for (keys %args) { die "Invalid arg '$_'" unless (m/^stop_before$/); }
	my $order = WisCon::ProgramItem->param('assignment_order');
	my @items = WisCon::ProgramItem->all_auto_assigned();
	my @assignments;

	if ($order eq "par-mod") {
		for my $item (@items) {
			last if ($args{stop_before} == $item->id);
			my @participants = WisCon::Assignment->auto_assign_participants(item=>$item);
			my @moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
			push(@assignments, @participants, @moderators);
		}
	} elsif ($order eq "all-mod-all-par") {
		for my $item (@items) {
			my @moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
			push(@assignments, @moderators);
		}
		for my $item (@items) {
			my @participants = WisCon::Assignment->auto_assign_participants(item=>$item);
			push(@assignments, @participants);
		}
	} elsif ($order eq "all-par-all-mod") {
		for my $item (@items) {
			my @participants = WisCon::Assignment->auto_assign_participants(item=>$item);
			push(@assignments, @participants);
		}
		for my $item (@items) {
			my @moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
			push(@assignments, @moderators);
		}
	} elsif ($order eq "mod-par") {
		for my $item (@items) {
			last if ($args{stop_before} == $item->id);
			my @moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
			my @participants = WisCon::Assignment->auto_assign_participants(item=>$item);
			push(@assignments, @moderators, @participants);
		}
	} else {
		die "Unrecognized order parameter '$order'";
	}
	$class->assign_freelance_moderators(@assignments);
	return @assignments;
}

=pod

=item ->assign_freelance_moderators(@assignments)

This assigns freelance moderators - i.e. users who expressed interest in 
freelance moderation rather than interest in a particular item.  

=cut

sub assign_freelance_moderators {
	my $class = shift;
	my $free_id = WisCon::ProgramItem->param('freelance_moderator_item_id');
	
	return undef if ($free_id =~ m/^(none|undef|null|)$/);
	die "Invalid freelance_moderator_item_id " unless ($free_id =~ m/^\d+$/);

	my $max_assignments = WisCon::ProgramItem->param('max_assignments');
	my $freelance = WisCon::ProgramItem->fetch(id=>$free_id);
	die "Freelance moderator item not found" unless ($freelance);
	
	my @mods = map { WisCon::Person->find_user($_) } $freelance->interested_participants;
	@mods = grep { $_->num_assignments < $max_assignments } @mods;
	@mods = sort { $freelance->user_interest($a) <=> $freelance->user_interest($b) } @mods;
	warn "Trying to use ".scalar(@mods)." freelance moderators";

	my $count = 0;
	for my $assignment (grep { ($_->type eq "moderator") and (not $_->success) } @_) {
		die "Invalid argument" unless (ref($assignment) =~ m/Assignment/);
		my $next = undef;
		for (@mods) {
			if (($_->num_assignments < $max_assignments) and not $assignment->item->is_assigned($_)) {
				$next = $_;
				last;
			}
		}
		if ($next) {
			warn "Assigning freelance mod ".$next->id." to item ".$assignment->item->id;
			my $success = $next->assign(item=>$assignment->item,type=>"moderator");
			$assignment->person($next);
			$assignment->success( $success ? 1 : 0 );
			die "Failed freelance assignment" if (not $success);
			$assignment->freelance(1);
			$count++ if ($success);
		}
	} # end of for my $assignment (...)
	warn "Made $count freelance moderator assignments";
	return $count;
}

=pod

=item ->auto_assign_participants(item=>$item)

This does auto-assignment of participants for a given item.
...

=cut

sub auto_assign_participants {
	my $class = shift;
	my %args = @_;
	my $item = (ref($args{item})) ? $args{item} : WisCon::ProgramItem->fetch(id=>$args{item});
	die "No item object" unless (ref($item));
	my $max_assignments = WisCon::ProgramItem->param('max_assignments');

	my @possibles = map { WisCon::Person->find_user($_) } $item->interested_participants();

	# Within a level of interest, assign people by how many items they 
	# are already signed up for.  
	@possibles = grep { $_->num_assignments < $max_assignments and not $item->is_assigned($_) } @possibles;
	@possibles = sort { $item->participant_priority($a) <=> $item->participant_priority($b) } @possibles;
	my $slots = $item->open_participant_slots();
	my @assignments = ();
	for (1 .. $slots) {
		my $assignment = $class->new(item=>$item);
		push(@assignments, $assignment);
		if (@possibles) {
			my $next = shift(@possibles);
			my $success = $next->assign(item=>$item);
			$assignment->person($next);
			$assignment->success( $success ? 1 : 0 );
		}
	}
	return @assignments;
}

=pod

=item ->auto_assign_moderators(item=>$item)

This does auto-assignment of moderator(s) for a given item.
...

=cut

sub auto_assign_moderators {
	my $class = shift;
	my %args = @_;
	my $item = (ref($args{item})) ? $args{item} : WisCon::ProgramItem->fetch(id=>$args{item});
	die "No item object" unless (ref($item));
	my $max_assignments = WisCon::ProgramItem->param('max_assignments');

	if ($item->open_moderator_slots()) {
		my $assignment = $class->new(item=>$item,type=>"moderator");
		# Now select a moderator
		my @mods = map { WisCon::Person->find_user($_) } $item->interested_moderators();
		@mods = grep { $_->num_assignments < $max_assignments and not $item->is_assigned($_) } @mods;
		@mods = sort { $item->moderator_priority($a) <=> $item->moderator_priority($b) } @mods;
		my $next = shift(@mods);
		if ($next) {
			my $success = $next->assign(item=>$item,type=>"moderator");
			die "Too many assignments" if ($next->num_assignments > $max_assignments);
			$assignment->person($next);
			$assignment->success( $success ? 1 : 0 );
		}
		return ($assignment);
	} else {
		return ();
	} # end of if ($item->open_moderator_slots()) ... else ...
}

#################################################################
### Instance methods

=pod

=item ->success()

Returns 1 if the item is a success.  Can be called with an argument 
to set the success parameter.  

=cut

=pod

=item ->type()

Returns the assignment type, i.e. "participant" or "moderator" or "other". 

=cut

=cut

=pod

=item ->freelance()

Returns whether the assignment was freelance (i.e. based on interest in 
freelance participation rather than interested in the specific item).  

=cut

sub AUTOLOAD {
    my $self = shift;
    my $attr = $AUTOLOAD;
    $attr =~ s/.*:://;
    return if ($attr =~ m/^DESTROY$/);
    die "Unknown assignment field '$attr'" unless ($field_def{$attr});
    if (@_) {
	    my $value = shift;
	    die "Invalid $attr object" if (($field_def{$attr} eq "object") and not ref($value));
	    die "Invalid $attr value '$value'" if (($field_def{$attr} =~ m/\|/) and ($value !~ m/^$field_def{$attr}$/));
	    $self->{$attr} = $value;
    }
    return $self->{$attr};
}

######################################################################

1;
