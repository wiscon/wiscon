package WisCon::WebFormBuilder;

use strict;

sub html_escape { &HTML::Mason::Commands::html_escape }

######################################################################

sub stylesheet_include {
	qq{
		<style>
			form.data-entry { margin: 0; }
			.typical-input-field { width: 400px; }
			.error-msg { color: red; }
			.footer-submit-buttons { text-align: right; }
		</style>
		
	}
}

sub html_tag {
	my ( $name, $attrs ) = @_;
	
	join( '', '<', $name, ( map { 
		' ' . ( defined $attrs->{$_} ? $_ . '="' . html_escape( $attrs->{$_} ) . '"' : () ) 
	} sort keys %$attrs ), '>' )
}

sub input_field {
	my ( $name, $value, $type, $options ) = @_;
	
	$type ||= 'text';
	$options ||= {};
	
	if ( $type eq 'textarea' ) {
		html_tag( 'textarea', { 
			name => $name, 
			placeholder => $options->{placeholder}, 
			rows => $options->{rows}, 
			cols => $options->{cols} 
		} ) . html_escape( $value ) . qq{</textarea>}
	} else {
		html_tag( 'input', { 
			type => $type, 
			name => $name, 
			value => $value, 
			placeholder => $options->{placeholder}, 
			size => $options->{size} 
		} ) 
	}
}

sub select_from_list {
	my ( $name, $current, $list, $options ) = @_;
	
	my @list = @$list;
	
	$options ||= {};
	$options->{value} ||= 'id';
	$options->{label} ||= 'name';
	
	if ( $options->{pick_one} ) {
		unshift @list, { 
			$options->{ value } => '', 
			$options->{ label } => $options->{pick_one}, 
		},
	}
	
	unless ( $options->{do_not_preserve_current} ) {
		unless ( grep { $current eq $_->{ $options->{ value } } } @list ) {
			push @list, { 
				$options->{ value } => $current, 
				$options->{ label } => $current 
			},
		}
	}
	
	qq{<select name="} . html_escape( $name ) . qq{">} . 
	join( ' ', map {
		my $value = $_->{ $options->{ value } };
		my $label = $_->{ $options->{ label } };
		qq{<option value="} . html_escape( $value ) . qq{"} . ($value eq $current ? ' selected' : '') . qq{>} . 
			html_escape( $label ) . 
		qq{</option>}
	} @list ) .
	qq{</select>}
}

sub error_msg_if_any {
	my $msg = shift;
	
	$msg =~ /\S/ 
		or return '';
	
	return qq{<div class="error-msg">} . $msg . qq{</div>}
	
}

######################################################################

1;
