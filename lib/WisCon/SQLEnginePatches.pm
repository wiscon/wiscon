package WisCon::SQLEnginePatches;

use strict;
use warnings;
no warnings 'redefine';

use DBIx::SQLEngine;
use DBIx::SQLEngine::Driver;

######################################################################

package DBIx::SQLEngine::Driver::Default;

# Patch to modify the version of retrieve_columns distributed with SQLEngine.
# By default, the retrieve_columns function forces column names to lowercase.
# This patch removes that behavior.

sub retrieve_columns_name_key { 'NAME' }

sub retrieve_columns {
  my ($self, $sth) = @_;
  
  my $type_defs = $self->column_type_codes();
  my $names = $sth->{ $self->retrieve_columns_name_key() };

  my $types = eval { $sth->{'TYPE'} || [] };
  # warn "Types: " . join(', ', map "'$_'", @$types);
  my $type_codes = [ map { 
	my $typeinfo = scalar $self->type_info($_);
	# warn "Type $typeinfo";
	ref($typeinfo) ? scalar $typeinfo->{'DATA_TYPE'} : $typeinfo;
  } @$types ];
  my $sizes = eval { $sth->{PRECISION} || [] };
  my $nullable = eval { $sth->{'NULLABLE'} || [] };
  [
    map {
      my $type = $type_defs->{ $type_codes->[$_] || 0 } || $type_codes->[$_];
      $type ||= 'text';
      # warn "New col: $names->[$_] ($type / $types->[$_] / $type_codes->[$_])";
      
      {
	'name' => $names->[$_],
	'type' => $type,
	'required' => ! $nullable->[$_],
	( $type eq 'text' ? ( 'length' => $sizes->[$_] ) : () ),
	
      }
    } (0 .. $#$names)
  ];
}

######################################################################

# Adds "having".
sub sql_select {
  my ( $self, %clauses ) = @_;

  my $keyword = 'select';
  my ($sql, @params);

  if ( my $named = delete $clauses{'named_query'} ) {
    my %named = $self->interpret_named_query( ref($named) ? @$named : $named );
    %clauses = ( %named, %clauses );
  }

  if ( my $action = delete $clauses{'action'} ) {
    confess("Action mismatch: expecting $keyword, not $action query") 
	unless ( $action eq $keyword );
  }

  if ( my $union = delete $clauses{'union'} ) {
    if ( my ( $conflict ) = grep $clauses{$_}, qw/sql table tables columns/ ) { 
      croak("Can't build a $keyword query using both union and $conflict args")
    }
    ref($union) eq 'ARRAY' or 
      croak("Union clause must be a reference to an array of hashes or arrays");
    
    $clauses{'sql'} = [ $self->sql_union( @$union ) ]
  } 

  if ( my $literal = delete $clauses{'sql'} ) {
    if ( my ($conflict) = grep $clauses{$_}, qw/distinct table tables columns/){ 
      croak("Can't build a $keyword query using both sql and $conflict clauses")
    }
    ($sql, @params) = ( ref($literal) eq 'ARRAY' ) ? @$literal : $literal;
  
  } else {
    
    if ( my $distinct = delete $clauses{'distinct'} ) {
      $keyword .= " distinct";
    } 
    
    my $columns = delete $clauses{'columns'} || delete $clauses{'column'};
    if ( ! $columns ) {
      $columns = '*';
    } elsif ( ! ref( $columns ) and length( $columns ) ) {
      # should be one or more comma-separated column names
    } elsif ( UNIVERSAL::can($columns, 'column_names') ) {
      $columns = join ', ', $columns->column_names;
    } elsif ( ref($columns) eq 'ARRAY' ) {
      $columns = join ', ', @$columns;
    } elsif ( ref($columns) eq 'HASH' ) {
      $columns = join ', ', map { "$_ as $columns->{$_}" } sort keys %$columns;
    } else {
      confess("Unsupported column spec '$columns'");
    }
    $sql = "$keyword $columns";
    
    my $tables = delete $clauses{'tables'} || delete $clauses{'table'};
    if ( ! $tables ) {
      confess("You must supply a table name if you do not use literal SQL or a named query");
    } elsif ( ! ref( $tables ) and length( $tables ) ) {
      # should be one or more comma-separated table names
    } elsif ( UNIVERSAL::can($tables, 'table_names') ) {
      $tables = $tables->table_names;
    } elsif ( ref($tables) eq 'ARRAY' ) {
      ($tables, my @join_params) = $self->sql_join( @$tables );
      push @params, @join_params;
    } elsif ( ref($tables) eq 'HASH' ) {
      ($tables, my @join_params) = $self->sql_join( $tables );
      push @params, @join_params;
    } else {
      confess("Unsupported table spec '$tables'");
    }
    $sql .= " from $tables";
  }
  
  if ( my $criteria = delete $clauses{'criteria'} || delete $clauses{'where'} ){
    ($sql, @params) = $self->sql_where($criteria, $sql, @params);
  }
  
  if ( my $group = delete $clauses{'group'} ) {
    if ( ! ref( $group ) and length( $group ) ) {
      # should be one or more comma-separated column names or expressions
    } elsif ( ref($group) eq 'ARRAY' ) {
      $group = join ', ', @$group;
    } else {
      confess("Unsupported group spec '$group'");
    }
    if ( $group ) {
      $sql .= " group by $group";
    }
  }
  
  if ( my $criteria = delete $clauses{'having'} ){
	my ( $hsql, @hparams ) = $self->sql_where($criteria);
	$hsql =~ s/where/having/;
	$sql .= ' ' . $hsql;
	push @params, @hparams;
  }
  
  if ( my $order = delete $clauses{'order'} ) {
    if ( ! ref( $order ) and length( $order ) ) {
      # should be one or more comma-separated column names with optional 'desc'
    } elsif ( ref($order) eq 'ARRAY' ) {
      $order = join ', ', @$order;
    } else {
      confess("Unsupported order spec '$order'");
    }
    if ( $order ) {
      $sql .= " order by $order";
    }
  }
  
  my $limit = delete $clauses{limit};
  my $offset = delete $clauses{offset};
  if ( $limit or $offset) {
    ($sql, @params) = $self->sql_limit($limit, $offset, $sql, @params);
  }
  
  if ( scalar keys %clauses ) {
    confess("Unsupported $keyword clauses: " . 
      join ', ', map "$_ ('$clauses{$_}')", keys %clauses);
  }
  
  $self->log_sql( $sql, @params );
  
  return( $sql, @params );
}

######################################################################


1;
