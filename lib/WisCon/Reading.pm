package WisCon::Reading;

=pod

Utility methods for reading proposals.

=cut

use Carp;
use Data::Dumper;
use strict;
use warnings;
use WisCon::DateTime;
use WisCon::ProgramItem;
use WisCon::SQLEngine;

#################################################################
### Fields

my $sqldb = undef;

#################################################################
### Constructors and factory methods

=pod

=item ->new(record => $record)

Creates a new, empty Reading object with the specified database record.

=cut

sub new {
    my $class = shift;
    my %args  = @_;
    my $self  = { record => $args{record}, persons => []};
    bless $self, $class;

    my $type = $self->get_val('ProgramItemType');
    if ($type) {
	my $item_id = $self->get_val('idItems');
        die "Item $item_id is not a reading: expected type 2, was $type" unless ($type == 2);
    }
    
    $self->set_val( 'ProgramItemType', 2); # TODO __DAVID__ Types from DB
    
    return $self;
}


=pod

=item ->fetch($item_id)

Factory method; Reads the reading with the specified ID from the database.

=cut

sub fetch {
	my $class = shift;
	my ( $item_id ) = @_;

	# TODO __DAVID__ Include user ID in query
	#	my $user_id = WisCon::Authentication::current_user_id();
	#	unless ($user_id) {
	#		die "no user ID";
	#	}

	my $records = sqldb()->fetch_select(
		table => 'ProgramItems',
		where => { idItems => $item_id }
	);

	my $record = $records->[0];

	return WisCon::Reading->new( record => $record );
}

#################################################################
### Utility class methods

sub sqldb {
    my $class = shift;
    if (not $sqldb or (not $sqldb->get_dbh())) {
        $sqldb = WisCon::SQLEngine::sqldb();
    }
    return $sqldb;
}

#################################################################
### Accessors

sub get_val {
    my $self = shift;
    my $col  = shift;
    return $self->{record}{$col};
}

sub set_val {
    my $self    = shift;
    my $col     = shift;
    my $new_val = shift;
    $self->{record}{$col} = $new_val;
}

sub id {
	my $self = shift;
	return $self->get_val('idItems');
}

sub title {
    my $self = shift;
    return $self->get_val('ItemName');
}

sub set_title {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'ItemName', $new_val);
}

sub desc {
    my $self = shift;
    return $self->get_val('ItemDesc');
}

sub set_desc {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'ItemDesc', $new_val);
}

sub equipment {
    my $self = shift;
    return $self->get_val('EquipmentDesc');
}

sub set_equipment {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'EquipmentDesc', $new_val);
}

sub proposer {
    my $self = shift;
    return $self->get_val('idUserProposer');
}

sub set_proposer {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'idUserProposer', $new_val);
}

sub convention {
    my $self = shift;
    return $self->get_val('idConventions');
}

sub set_convention {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'idConventions', $new_val);
}

=pod
=item ->set_panelists("John, Paul, George, Ringo")
Sets suggested panelists as a string.
=cut
sub set_panelists {
    my $self = shift;
    my $new_val = shift;
    $self->set_val( 'SuggestedPanelists', $new_val);
}

=pod
=item ->set_persons($persons)
Sets suggested panelists as a list of Person objects
=cut
sub set_persons {
    my $self = shift;
    my $persons = shift;
    $self->{persons} = $persons;
}

sub persons {
    my $self = shift;

    my $assignments = sqldb()->fetch_select(
        table => 'Assignments',
        where => { idItems => $self->id() }
    );

    my @persons = ();
    foreach my $record (@$assignments) {
        my %assignment = %{$record}; 
 	my $person_id = $assignment{'idPersons'};
        my $person = WisCon::Person->fetch($person_id);
        push (@persons, $person);
    }

    return @persons;
}

#################################################################
### Instance methods

sub save {
    my $self = shift;
    my $record = $self->{record};

    my $sqldb = sqldb();

    ##############################
    # save reading request
    # warn Data::Dumper::Dumper($record);
    my $new_id = WisCon::SQLEngine::log_insert(
        $sqldb,
        table    => 'ProgramItems',
        sequence => 'idItems',
        values   => $record
    );

    ##############################
    # save requested assignments
    my $persons = $self->{persons};

    my $item = WisCon::ProgramItem->fetch(id=>$new_id);

    foreach my $person (@$persons) {
        $person->assign(item=>$item, type=>"participant", status=>"suggested");
    }

    return $new_id;
}

######################################################################

1;
