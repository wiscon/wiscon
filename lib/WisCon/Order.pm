package WisCon::Order;

use strict;
use warnings;
use Carp;
use Data::Dumper;
use WisCon::SQLEngine;

my $sqldb = WisCon::SQLEngine::sqldb();

sub fetch {
    my $class = shift;
	my $idOrders = shift;
    my $order = $sqldb->fetch_one( 
        table => 'Orders',
        where => { idOrders => $idOrders },
    );
	return undef unless $order;
    bless $order, $class;
}

sub fetch_id_by_transaction {
    my $class = shift;
	my $idFinancialTransactions = shift;
    my $order_ids = $sqldb->fetch_select(
		sql => [ "select distinct(OrderLines.idOrders) from FinancialTransactions 
					join Distributions on FinancialTransactions.idFinancialTransactions = Distributions.idFinancialTransactions
					join OrderLines on OrderLines.idOrderLines = Distributions.idOrderLines
					where FinancialTransactions.idFinancialTransactions=?", $idFinancialTransactions ]
	);
	return $order_ids->[0]->{idOrders} if scalar(@$order_ids);
	return undef;
}

sub fetch_id_by_orderline {
    my $class = shift;
	my $idOrderLines = shift;
    my $order_ids = $sqldb->fetch_select(
		sql => [ "select distinct(idOrders) from OrderLines
					where idOrderLines=?", $idOrderLines ]
	);
	return $order_ids->[0]->{idOrders} if scalar(@$order_ids);
	return undef;
}

sub id {
    my $order = shift;
    return $order->{idOrders};
}

sub counts_per_order {
	my $class = shift;
    my $idOrders = shift;
    my $idConventions = shift || WisCon::Convention->current->id;
    my $sql;
	$sql = "select CatalogItems.SaleItemName, count(OrderLines.idCatalog) as Counts from CatalogItems 
            left join OrderLines on CatalogItems.idCatalog = OrderLines.idCatalog
            where CatalogItems.idConventions= ? and (OrderLines.idOrders = ? or isnull(OrderLines.idOrders))
            group by CatalogItems.SaleItemName";
    my $lineitem_counts = 
		$sqldb->fetch_select(
			sql => [ $sql, $idConventions, $idOrders ]
		);
    return $lineitem_counts;
}

sub order_info {
    my $order = shift;
	my $orderinfo = $sqldb->fetch_select(
	  sql => ["select OrderLines.*, Distributions.DistributionDate, Distributions.DistributionAmt, Distributions.DistributionStatus, FinancialTransactions.idFinancialTransactions, 
	  FinancialTransactions.TransactionDate, FinancialTransactions.TransactionType, FinancialTransactions.TransactionAmt, FinancialTransactions.TransactionStatus,
	  FinancialTransactions.TransactionComment, FinancialTransactions.TransactionSource, FinancialTransactions.TransactionId, FinancialTransactions.idPersons as ftPerson
	  from (OrderLines left join Distributions on OrderLines.idOrderLines = Distributions.idOrderLines) 
					   left join FinancialTransactions on Distributions.idFinancialTransactions = FinancialTransactions.idFinancialTransactions
	  where idOrders=?", $order->id ],
	);
    return @$orderinfo;
}


1;
