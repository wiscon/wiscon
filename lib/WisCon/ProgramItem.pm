package WisCon::ProgramItem;

=pod

A class representing a program item.  

=cut

use Carp;
use Data::Dumper;
use strict;
use warnings;
use WisCon::SQLEngine;

# local database SQLEngine object
my $sqldb = undef;
# parameters from the database table `AutoAssignmentParameters`
my %params;
# a cache of user total interest, keyed on userID
my %user_total_interest;
# a cache of number of moderator flags, keyed on userID
my %user_mod_flags;

#################################################################
### Constructors and factory methods

=pod

=item ->all()

Returns an unordered list of all program items. 

=cut

sub all {
	my $class = shift;
	my %args = @_;
	my $records = $class->sqldb()->fetch_select(table => 'ProgramItems',
	      where=> { idConventions => WisCon::Convention->current->id} );
	my @all = map { WisCon::ProgramItem::->fetch(id=>$_->{idItems}) } @$records;
	return @all;
}

=pod

=item ->all_auto_assigned()

Returns a list of all program items that are targeted for automatic 
assignment of participants and/or moderators -- according to the 
parameters for selection in the AutoAssignmentParameters table. 

=cut

sub all_auto_assigned {
	my $class = shift;
	my @all = $class->all();
	my @auto = grep { $_->is_auto_assigned() } @all;
	my @sorted = sort { ($a->total_interest <=> $b->total_interest) or ($a->id <=> $b->id) } @auto;
	return @sorted;
}

=pod

=item ->fetch(id=>$item_id)

Factory method; Reads the item with the specified ID from the 
ProgramItems tables.

=cut

sub fetch {
	my $class = shift;
	my %args = @_;
	$args{id} and $args{id} =~ m/^\d+$/ or croak "No id argument";
	my $record = $class->sqldb()->fetch_one(table => 'ProgramItems',
						where => { 'idItems' => $args{id} });
	return undef unless ($record);
	my $self = WisCon::ProgramItem->new( record => $record );
	# set participants, moderators
	my $people = $sqldb->fetch_select( sql => ["select idPersons,AssignmentType from Assignments where Assignments.idItems=? and Assignments.AssignmentType<>'hidden'", $self->id] );
	my @mods = grep( {$_->{AssignmentType} eq 'moderator'} @$people);
	my @parts = grep( {$_->{AssignmentType} eq 'participant'} @$people);

	$self->{participants} = [ map {WisCon::Person->fetch($_->{idPersons})} @parts ];
	$self->{moderators} = [ map {WisCon::Person->fetch($_->{idPersons})} @mods ];
	return $self;
}

=pod

=item ->new(record => $record)

Creates a new, empty ProgramItem object with the specified database record.

This also reads from the ItemRankings and Assignments tables for 
user interest and assignments to this item.  

=cut

sub new {
	my $class = shift;
	my %args  = @_;
	my $record = $args{record};
	my $self  = { record => $record };
	bless $self, $class;

	$self->{num_participants} = ($record->{GeneralSignupFlag}) ? $class->param('default_num_participants') : 0;
	$self->{num_moderators} = ($record->{ModeratorSignupFlag}) ? 1 : 0;

	$self->{assigned_type} = {};
	$self->{assigned_status} = {};
	$self->{participants} = [];
	$self->{moderators} = [];

	# get list of users already assigned to this panel
	my $assignments = $class->sqldb()->fetch_select(sql => ['select idPersons,AssignmentType,AssignmentStatus from Assignments where idItems=?',$record->{idItems}]);
	for (@$assignments) {
		$self->register_assignment($_);
	}
	my $rankings = $class->sqldb()->fetch_select(sql => ["select idUsers,UserInterestRanking,ModeratorFlag from ItemRankings where idItems=? and UserInterestRanking < 9",$record->{idItems}]);
	$self->{interested_moderators} = [];
	$self->{participants_of_ranking} = [undef, [], [], [], [], [], [], [], [] ];
	$self->{ranking_of_user} = {};
	$self->{attendance} = $class->sqldb()->fetch_select(sql => ["select count(*) as count from ItemRankings where idItems=? and AttendFlag=1",$record->{idItems}])->[0]->{count};
	for (@$rankings) {
		my $rank = $_->{UserInterestRanking};
		die "Invalid rank '$rank'" if ($rank !~ m/^\d$/);
		
		push(@{ $self->{interested_moderators} },$_->{idUsers}) if ($_->{ModeratorFlag});
		push(@{ $self->{participants_of_ranking}->[$rank] },$_->{idUsers});
		$self->{ranking_of_user}->{$_->{idUsers}} = $rank;
	}

	return $self;
}

#################################################################
### Utility class methods

sub sqldb {
	my $class = shift;
	if (not $sqldb or (not $sqldb->get_dbh())) {
		$sqldb = WisCon::SQLEngine::sqldb();
	}
	return $sqldb;
}

sub param_row {
	my $proto = shift;
	my $name = shift;
	$proto->load_params() unless (%params);
	croak "Invalid auto-assignment parameter name '$name'" unless (exists($params{$name}));
	return $params{$name};
}

sub param {
	my $proto = shift;
	my $row = $proto->param_row(@_);
	return ($row) ? $row->{Value} : undef;
}

sub param_description {
	my $proto = shift;
	my $row = $proto->param_row(@_);
	return ($row) ? $row->{Description} : undef;
}

sub param_id {
	my $proto = shift;
	my $row = $proto->param_row(@_);
	return ($row) ? $row->{idParam} : undef;
}

sub params {
	my $proto = shift;
	$proto->load_params() unless (%params);
	my @rows = sort { $a->{idParam} <=> $b->{idParam} } values %params;
	my @names = map { $_->{Name} } @rows;
	return @names;
}

sub load_params {
	my $proto = shift;
	my $data = $proto->sqldb()->fetch_select(table=>'AutoAssignmentParameters');
	for (@$data) { $params{$_->{Name}} = $_; }
}

=pod

=item ->user_total_interest($idUsers)

Returns a number representing the weighted total of expressed interest 
by a given user ID in all item, equivalent to a number of people 
indicating maximum interest in it. 

=cut

sub user_total_interest {
	my $proto = shift;
	my $idUsers = shift;
	$idUsers =~ m/^\d+$/ or croak "Invalid idUsers argument '$idUsers'";
	if (exists($user_total_interest{$idUsers})) {
		return $user_total_interest{$idUsers};
	}
	
	# Rank 1 is weight param(max_interest_weight), Rank MAX is weight 1
	my $base = $proto->interest_weight_base;
	my $mult = $proto->interest_weight_multiplier;

	my $result = $proto->sqldb()->fetch_one(sql => ["select SUM($base-$mult*UserInterestRanking) as interest from ItemRankings where idItems in (select idItems from ProgramItems where idConventions=?) and idUsers=? and (UserInterestRanking < 9)",WisCon::Convention->current->id, $idUsers]);
	my $total = ($result) ? ($result->{interest} or 0) : 0;
	my $value = sprintf("%.1f", $total / $proto->param('max_interest_weight'));
	# store this is a cache of total_interest
	$user_total_interest{$idUsers} = $value;
	return $value;
}

=pod

=item ->user_mod_flags($idUsers)

Returns a number representing the weighted total of expressed interest 
by a given user ID in all item, equivalent to a number of people 
indicating maximum interest in it. 

=cut

sub user_mod_flags {
	my $proto = shift;
	my $idUsers = shift;
	$idUsers =~ m/^\d+$/ or croak "Invalid idUsers argument '$idUsers'";
	if (exists($user_mod_flags{$idUsers})) {
		return $user_mod_flags{$idUsers};
	}
	my $result = $proto->sqldb()->fetch_one(sql => ["select count(idUsers) as num from ItemRankings where idItems in (select idItems from ProgramItems where idConventions=?) and idUsers=? and ModeratorFlag > 0", WisCon::Convention->current->id, $idUsers]);
	my $num = ($result) ? ($result->{num} or 0) : 0;
	$user_mod_flags{$idUsers} = $num;
	return $num;
}

sub interest_weight_multiplier {
	my $proto = shift;
	return ($proto->param('max_interest_weight')-1)/($proto->param('num_interest_ranks')-1);
}

sub interest_weight_base {
	my $proto = shift;
	return 1 + $proto->param('num_interest_ranks')*$proto->interest_weight_multiplier();
}

#################################################################
### Instance methods

=pod

=item ->total_interest()

Returns a number representing the weighted total of expressed interest 
in this item, equivalent to a number of people indicating maximum 
interest in it. 

=cut

sub total_interest {
	my $self = shift;
	# Rank 1 is weight param(max_interest_weight), Rank MAX is weight 1
	my $base = $self->interest_weight_base;
	my $mult = $self->interest_weight_multiplier;

	my $total = 0;
	for my $rank (1 .. $self->param('num_interest_ranks')) {
		my $users = $self->{participants_of_ranking}->[$rank];
		my $weight = ($base - $mult*$rank);
		$total += scalar(@$users)*$weight;
	}
	return sprintf("%.1f", $total / $self->param('max_interest_weight'));
}

=pod

=item ->user_interest($person|$user_id)

Returns the interest code (1 for best) in this item for the specified
user.  The argument can either be a user ID number or a Person object.  

=cut

sub user_interest {
	my $self = shift;
	my $user = shift;
	my $idUsers;
	if (ref($user)) {
		$idUsers = $user->user_id or die "Invalid user argument ".Dumper($user);
	} else {
		$idUsers = $user;
	}
	$idUsers =~ m/^\d+$/ or croak "Invalid idUsers argument '$idUsers'";
	return $self->{ranking_of_user}->{$idUsers};
}

=pod

=item ->participant_priority($person)

Returns the priority number to be a participant on this item 
for the specified person.  This is an arbitrary number found by 
a weighting of user interest in the item, number of assignments 
that person has, and the total interest for that person.  
The argument must be a Person object.  

=cut

sub participant_priority {
	my $self = shift;
	my $person = shift;
	croak "Invalid person argument ".Dumper($person) unless (ref($person) =~ m/Person/);
	my $wt1 = $self->param('weight_item_interest'); 
	my $wt2 = $self->param('weight_num_assignments');
	my $wt3 = $self->param('weight_total_interest');
	my $base1 = $self->user_interest($person);
	my $base2 = $person->num_assignments();
	my $base3 = $person->total_interest();
	die "Undefined wt1" unless (defined($wt1));
	die "Undefined wt2" unless (defined($wt2));
	die "Undefined wt3" unless (defined($wt3));
	die "No user interest for ".$person->user_id." found in ".Dumper($self->{ranking_of_user}) unless (defined($base1));
	die "Undefined base2" unless (defined($base2));
	die "Undefined base3" unless (defined($base3));
	my $priority = $wt1*$base1 + $wt2*$base2 + $wt3*$base3;
	return $priority;
}

=pod

=item ->moderator_priority($person)

Returns the priority number to be a moderator on this item 
for the specified person.  This is an arbitrary number found by 
a weighting of user interest in the item, number of assignments 
that person has, and the total number of moderator flags that person
has marked in ranking.  The argument must be a Person object.  

=cut

sub moderator_priority {
	my $self = shift;
	my $person = shift;
	croak "Invalid person argument ".Dumper($person) unless (ref($person) =~ m/Person/);
	my $wt1 = $self->param('mod_weight_num_assignments');
	my $wt2 = $self->param('mod_weight_num_flags');
	my $wt3 = $self->param('mod_weight_item_interest');
	my $base1 = $person->num_assignments();
	my $base2 = $person->num_mod_flags();

	# the third base is an inversion of item interest
	# i.e. there is less moderator priority if the person wants 
	# to be on the panel as a participant
	my $n = $self->param('num_interest_ranks');
	my $i = $self->user_interest($person);
	my $base3 = ($i and $i < $n) ? ($n+1-$i) : ($n+1);

	die "Undefined wt1" unless (defined($wt1));
	die "Undefined wt2" unless (defined($wt2));
	die "Undefined wt3" unless (defined($wt3));
	die "Undefined base2" unless (defined($base1));
	die "Undefined base2" unless (defined($base2));
	die "Undefined base3" unless (defined($base3));
	my $priority = $wt1*$base1 + $wt2*$base2 + $wt3*$base3;
	return $priority;
}

=pod

=item ->register_assignment($values)

Registers an assignment of a person to this item.  This takes a 
hashref of values according to the Assignment.  

This does not save the assignment to the database - that is done 
in the WisCon::Person method "assign."  

=cut

sub register_assignment {
	my $self = shift;
	my $vals = shift;
	die "Invalid assignment ".Dumper($vals) unless (ref($vals) and $vals->{idPersons} =~ m/^\d+$/ and $vals->{AssignmentType} and $vals->{AssignmentStatus});
	if ($vals->{AssignmentType} eq "participant") {
		push(@{$self->{participants}},$vals->{idPersons});
	} elsif ($vals->{AssignmentType} eq "moderator") {
		push(@{$self->{moderators}},$vals->{idPersons});
	}
	$self->{assigned_type}->{$vals->{idPersons}} = $vals->{AssignmentType};
	$self->{assigned_status}->{$vals->{idPersons}} = $vals->{AssignmentStatus};
	return 1;
}


=pod

=item ->assigned_participants()

Returns an array reference to a list of idPersons for participants.  

=cut

sub assigned_participants {
	my $self = shift;
	my @list = @{ $self->{participants} };
	return @list;
}

=pod

=item ->assigned_moderators()

Returns an array reference to a list of idPersons for participants.  

=cut

sub assigned_moderators {
	my $self = shift;
	my @list = @{ $self->{moderators} };
	return @list;
}

=pod

=WisCon::ProgramItem::participant_text( $sqldb, $idItems, $use_admin_name)

Returns a string for displaying the participants.

=cut

sub participant_text {
  my $sqldb = shift;
  my $idItems = shift;
  my $self = WisCon::ProgramItem->fetch(id=>$idItems);
  my $name_func = shift || 'program_name';
  my $modtext = "";
  no strict "refs";
  $modtext = ('M: ' . join(', ', map { "WisCon::Person::$name_func"->($_) } (sort { WisCon::Person::sorting_name($a) cmp WisCon::Person::sorting_name($b) } @{$self->{moderators}})) . '. ') if scalar(@{$self->{moderators}});
  return $modtext . join(', ', map { "WisCon::Person::$name_func"->($_) } (sort { WisCon::Person::sorting_name($a) cmp WisCon::Person::sorting_name($b) } @{$self->{participants}}));
}

=pod

=item ->is_auto_assigned()

Returns 1 if this item is slated to be auto-assigned.  That is, 
if it fits the criteria in the auto-assignment parameters, and 
either has open assignment slots or has current assignments of 
status "auto".  

=cut

sub is_auto_assigned {
	my $self = shift;
	my $status = $self->param('status');
	my $min_interest = $self->param('min_total_interest');
	if ($self->get_val("ItemStatus") !~ m/$status/) {
		return undef;
	# do not assign anyone to the moderator pool items!!
	} elsif ($self->get_val("idTrackPrimary") == 10) { 
		return undef;
	} elsif ($self->total_interest < $min_interest) { 
		return undef;
	} else {
		return 1 if ($self->open_slots > 0);
		for ($self->assigned_participants()) {
			return 1 if ($self->is_assigned_status($_) =~ m/auto/);
		}
		for ($self->assigned_moderators()) {
			return 1 if ($self->is_assigned_status($_) =~ m/auto/);
		}
		return undef;
	}
}

=pod

=item ->is_assigned($person)

Given a Person object or person ID, returns 1 if that person is assigned 
to this ProgramItem.  

=cut

sub is_assigned {
	my $self = shift;
	return defined($self->is_assigned_type(@_));
}

=pod

=item ->is_assigned_type($person)

Given a Person object or person ID, returns the assignment type 
of that person to this ProgramItem if they are assigned to it, 
or undef otherwise.  

=cut

sub is_assigned_type {
	my $self = shift;
	my $person = shift;
	my $person_id = (ref($person)) ? $person->id : $person;
	die "Invalid person argument ".Dumper($person) unless ($person_id =~ m/^\d+$/);
	return $self->{assigned_type}->{$person_id};
}

=pod

=item ->is_assigned_status($person)

Given a Person object or person ID, returns the assignment type 
of that person to this ProgramItem if they are assigned to it, 
or undef otherwise.  

=cut

sub is_assigned_status {
	my $self = shift;
	my $person = shift;
	my $person_id = (ref($person)) ? $person->id : $person;
	die "Invalid person argument ".Dumper($person) unless ($person_id =~ m/^\d+$/);
	return $self->{assigned_status}->{$person_id};
}

=pod

=item ->open_participant_slots()

Returns an integer for the number of participants that still need 
to be assigned for this item. 

=cut

sub open_participant_slots {
	my $self = shift;
	my $slots = $self->{num_participants} - scalar($self->assigned_participants);
	return ($slots < 0) ? 0 : $slots;
}

=pod

=item ->open_moderator_slots()

Returns an integer for the number of moderators that still need 
to be assigned for this item. 

=cut

sub open_moderator_slots {
	my $self = shift;
	my $slots = $self->{num_moderators} - scalar($self->assigned_moderators);
	return ($slots < 0) ? 0 : $slots;
}

=pod

=item ->open_slots()

Returns an integer for the total number of people (i.e. participants plus 
moderators) that still need to be assigned for this item. 

=cut

sub open_slots {
	my $self = shift;
	return $self->open_participant_slots()+$self->open_moderator_slots();
}

=pod

=item ->interested_participants()

Returns a list of Person objects representing people who expressed 
interest in participating in this item.  

=cut

sub interested_participants {
	my $self = shift;
	my $rank = shift;
	my %assigned;
	for ($self->assigned_participants) { $assigned{$_}=1; }
	if (defined($rank)) {
		$rank =~ m/^\d+$/ or croak "Invalid rank '$rank'";
		my $idlist = $self->{participants_of_ranking}->[$rank];
		my @list = grep { not $assigned{$_} } @$idlist;
		return @list;
	} else {
		my @ids;
		for my $rank (1 .. $self->param('num_interest_ranks')) {
			my $idlist = $self->{participants_of_ranking}->[$rank];
			push(@ids, @{$idlist});
		}
		my @list = grep { not $assigned{$_} } @ids;
		return @list;
	}
}

=pod

=item ->interested_moderators()

Returns a list of idPersons representing people who expressed 
interest in moderating this item.  

=cut

sub interested_moderators {
	my $self = shift;
	my %assigned;
	for ($self->assigned_moderators) { $assigned{$_}=1; }
	my $ref = $self->{interested_moderators};
	my @list = grep { not $assigned{$_} } @$ref;
	return @list;
}

sub equipment_requests {
    my $self = shift;
	my $requests = $sqldb->fetch_select( sql => ["select * from EquipmentRequests, RefEquipmentTypes where EquipmentRequests.idEquipmentTypes=RefEquipmentTypes.idEquipmentTypes and EquipmentRequests.idItems=?", $self->id] );
	return @$requests;
}


#################################################################
### Accessors

sub get_val {
	my $self = shift;
	my $col  = shift;
	return $self->{record}{$col};
}

sub set_val {
	my $self    = shift;
	my $col     = shift;
	my $new_val = shift;
	$self->{record}{$col} = $new_val;
}

sub id {
	my $self = shift;
	return $self->get_val('idItems');
}

sub publicstart {
	my $self = shift;
	my $return = $self->get_val('PublicStartTime');
	$return = $self->get_val('StartDateTime') unless $return;
	return $return;
}

sub publicend {
	my $self = shift;
	my $return = $self->get_val('PublicEndTime');
	$return = $self->get_val('EndDateTime') unless $return;
	return $return;
}

sub name {
	my $self = shift;
	return $self->get_val('ItemName');
}

sub hashtag {
	my $self = shift;
	return '#' . $self->get_val('HashTag') if $self->get_val('HashTag');
	my $hashtag = $self->get_val('GridDesc') || $self->get_val('ItemName');
	$hashtag =~ s/\W//g;
	return '#' . $hashtag;
}

######################################################################

1;
