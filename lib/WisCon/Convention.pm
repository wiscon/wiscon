package WisCon::Convention;

use strict;
use warnings;
use Carp;
use Data::Dumper;
use WisCon::SQLEngine;
use WisCon::DateTime;

my $sqldb = WisCon::SQLEngine::sqldb();

sub fetch {
    my $class = shift;
	my $idConventions = shift;
    my $convention = $sqldb->fetch_one( 
        table => 'Conventions',
        where => { idConventions => $idConventions },
    );
    die("Can't fetch convention '$idConventions'") unless $convention;
    bless $convention, $class;
}

sub current {
    my $class = shift;
    my $convention = $sqldb->fetch_one( 
        table => 'Conventions',
        where => { CurrentFlag => 1 },
    );
    if ( ! $convention ) {
        $sqldb->reconnect();
        $convention = $sqldb->fetch_one( 
            table => 'Conventions',
            where => { CurrentFlag => 1 },
        );
    }
    die("Can't retrieve current convention") unless $convention;
    bless $convention, $class;
}


# expects an input idConventions
# clears the old CurrentFlag & turns it on for the input convention
sub set_current {
	my $idConventions = shift;
    my $oldconvention = $sqldb->fetch_one( 
        table => 'Conventions',
        where => { CurrentFlag => 1 },
    );
	$oldconvention->{ CurrentFlag } = 0;
    my $newconvention = $sqldb->fetch_one( 
        table => 'Conventions',
        where => { idConventions => $idConventions },
    );
	if (ref $newconvention) {
		$newconvention->{ CurrentFlag } = 1;
		WisCon::SQLEngine::log_update( $sqldb, table=>'Conventions', key=>'idConventions', id=>$oldconvention->{idConventions}, values=>$oldconvention );
		WisCon::SQLEngine::log_update( $sqldb, table=>'Conventions', key=>'idConventions', id=>$newconvention->{idConventions}, values=>$newconvention );
	}	
}

#  Expects an integer input parameter. Value == 0 means
#  you want the current convention, Value == 1 means you
#  want the previous convention, etc. Value must be >= 0.
sub previous {
    my $class = shift;
    my $howLongAgo = shift;
    
    # previous(0), previous(), or previous(-2) all get the current convention
    if ((!$howLongAgo) || ($howLongAgo < 0)) {
		$howLongAgo = 0;
    }
    
   	# Get the ConventionOrder of the current convention
   	my $convention = $sqldb->fetch_one( 
       	table => 'Conventions',
       	where => { CurrentFlag => 1 },
   	);
   	
   	# Then adjust it based on the input parameter
   	my $conventionOrder = $convention->{ConventionOrder} - $howLongAgo;
   	# And get the convention implied by that ordering.
   	$convention = $sqldb->fetch_one( 
       	table => 'Conventions',
       	where => { ConventionOrder => $conventionOrder },
   	);
   	
    die unless $convention;
    bless $convention, $class;
}

# prior differs from previous & next in that it takes an input convention id (not necessarily current!) and a howlongago parameter,
# and returns the convention that was that far ago before the input convention
sub prior {
    my $class = shift;
    my $idConvention = shift;
    my $howLongAgo = shift;
        
   	# Get the ConventionOrder of the input convention
   	my $convention = WisCon::Convention->fetch($idConvention);
   	
   	# Then adjust it based on the input parameter
   	my $conventionOrder = $convention->{ConventionOrder} - $howLongAgo;
   	# And get the convention implied by that ordering.
   	$convention = $sqldb->fetch_one( 
       	table => 'Conventions',
       	where => { ConventionOrder => $conventionOrder },
   	);
   	
    die unless $convention;
    bless $convention, $class;
}

sub next {
    my $class = shift;
    my $convention = $sqldb->fetch_one( 
        sql => "select * from Conventions where ConventionOrder in (select min(ConventionOrder) from Conventions where ConventionOrder > (select ConventionOrder from Conventions where CurrentFlag=1))",
    );
    die unless $convention;
    bless $convention, $class;
}

sub last {
    my $class = shift;
    my $convention = $sqldb->fetch_one( 
        sql => "select * from Conventions where ConventionOrder in (select max(ConventionOrder) from Conventions where ConventionOrder < (select ConventionOrder from Conventions where CurrentFlag=1))",
    );
    die unless $convention;
    bless $convention, $class;
}

# returns con furthest in the future

sub latest {
    my $class = shift;
    my $convention = $sqldb->fetch_one( 
        sql => "select * from Conventions where ConventionOrder in (select max(ConventionOrder) from Conventions)",
    );
    die unless $convention;
    bless $convention, $class;
}

# returns con accepting panel ideas, either current or next
# if today's date is after the start of the current convention, return next
# otherwise, return current

sub submittable {
    my $class = shift;
	my $convention = $class->current();
	if (WisCon::DateTime::make_datetime($convention->{StartDate}) <= WisCon::DateTime::make_datetime(WisCon::DateTime::now())) {
		return $class->next();
	} else {
		return $convention;
	}
}

sub id {
    my $convention = shift;
    return $convention->{idConventions};
}

sub name {
    my $convention = shift;
    return $convention->{ConventionName};
}

sub number {
    my $convention = shift;
    my @words = split(' ', $convention->{ConventionName});
    my $number = $words[$#words];
    return $number;
}

sub year {
    my $convention = shift;
    my @date_parts = split('-', $convention->{StartDate});
    return $date_parts[0];
}

sub configflags {
    my $convention = shift;
    my $configs = $sqldb->fetch_select( 
        sql => ["select ConfigCode, 
                CASE 
                    WHEN now() < StartDateTime THEN -1
                    WHEN now() between StartDatetime and EndDatetime THEN 1
                    WHEN NOW() > EndDateTime THEN 0
                    ELSE NULL
                END as openflag
                from Config where idConventions = ?", $convention->{idConventions}]
    );

    my $hash;
      foreach my $config (@$configs) {
        $hash->{$config->{"ConfigCode"}} = $config->{"openflag"};
    }
    return $hash;
}

sub configflag {
    my $convention = shift;
	my $config_code = shift;
    my $config = $sqldb->fetch_one( 
        sql => ["select *
                from Config where idConventions = ? and ConfigCode = ?", $convention->{idConventions}, $config_code ]
    );

    
    return $config;
}


sub config {
    my $convention = shift;
    my $config = $sqldb->fetch_select( 
        sql => ["select *
                from Config where idConventions = ? order by StartDatetime", $convention->{idConventions}]
    );

    
    return $config;
}

# return a list of guests for the convention object

sub guests {
    my $convention = shift;
    my $guests = $sqldb->fetch_select( 
        sql => ["select *
                from Guests where idConventions = ? order by GuestType", $convention->{idConventions}]
    );

    
    return $guests;
}

sub GoHs {
    my $convention = shift;
    my $guests = $sqldb->fetch_select( 
        sql => ["select *
                from Guests where idConventions = ? order by GuestType", $convention->{idConventions}]
    );
    my @GoHs = grep { $_->{GuestType} eq 'Guest of Honor' } @$guests;
    return \@GoHs;
}

1;
