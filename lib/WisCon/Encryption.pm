package WisCon::Encryption;

=pod

Utility methods for encryption.

=cut

use strict;

use Crypt::CBC;
use Crypt::Blowfish;

######################################################################

my $delimiter = "\x028";

sub encrypt_values {
	my ( $password, @values ) = @_;
	my $cipher = Crypt::CBC->new( -cipher=>'Blowfish', -key=>$password );
	push @values, ' ';
	my $value = join( $delimiter, @values );
	$cipher->encrypt( $value )
}

sub decrypt_values {
	my ( $password, $encrypted ) = @_;
	my $cipher = Crypt::CBC->new( -cipher=>'Blowfish', -key=>$password );
	my $value = $cipher->decrypt( $encrypted );
	my @values = split $delimiter, $value;
	return unless ( @values > 0 and pop(@values) eq ' ' );
	@values;
}

######################################################################

1;
