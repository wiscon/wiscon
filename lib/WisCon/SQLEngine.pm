package WisCon::SQLEngine;

use strict;
use warnings;

use DBIx::SQLEngine;
use WisCon::SQLEnginePatches;

###

my ( $dbi_dsn, $dbi_user, $dbi_pass );

sub set_connection_params {
	( $dbi_dsn, $dbi_user, $dbi_pass ) = @_;
}

sub find_connection_params {
	my ( $conf_file ) = @_;
	
	unless ( $conf_file ) {
		my $local_dir = $WisCon::Paths{ webconf };
		# change to allow non-web use
		if (not defined($local_dir)) {
			my $userhome = ($ENV{'HOME'} || $ENV{'LOGDIR'} || (getpwuid($>))[7]);
			$local_dir = "$userhome/wisconlocal";
		}
		if ((not defined($local_dir)) or (not -d $local_dir)) {
			Carp::confess( "Can't find local dir for SQL connection params" );
		}
		$conf_file = "$local_dir/sqlengine.conf";
	}
	
	open( INFH, $conf_file )
		or Carp::confess( "Can't read $conf_file for SQL connection params" );
	
	my @params = map { chomp; $_ } map <INFH>, 1 .. 3;
	set_connection_params( @params );	
	
	1;
}

sub get_new_connection {
	$dbi_dsn
			or Carp::confess( "Did not receive connection params" );
	DBIx::SQLEngine->new( $dbi_dsn, $dbi_user, $dbi_pass );
}

###

my $sqldb;

sub sqldb {
	find_connection_params() unless ( $dbi_dsn );
	$sqldb ||= get_new_connection()
}

###
# automatic Create/Update user/date

use Carp;
use Data::Dumper;

sub do_insert {
  my ($self, %args) = @_;
	warn( "Running do_insert( " . Data::Dumper::Dumper( [ \@_ ] ) . " )\n");
#	Carp::cluck( "... backtrace:");
  if ($args{values}) {
    my @columns = $sqldb->detect_table( $args{table} );
    if (grep { $_->{name} eq 'CreateDate' } @columns) {
      $args{values}{CreateDate} = WisCon::DateTime::now();
    } 
    if (grep { $_->{name} eq 'CreateUser' } @columns) {
      $args{values}{CreateUser} = WisCon::Authentication::current_user_id() || 0;
    }
  }
# looks recursive, but isn't, because connection params are twisty
  $self->do_insert(%args);
}

sub do_update {
  my ($self, %args) = @_;
  if ($args{values}) {
    my @columns = $sqldb->detect_table( $args{table} );
    if (grep { $_->{name} eq 'UpdateDate' } @columns) {
      $args{values}{UpdateDate} = WisCon::DateTime::now();
    }
    if (grep { $_->{name} eq 'UpdateUser' } @columns) {
      $args{values}{UpdateUser} = WisCon::Authentication::current_user_id();
    }
  }
# looks recursive, but isn't, because connection params are twisty
  $self->do_update(%args);
}

# takes a key (fieldname), an id (fieldname of table id), a table
# name, and a values hash for the updated row.
# logs changes in the row in ChangeHistory, updates the row.
sub log_update {
    my $sqldb = shift;
    my %params = @_;
    my $old_row = $sqldb->fetch_one( 
            table => $params{table},
	     where => { $params{key}=>$params{id} },
     );
    foreach my $key (keys %{$params{values}} ) {
		next if ( $key eq 'UpdateUser' );
		next if ( $key eq 'UpdateDate' );
        next if ( ! (defined $old_row->{ $key }) && ! (defined $params{values}->{ $key }) );
        next if ( (defined $old_row->{ $key }) && (defined $params{values}->{ $key }) && ($params{values}->{ $key } eq $old_row->{ $key }) );
        # log changed $key from $old_row->{ $key } to $params{values}->{ $key }
        WisCon::SQLEngine::do_insert( $sqldb, 
			  table=>'ChangeHistory', 
			  values => { 'TableName' => $params{table},
				      'FieldName' => $key,
				      'idRecord' => $params{id},
				      'OldValue' => $old_row->{ $key },
				      'NewValue' => $params{values}->{ $key },
				  });
    }
    WisCon::SQLEngine::do_update( $sqldb,
		      table => $params{table},
		      where => { $params{key}=>$params{id} },
		      values => $params{values},
    )
}

# takes a sequence (fieldname), a table
# name, and a values hash for the new row.
# inserts the row, logs new values in the row in ChangeHistory.
#
# returns the ID of the newly inserted row.
sub log_insert {
    my $sqldb = shift;
    my %params = @_;
    WisCon::SQLEngine::do_insert( $sqldb,
		      table => $params{table},
		      sequence => $params{sequence},
		      values => $params{values},
				  );
    my $query = "select last_insert_id() as new_id;";
    my $result = sqldb()->fetch_one( sql => [$query] );
    my $new_id = $$result{"new_id"};

    foreach my $key (keys %{$params{values}} ) {
	next if ( $key eq 'UpdateUser' );
	next if ( $key eq 'UpdateDate' );
        # log new values for $params{values}->{ $key }
        WisCon::SQLEngine::do_insert( $sqldb, 
			  table=>'ChangeHistory', 
			  values => { 'TableName' => $params{table},
				      'FieldName' => $key,
				      'idRecord' => $params{values}->{ $params{sequence} },
				      'NewValue' => $params{values}->{ $key },
				  });
    }
    
    return $new_id;
}

1;
