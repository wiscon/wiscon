package WisCon::Catalog;

use strict;
use warnings;

use WisCon::Convention;

my $sqldb = WisCon::SQLEngine::sqldb();

sub fetch {
    my $class = shift;
	my $id = shift;
	my $catalogItem = $sqldb->fetch_one(
		table => 'CatalogItems',
		where => { 'idCatalog' => $id }
	);
    return undef unless $catalogItem;
    bless $catalogItem, $class;
}

sub fetch_by_type {
    my $class = shift;
    my $type = shift;
    my $convention = shift || WisCon::Convention->current->id;
    my $catalogItem = $sqldb->fetch_one( 
        sql => ["select * from CatalogItems where SaleItemName=? and idConventions=?", $type, $convention],
    );
	$catalogItem = $sqldb->fetch_one( 
        sql => ["select * from CatalogItems where SaleItemName=? and isnull(idConventions)", $type],
    ) unless $catalogItem;
    return undef unless $catalogItem;
    bless $catalogItem, $class;
}

sub holders {
	my $catalogItem = shift;
	my $persons = $sqldb->fetch_select( 
		table => 'OrderLines',
		where =>  "idCatalog = $catalogItem->{idCatalog} and LineStatus in ('complete', 'new', 'incomplete', 'reserved')"
	);
}

1;
