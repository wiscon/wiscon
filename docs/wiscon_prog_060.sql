SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `WisCon` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `WisCon`;

-- -----------------------------------------------------
-- Table `WisCon`.`RefCapacityUnits`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefCapacityUnits` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefLocationType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefLocationType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Locations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Locations` (
  `idLocations` INT NOT NULL AUTO_INCREMENT ,
  `parentidLocations` INT NULL ,
  `LocationName` VARCHAR(45) NOT NULL ,
  `LocationType` VARCHAR(20) NOT NULL ,
  `PublicFlag` BOOLEAN NOT NULL ,
  `CapacityAmt` INT NULL ,
  `CapacityUnitCode` VARCHAR(20) NULL ,
  `StartDate` DATE NULL ,
  `EndDate` DATE NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idLocations`) ,
  UNIQUE INDEX LocationName (`LocationName` ASC) ,
  INDEX LocationParent (`parentidLocations` ASC) ,
  INDEX fkLocationParent (`parentidLocations` ASC) ,
  INDEX fkCapacityUnits (`CapacityUnitCode` ASC) ,
  INDEX fkLocationType (`LocationType` ASC) ,
  CONSTRAINT `fkLocationParent`
    FOREIGN KEY (`parentidLocations` )
    REFERENCES `WisCon`.`Locations` (`idLocations` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkCapacityUnits`
    FOREIGN KEY (`CapacityUnitCode` )
    REFERENCES `WisCon`.`RefCapacityUnits` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkLocationType`
    FOREIGN KEY (`LocationType` )
    REFERENCES `WisCon`.`RefLocationType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Where will the items of the convention be held?';


-- -----------------------------------------------------
-- Table `WisCon`.`Conventions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Conventions` (
  `idConventions` INT NOT NULL AUTO_INCREMENT ,
  `ConventionName` VARCHAR(45) NOT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NOT NULL ,
  `CurrentFlag` BOOLEAN NOT NULL DEFAULT true COMMENT 'Is this the current convention? Defaults to true because we\'ll generally be creating a new one.' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idConventions`) ,
  UNIQUE INDEX iConventionName (`ConventionName` ASC) )
ENGINE = InnoDB
COMMENT = 'Identifies each convention and provides the start and end dates and times, and the public start and end information.';


-- -----------------------------------------------------
-- Table `WisCon`.`RefConventionLimitType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefConventionLimitType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`TimePeriods`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`TimePeriods` (
  `idTimePeriods` INT NOT NULL AUTO_INCREMENT ,
  `idConvention` INT NOT NULL ,
  `StartDateTime` DATETIME NOT NULL ,
  `EndDateTime` DATETIME NOT NULL ,
  `PeriodComment` VARCHAR(4000) NULL ,
  `StandardTimeFlag` BOOLEAN NOT NULL DEFAULT false COMMENT 'Is this a normal time that goes in the grid?' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  `WarnOnSecondUseFlag` BOOLEAN NOT NULL DEFAULT false COMMENT 'Warn users when trying to use this time period more than once?' ,
  `PublicStartTime` DATETIME NULL ,
  `PublicEndTime` DATETIME NULL ,
  PRIMARY KEY (`idTimePeriods`) ,
  INDEX fkLimitConvention (`idConvention` ASC) ,
  INDEX fkLimitUnits (`StandardTimeFlag` ASC) ,
  INDEX fkLimitType (`StartDateTime` ASC) ,
  CONSTRAINT `fkLimitConvention`
    FOREIGN KEY (`idConvention` )
    REFERENCES `WisCon`.`Conventions` (`idConventions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkLimitUnits`
    FOREIGN KEY (`StandardTimeFlag` )
    REFERENCES `WisCon`.`RefCapacityUnits` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkLimitType`
    FOREIGN KEY (`StartDateTime` )
    REFERENCES `WisCon`.`RefConventionLimitType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'For a particular convention, when do things happen. There are standard periods that can be displayed in the grid, and non-standard periods. ';


-- -----------------------------------------------------
-- Table `WisCon`.`Slots`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Slots` (
  `idSlots` INT NOT NULL AUTO_INCREMENT ,
  `idLocations` INT NOT NULL ,
  `idTimePeriods` INT NOT NULL ,
  `SlotDesc` VARCHAR(4000) NULL ,
  `CapacityAmt` INT NULL ,
  `CapacityUnitCode` VARCHAR(20) NULL ,
  `PublicFlag` BOOLEAN NOT NULL DEFAULT true ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idSlots`) ,
  INDEX fkLocations (`idLocations` ASC) ,
  INDEX fkSlotPeriod (`idTimePeriods` ASC) ,
  INDEX fkCapacityUnits (`CapacityUnitCode` ASC) ,
  CONSTRAINT `fkLocations`
    FOREIGN KEY (`idLocations` )
    REFERENCES `WisCon`.`Locations` (`idLocations` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkSlotPeriod`
    FOREIGN KEY (`idTimePeriods` )
    REFERENCES `WisCon`.`TimePeriods` (`idTimePeriods` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkCapacityUnits`
    FOREIGN KEY (`CapacityUnitCode` )
    REFERENCES `WisCon`.`RefCapacityUnits` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Combination of convention, location, and time period. Did not yet build a master set of time periods because that always gets us into trouble when some items don\'t quite fit. Handle that in reporting.';


-- -----------------------------------------------------
-- Table `WisCon`.`RefSalutation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefSalutation` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Persons`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Persons` (
  `idPersons` INT NOT NULL AUTO_INCREMENT ,
  `PersonEmail` VARCHAR(200) NULL ,
  `LastName` VARCHAR(45) NOT NULL ,
  `FirstName` VARCHAR(45) NULL ,
  `MiddleName` VARCHAR(45) NULL ,
  `Salutation` VARCHAR(20) NULL ,
  `Line1Address` VARCHAR(200) NULL ,
  `Line2Address` VARCHAR(200) NULL ,
  `City` VARCHAR(45) NULL ,
  `StateProvince` VARCHAR(45) NULL ,
  `ZipPostalCode` VARCHAR(45) NULL ,
  `Country` VARCHAR(45) NULL ,
  `Title` VARCHAR(45) NULL ,
  `Company` VARCHAR(45) NULL ,
  `ActiveFlag` BOOLEAN NULL ,
  `LongBio` VARCHAR(20000) NULL ,
  `ShortBio` VARCHAR(200) NULL ,
  `PublicName` VARCHAR(100) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idPersons`) ,
  UNIQUE INDEX iPersonEmail (`PersonEmail` ASC) ,
  INDEX fkSalutation (`Salutation` ASC) ,
  CONSTRAINT `fkSalutation`
    FOREIGN KEY (`Salutation` )
    REFERENCES `WisCon`.`RefSalutation` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'People involved in the convention. May suggest items or be assigned to items. May also have order items (registrations, etc.)';


-- -----------------------------------------------------
-- Table `WisCon`.`RefItemStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefItemStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`ProgramItems`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`ProgramItems` (
  `idItems` INT NOT NULL AUTO_INCREMENT ,
  `ItemName` VARCHAR(45) NOT NULL ,
  `ItemDesc` VARCHAR(4000) NULL ,
  `idConventions` INT NULL ,
  `idPersonsProposer` INT NULL ,
  `RecurringFlag` BOOLEAN NOT NULL DEFAULT false ,
  `idItemsParent` INT NULL ,
  `idSlots` INT NULL ,
  `PublicDesc` VARCHAR(4000) NULL ,
  `CapacityAmt` INT NULL ,
  `CapacityUnitCode` VARCHAR(20) NULL ,
  `ItemStatus` VARCHAR(20) NOT NULL DEFAULT 'proposed' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idItems`) ,
  INDEX fkItemsParent (`idItemsParent` ASC) ,
  INDEX fkItemsSlot (`idSlots` ASC) ,
  INDEX fkItemsConventions (`idConventions` ASC) ,
  INDEX fkItemsProposer (`idPersonsProposer` ASC) ,
  INDEX fkItemsStatus (`ItemStatus` ASC) ,
  INDEX fkCapacityUnits (`CapacityUnitCode` ASC) ,
  CONSTRAINT `fkItemsParent`
    FOREIGN KEY (`idItemsParent` )
    REFERENCES `WisCon`.`ProgramItems` (`idItems` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkItemsSlot`
    FOREIGN KEY (`idSlots` )
    REFERENCES `WisCon`.`Slots` (`idSlots` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkItemsConventions`
    FOREIGN KEY (`idConventions` )
    REFERENCES `WisCon`.`Conventions` (`idConventions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkItemsProposer`
    FOREIGN KEY (`idPersonsProposer` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkItemsStatus`
    FOREIGN KEY (`ItemStatus` )
    REFERENCES `WisCon`.`RefItemStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkCapacityUnits`
    FOREIGN KEY (`CapacityUnitCode` )
    REFERENCES `WisCon`.`RefCapacityUnits` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'A single item held at the convention. Panel, party, etc. Parent-child relationship so we can have items which contain other items, such as an academic item with 3 papers, or a Gathering with 20 individual booths.';


-- -----------------------------------------------------
-- Table `WisCon`.`RefAlternateScope`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefAlternateScope` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefAlternateDataType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefAlternateDataType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`AlternateNames`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`AlternateNames` (
  `idAlternateNames` INT NOT NULL AUTO_INCREMENT ,
  `idPersons` INT NOT NULL ,
  `StartDate` DATE NOT NULL COMMENT 'Start date for using this alternate name' ,
  `EndDate` DATE NULL COMMENT 'End date for using this alternate data' ,
  `AlternateScope` VARCHAR(20) NOT NULL COMMENT 'Public, current convention, all conventions, secondary email, etc.' ,
  `SeasonalRepeatsFlag` BOOLEAN NOT NULL DEFAULT false COMMENT 'If true, dates repeat each year, for example for a seasonal address' ,
  `AlternateDataType` VARCHAR(20) NOT NULL COMMENT 'Name, email, address' ,
  `FullName` VARCHAR(200) NULL ,
  `Line1Address` VARCHAR(200) NULL ,
  `Line2Address` VARCHAR(200) NULL ,
  `City` VARCHAR(45) NULL ,
  `StateProvince` VARCHAR(45) NULL ,
  `ZipPostalCode` VARCHAR(45) NULL ,
  `Country` VARCHAR(45) NULL ,
  `PersonEmail` VARCHAR(200) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idAlternateNames`) ,
  INDEX fkAltNamesPersons (`idPersons` ASC) ,
  INDEX fkAltNamesScope (`AlternateScope` ASC) ,
  INDEX fkAltNamesDataType (`AlternateDataType` ASC) ,
  CONSTRAINT `fkAltNamesPersons`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAltNamesScope`
    FOREIGN KEY (`AlternateScope` )
    REFERENCES `WisCon`.`RefAlternateScope` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAltNamesDataType`
    FOREIGN KEY (`AlternateDataType` )
    REFERENCES `WisCon`.`RefAlternateDataType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'The idea here is that the primary name, email, and address are in the Persons table, but some people will have alternate names or addresses. Those have a scope -- who gets to see them -- and a type, along with start and end dates. ';


-- -----------------------------------------------------
-- Table `WisCon`.`RefAssignmentType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefAssignmentType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Assignments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Assignments` (
  `idAssignments` INT NOT NULL AUTO_INCREMENT ,
  `idItems` INT NOT NULL ,
  `idPersons` INT NOT NULL ,
  `AssignmentComment` VARCHAR(4000) NULL ,
  `AssignmentType` VARCHAR(20) NOT NULL ,
  `PublicFlag` BOOLEAN NOT NULL DEFAULT true ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idAssignments`) ,
  INDEX fkAssignmentItem (`idItems` ASC) ,
  INDEX fkAssignmentPersons (`idPersons` ASC) ,
  INDEX fkAssignmentType (`AssignmentType` ASC) ,
  CONSTRAINT `fkAssignmentItem`
    FOREIGN KEY (`idItems` )
    REFERENCES `WisCon`.`ProgramItems` (`idItems` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAssignmentPersons`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAssignmentType`
    FOREIGN KEY (`AssignmentType` )
    REFERENCES `WisCon`.`RefAssignmentType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Person will be doing something for the item. For example, participant or moderator.';


-- -----------------------------------------------------
-- Table `WisCon`.`Users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Users` (
  `idUser` INT NOT NULL AUTO_INCREMENT ,
  `ProfileEmail` VARCHAR(100) NOT NULL ,
  `ProfilePassword` VARCHAR(45) NOT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idUser`) ,
  UNIQUE INDEX iProfilesEmail (`ProfileEmail` ASC) )
ENGINE = InnoDB
COMMENT = 'Account on the application. ProfileEmail address is the userid, ProfilePassword the password. ';


-- -----------------------------------------------------
-- Table `WisCon`.`RefProfileDetailType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefProfileDetailType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`UserProfiles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`UserProfiles` (
  `idProfile` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NOT NULL COMMENT 'FK column to the profile' ,
  `DetailType` VARCHAR(20) NOT NULL ,
  `DetailValue` VARCHAR(4000) NOT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL COMMENT 'Triggered audit column' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Triggered audit column' ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Triggered audit column' ,
  `CreateUser` VARCHAR(45) NOT NULL COMMENT 'Triggered audit column' ,
  PRIMARY KEY (`idProfile`) ,
  INDEX fkDetailProfile (`idUser` ASC) ,
  INDEX fkDetailType (`DetailType` ASC) ,
  CONSTRAINT `fkDetailProfile`
    FOREIGN KEY (`idUser` )
    REFERENCES `WisCon`.`Users` (`idUser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkDetailType`
    FOREIGN KEY (`DetailType` )
    REFERENCES `WisCon`.`RefProfileDetailType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Details attached to the user. Not sure what these will be yet. ';


-- -----------------------------------------------------
-- Table `WisCon`.`RefConnectionType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefConnectionType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`PeopleConnections`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`PeopleConnections` (
  `idPeopleConnections` INT NOT NULL AUTO_INCREMENT ,
  `idPersonsFrom` INT NOT NULL ,
  `idPersonsTo` INT NOT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NULL ,
  `ConnectionType` VARCHAR(20) NULL ,
  `ConnectionComment` VARCHAR(4000) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idPeopleConnections`) ,
  INDEX fkConnectionFrom (`idPersonsFrom` ASC) ,
  INDEX fkConnectionTo (`idPersonsTo` ASC) ,
  INDEX fkConnectionType (`ConnectionType` ASC) ,
  CONSTRAINT `fkConnectionFrom`
    FOREIGN KEY (`idPersonsFrom` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkConnectionTo`
    FOREIGN KEY (`idPersonsTo` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkConnectionType`
    FOREIGN KEY (`ConnectionType` )
    REFERENCES `WisCon`.`RefConnectionType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'People-people relationship related to the convention. Example: will not be on the same program item as X';


-- -----------------------------------------------------
-- Table `WisCon`.`UserAuthority`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`UserAuthority` (
  `idProfileAuthority` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NOT NULL ,
  `idPersons` INT NOT NULL ,
  `StartTimestamp` DATETIME NOT NULL ,
  `EndTimestamp` DATETIME NULL ,
  `AuthorityCode` VARCHAR(20) NOT NULL DEFAULT 'owner' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idProfileAuthority`) ,
  INDEX fkAuthorityProfile (`idUser` ASC) ,
  INDEX fkAuthorityPersons (`idPersons` ASC) ,
  CONSTRAINT `fkAuthorityProfile`
    FOREIGN KEY (`idUser` )
    REFERENCES `WisCon`.`Users` (`idUser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAuthorityPersons`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'This user has row-level authority for this person record, for example to edit it. ';


-- -----------------------------------------------------
-- Table `WisCon`.`PersonAvailability`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`PersonAvailability` (
  `idPersonAvailability` INT NOT NULL AUTO_INCREMENT ,
  `idPersons` INT NOT NULL COMMENT 'FK to the person' ,
  `idConventions` INT NOT NULL COMMENT 'FK to the convention' ,
  `ArrivalDateTime` DATETIME NULL COMMENT 'When will the person first be available at the convention?' ,
  `LeavingDateTime` DATETIME NULL COMMENT 'When will the person leave the convention?' ,
  `EarliestTime` TIME NULL COMMENT 'Earliest time available each day' ,
  `LatestTime` TIME NULL COMMENT 'Latest time available each day' ,
  `OtherConstraintText` VARCHAR(4000) NULL COMMENT 'Description of other availability constraints, to be checked by hand' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idPersonAvailability`) ,
  INDEX fkAvailabilityPersons (`idPersons` ASC) ,
  INDEX fkAvailiabilityConvention (`idConventions` ASC) ,
  CONSTRAINT `fkAvailabilityPersons`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkAvailiabilityConvention`
    FOREIGN KEY (`idConventions` )
    REFERENCES `WisCon`.`Conventions` (`idConventions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefTransactionStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefTransactionStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefTransactionType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefTransactionType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`FinancialTransactions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`FinancialTransactions` (
  `idFinancialTransactions` INT NOT NULL AUTO_INCREMENT ,
  `idPersons` INT NOT NULL ,
  `TransactionDate` DATE NOT NULL ,
  `TransactionType` VARCHAR(20) NOT NULL COMMENT 'Payment, Refund, Debit Memo, Credit Memo' ,
  `TransactionAmt` DECIMAL(8,2) NOT NULL ,
  `TransactionStatus` VARCHAR(20) NOT NULL DEFAULT 'NEW' COMMENT 'Usually will be COMPLETE but can be VOID, NSF, etc.' ,
  `TransactionComment` VARCHAR(4000) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idFinancialTransactions`) ,
  INDEX fkTransPerson (`idPersons` ASC) ,
  INDEX fkTransStatus (`TransactionStatus` ASC) ,
  INDEX fkTransType (`TransactionType` ASC) ,
  CONSTRAINT `fkTransPerson`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkTransStatus`
    FOREIGN KEY (`TransactionStatus` )
    REFERENCES `WisCon`.`RefTransactionStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkTransType`
    FOREIGN KEY (`TransactionType` )
    REFERENCES `WisCon`.`RefTransactionType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Payments or refunds';


-- -----------------------------------------------------
-- Table `WisCon`.`RefOrderMethod`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefOrderMethod` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefOrderStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefOrderStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Orders`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Orders` (
  `idOrders` INT NOT NULL AUTO_INCREMENT ,
  `idPersons` INT NOT NULL COMMENT 'Person making order' ,
  `OrderDate` DATE NOT NULL ,
  `OrderMethodCode` VARCHAR(20) NULL COMMENT 'How did this order come in? Paypal, paper, etc.' ,
  `SourceTransactionNo` VARCHAR(45) NULL COMMENT 'Transaction identifier in the source system (for example, paypal identifier)' ,
  `OrderStatus` VARCHAR(20) NOT NULL DEFAULT 'new' COMMENT 'Usually will get to COMPLETE quickly' ,
  `OrderComment` VARCHAR(4000) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser`  NOT NULL ,
  PRIMARY KEY (`idOrders`) ,
  INDEX fkOrderPerson (`idPersons` ASC) ,
  INDEX fkOrderMethod (`OrderMethodCode` ASC) ,
  INDEX fkOrderStatus (`OrderStatus` ASC) ,
  CONSTRAINT `fkOrderPerson`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkOrderMethod`
    FOREIGN KEY (`OrderMethodCode` )
    REFERENCES `WisCon`.`RefOrderMethod` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkOrderStatus`
    FOREIGN KEY (`OrderStatus` )
    REFERENCES `WisCon`.`RefOrderStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Somebody buys something or gives us something';


-- -----------------------------------------------------
-- Table `WisCon`.`Catalog`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Catalog` (
  `idCatalog` INT NOT NULL AUTO_INCREMENT ,
  `idConventions` INT NULL COMMENT 'Optional foreign key. Some sales items may not be for a particular convention.' ,
  `SaleItemName` VARCHAR(45) NOT NULL COMMENT 'Name, such as \"Adult Membership\"' ,
  `SaleItemDesc` VARCHAR(4000) NULL COMMENT 'Description of what this sales item is' ,
  `UnitPrice` DECIMAL(6,2) NOT NULL COMMENT 'What it costs' ,
  `StartDate` DATE NOT NULL COMMENT 'First sale date' ,
  `EndDate` DATE NULL COMMENT 'Last sale date' ,
  `CapacityAmt` INT NULL COMMENT 'Used for single capacity limits, like child care memberships or dealers tables' ,
  `CapacityUnitCode` VARCHAR(20) NULL ,
  `ConventionMembershipFlag` BOOLEAN NOT NULL DEFAULT false COMMENT 'Is this a convention membership?' ,
  `ConventionLimitType` VARCHAR(20) NULL COMMENT 'Does this count towards an overall convention limit? If so, which one.' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idCatalog`) ,
  INDEX fkCatalogConvention (`idConventions` ASC) ,
  INDEX fkCapacityUnits (`CapacityUnitCode` ASC) ,
  INDEX fkConventionLimit (`ConventionLimitType` ASC) ,
  CONSTRAINT `fkCatalogConvention`
    FOREIGN KEY (`idConventions` )
    REFERENCES `WisCon`.`Conventions` (`idConventions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkCapacityUnits`
    FOREIGN KEY (`CapacityUnitCode` )
    REFERENCES `WisCon`.`RefCapacityUnits` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkConventionLimit`
    FOREIGN KEY (`ConventionLimitType` )
    REFERENCES `WisCon`.`RefConventionLimitType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'What we\'re selling, most tied to a particular convention';


-- -----------------------------------------------------
-- Table `WisCon`.`RefLineStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefLineStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`OrderLines`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`OrderLines` (
  `idOrderLines` INT NOT NULL AUTO_INCREMENT ,
  `idOrders` INT NOT NULL ,
  `idCatalog` INT NOT NULL ,
  `idPersons` INT NULL COMMENT 'Line applies to person' ,
  `LineStatus` VARCHAR(20) NOT NULL DEFAULT 'new' ,
  `LineComment` VARCHAR(4000) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idOrderLines`) ,
  INDEX idLineOrder (`idOrders` ASC) ,
  INDEX idLineCatalog (`idCatalog` ASC) ,
  INDEX idLinePerson (`idPersons` ASC) ,
  INDEX idLineStatus (`LineStatus` ASC) ,
  CONSTRAINT `idLineOrder`
    FOREIGN KEY (`idOrders` )
    REFERENCES `WisCon`.`Orders` (`idOrders` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idLineCatalog`
    FOREIGN KEY (`idCatalog` )
    REFERENCES `WisCon`.`Catalog` (`idCatalog` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idLinePerson`
    FOREIGN KEY (`idPersons` )
    REFERENCES `WisCon`.`Persons` (`idPersons` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idLineStatus`
    FOREIGN KEY (`LineStatus` )
    REFERENCES `WisCon`.`RefLineStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefDistributionStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefDistributionStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Distributions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Distributions` (
  `idDistributions` INT NOT NULL AUTO_INCREMENT ,
  `idOrderLines` INT NOT NULL ,
  `idFinancialTransactions` INT NOT NULL ,
  `DistributionDate` DATETIME NOT NULL ,
  `DistributionAmt` DECIMAL(8,2) NOT NULL ,
  `DistributionStatus` VARCHAR(20) NOT NULL DEFAULT 'NEW' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idDistributions`) ,
  INDEX idDistributionOrderLine (`idOrderLines` ASC) ,
  INDEX idDistributionFinancial (`idFinancialTransactions` ASC) ,
  INDEX idStatus (`DistributionStatus` ASC) ,
  CONSTRAINT `idDistributionOrderLine`
    FOREIGN KEY (`idOrderLines` )
    REFERENCES `WisCon`.`OrderLines` (`idOrderLines` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idDistributionFinancial`
    FOREIGN KEY (`idFinancialTransactions` )
    REFERENCES `WisCon`.`FinancialTransactions` (`idFinancialTransactions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idStatus`
    FOREIGN KEY (`DistributionStatus` )
    REFERENCES `WisCon`.`RefDistributionStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Application of a financial transaction to an order line. Ideally, all transactions would be for the exact amount of all orders and this would be easy.';


-- -----------------------------------------------------
-- Table `WisCon`.`ConventionLimit`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`ConventionLimit` (
  `idConventionLimit` INT NOT NULL AUTO_INCREMENT ,
  `idConvention` INT NOT NULL ,
  `LimitType` VARCHAR(20) NOT NULL ,
  `LimitAmt` INT NOT NULL ,
  `LimitComment` VARCHAR(4000) NULL ,
  `LimitUnits` VARCHAR(20) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idConventionLimit`) ,
  INDEX fkLimitConvention (`idConvention` ASC) ,
  INDEX fkLimitUnits (`LimitUnits` ASC) ,
  INDEX fkLimitType (`LimitType` ASC) ,
  CONSTRAINT `fkLimitConvention`
    FOREIGN KEY (`idConvention` )
    REFERENCES `WisCon`.`Conventions` (`idConventions` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkLimitUnits`
    FOREIGN KEY (`LimitUnits` )
    REFERENCES `WisCon`.`RefCapacityUnits` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkLimitType`
    FOREIGN KEY (`LimitType` )
    REFERENCES `WisCon`.`RefConventionLimitType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Capacity limits for a convention. WisCon has dealers tables, art show panels and tables, total memberships, child care memberships. Most of these can be handled directly in the Catalog table, but the membership one crosses several types of orders. Could do by having the catalog be a hierarchy, or by allowing other limits here, calculated and enforced by programmatic code. For now, use the programmatic code approach.';


-- -----------------------------------------------------
-- Table `WisCon`.`RefBatchStatus`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefBatchStatus` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`RefBatchType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefBatchType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Batch`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Batch` (
  `idBatch` INT NOT NULL AUTO_INCREMENT ,
  `BatchType` VARCHAR(20) NOT NULL ,
  `BatchStatus` VARCHAR(20) NOT NULL ,
  `BatchComment` VARCHAR(4000) NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idBatch`) ,
  UNIQUE INDEX RefCode (`BatchType` ASC) ,
  INDEX fkBatchStatus (`BatchStatus` ASC) ,
  INDEX fkBatchType (`BatchType` ASC) ,
  CONSTRAINT `fkBatchStatus`
    FOREIGN KEY (`BatchStatus` )
    REFERENCES `WisCon`.`RefBatchStatus` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkBatchType`
    FOREIGN KEY (`BatchType` )
    REFERENCES `WisCon`.`RefBatchType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'General table to identify processing batches. ID, type, status, comment.';


-- -----------------------------------------------------
-- Table `WisCon`.`Log`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Log` (
  `idLog` INT NOT NULL AUTO_INCREMENT ,
  `LogEntry` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idLog`) )
ENGINE = InnoDB
COMMENT = 'General table to log information, nothing fancy yet. Could add status, type (Error, normal processing, etc.)\n';


-- -----------------------------------------------------
-- Table `WisCon`.`EmailLog`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`EmailLog` (
  `idEmailLog` INT NOT NULL AUTO_INCREMENT ,
  `EmailSubject` VARCHAR(100) NOT NULL ,
  `Sender` VARCHAR(100) NOT NULL ,
  `EmailContents` BLOB NOT NULL ,
  `SentDate` DATETIME NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idEmailLog`) ,
  UNIQUE INDEX RefCode (`EmailSubject` ASC) )
ENGINE = InnoDB
COMMENT = 'Log of e-mails sent by the system. Could add types as well. BLOB for the email content. ';


-- -----------------------------------------------------
-- Table `WisCon`.`EmailLogAddressees`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`EmailLogAddressees` (
  `idEmailLogAddressee` INT NOT NULL AUTO_INCREMENT ,
  `idEmailLog` INT NOT NULL ,
  `BounceFlag` BOOLEAN NOT NULL DEFAULT false ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idEmailLogAddressee`) ,
  UNIQUE INDEX RefCode (`idEmailLog` ASC) ,
  INDEX fkAddressEmailLog (`idEmailLog` ASC) ,
  CONSTRAINT `fkAddressEmailLog`
    FOREIGN KEY (`idEmailLog` )
    REFERENCES `WisCon`.`EmailLog` (`idEmailLog` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Who was sent the email listed in the log. Status is to track bounces. ';


-- -----------------------------------------------------
-- Table `WisCon`.`RefRoleType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefRoleType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`Roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`Roles` (
  `idRoles` INT NOT NULL AUTO_INCREMENT ,
  `idProfiles` INT NOT NULL ,
  `RoleType` VARCHAR(20) NOT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRoles`) ,
  INDEX fkRoleProfiles (`idProfiles` ASC) ,
  INDEX fkRoleType (`RoleType` ASC) ,
  CONSTRAINT `fkRoleProfiles`
    FOREIGN KEY (`idProfiles` )
    REFERENCES `WisCon`.`Users` (`idUser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkRoleType`
    FOREIGN KEY (`RoleType` )
    REFERENCES `WisCon`.`RefRoleType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'What is the owner of this profile allowed to do?';


-- -----------------------------------------------------
-- Table `WisCon`.`RefRankingType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`RefRankingType` (
  `idRef` INT NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRef`) ,
  UNIQUE INDEX RefCode (`Code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WisCon`.`ItemRanking`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`ItemRanking` (
  `idRoles` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NOT NULL ,
  `idItems` INT NOT NULL ,
  `RankingType` VARCHAR(20) NOT NULL ,
  `RankingDate` DATE NOT NULL ,
  `Ranking` INT NOT NULL COMMENT 'Will these be numeric?' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idRoles`) ,
  INDEX fkRankingProfiles (`idUser` ASC) ,
  INDEX fkRankingType (`RankingType` ASC) ,
  INDEX fkRankingItems (`idItems` ASC) ,
  CONSTRAINT `fkRankingProfiles`
    FOREIGN KEY (`idUser` )
    REFERENCES `WisCon`.`Users` (`idUser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkRankingType`
    FOREIGN KEY (`RankingType` )
    REFERENCES `WisCon`.`RefRankingType` (`Code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkRankingItems`
    FOREIGN KEY (`idItems` )
    REFERENCES `WisCon`.`ProgramItems` (`idItems` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Ranking done by profiles about items, usually in the program planning period.';


-- -----------------------------------------------------
-- Table `WisCon`.`AppParameter`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`AppParameter` (
  `idParameter` INT NOT NULL AUTO_INCREMENT ,
  `Environment` VARCHAR(20) NOT NULL ,
  `Build` VARCHAR(4000) NOT NULL ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idParameter`) ,
  UNIQUE INDEX RefCode (`Environment` ASC) )
ENGINE = InnoDB
COMMENT = 'Single Row table containing basic useful information, such as the environment (dev, ua, production) and the build number.';


-- -----------------------------------------------------
-- Table `WisCon`.`AppScheduler`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `WisCon`.`AppScheduler` (
  `idScheduler` INT NOT NULL AUTO_INCREMENT ,
  `Job Name` VARCHAR(255) NOT NULL ,
  `NextSQL` VARCHAR(4000) NOT NULL COMMENT 'SQL to run to get the next time to execute the job.' ,
  `CreateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `UpdateDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `CreateUser` VARCHAR(45) NOT NULL ,
  `UpdateUser` VARCHAR(45) NOT NULL ,
  `NextTime` DATETIME NOT NULL COMMENT 'When the job is next scheduled to run.' ,
  PRIMARY KEY (`idScheduler`, `NextSQL`) ,
  UNIQUE INDEX RefCode (`Job Name` ASC) )
ENGINE = InnoDB
COMMENT = 'What automated processes to run and when.';



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
