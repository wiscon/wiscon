# README #



### What is this repository for? ###

* This repository holds code to help run a convention, specifically WisCon. Ideally we would like it to be customizable for any con


### How do I get set up? ###

* The current code uses Mason-perl, details for setting up are coming. For more information on using Mason check out [Mason Handbook](http://www.masonbook.com/book/)

### Contribution guidelines ###

* Contact us for information on how to contribute.

### Who do I talk to? ###

* appdev@wiscon.net