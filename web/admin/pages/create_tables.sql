create table WebPage (
	idWebPage	    int unsigned not null auto_increment primary key,
	PageName        varchar(64) not null,
	PageTitle       varchar(255) not null,
	PageContent     text,
	unique index ( PageName )
);
