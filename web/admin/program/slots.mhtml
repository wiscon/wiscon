<%attr>
	title => 'Assign Slots'
</%attr>
<%args>
	$idItems
	$idSlots => undef
	$submit_form => undef
	$clear_form => undef
	@assign => ()
	$group_by => 'day-room'
</%args>
<%init>
	my $sqldb = WisCon::SQLEngine::sqldb();
	my $error;
	my $primarytrack;
	my $secondarytrack;
	my $type;

	my $item = $sqldb->fetch_one(
		table => "ProgramItems",
		where => { idItems=>$idItems },
	);

	my $slot = ( ! $item->{idSlots} ) ? {} : $sqldb->fetch_one( sql => "select Slots.*,Locations.*,TimePeriods.* from Slots join Locations on Slots.idLocations=Locations.idLocations join TimePeriods on Slots.idTimePeriods=TimePeriods.idTimePeriods where idSlots = $item->{idSlots}" );
	
	my $possible_slots = $sqldb->fetch_select ( sql => ["select s.idSlots 
		from ProgramItems i, TimePeriods t, Slots s
		where i.idItems = ?
		and t.idTimePeriods in (select idTimePeriods from TimePeriods tp where tp.idConventions=?)
		and t.idTimePeriods = s.idTimePeriods
		and not exists (select 1 from Assignments a2, Assignments a3, ProgramItems i2, Slots s2, TimePeriods t2
		                 where i.idItems = a2.idItems
										   and a3.idPersons = a2.idPersons
											and i2.idItems = a3.idItems
											and i2.idItems != i.idItems
											and s2.idSlots = i2.idSlots
											and s2.idTimePeriods = t2.idTimePeriods
											and (t2.idTimePeriods = t.idTimePeriods or not (t2.StartDateTime > t.EndDateTime or t2.EndDateTime < t.StartDateTime)))
		and not exists (select 1 from Persons p, Assignments a, PersonAvailability pa 
		                 where a.idPersons = p.idPersons
		                                   and pa.idConventions = ?
										   and a.idItems = i.idItems
											 and pa.idPersons = p.idPersons
											 and (pa.ArrivalDate > t.StartDateTime or
											      (pa.DepartureDate > pa.ArrivalDate && pa.DepartureDate < t.EndDateTime) or
														(pa.Exclude830Flag = 1 and hour(t.StartDateTime) = 8) or
		                        (pa.Exclude1pmFlag = 1 and hour(t.StartDateTime) = 13) or 
														(pa.Exclude4pmFlag = 1 and hour(t.StartDateTime) = 16) or 
														(pa.Exclude7pmFlag = 1 and hour(t.StartDateTime) = 19) or 
														(pa.Exclude9pmFlag = 1 and hour(t.StartDateTime) = 21) or 
														(pa.Exclude1030Flag = 1 and hour(t.StartDateTime) = 22) or 
														(pa.ExcludeMidnightFlag = 1 and hour(t.StartDateTime) in (23,0)) or 
														(pa.Exclude10amFlag = 1 and hour(t.StartDateTime) = 10) or 
														(pa.Exclude230Flag = 1 and hour(t.StartDateTime) = 14)))
		order by s.idSlots;", $idItems, WisCon::Convention->current->id, WisCon::Convention->current->id] );
	$possible_slots = [map {$_->{idSlots}} @$possible_slots] if ref($possible_slots);	
		
	my $current_slot_location;
	my $current_slot_time;
	
	if ( ! $slot or ! $slot->{idSlots} ) { 
		$current_slot_location = "No location selected";
		$current_slot_time = "No time selected";
	} else {
		$current_slot_location = "$slot->{LocationName}";
		$current_slot_time = WisCon::DateTime::descriptor($slot->{StartDateTime}, $slot->{EndDateTime}, 'abbr');
	}
	

	if (not $item) {
	  redirect('/msg', msg=> "The program item requested cannot be found.");
	}
	$primarytrack = WisCon::Ref->fetch('ProgramTracks',id=>$item->{idTrackPrimary}) if $item->{idTrackPrimary};
	$secondarytrack = WisCon::Ref->fetch('ProgramTracks',id=>$item->{idTrackSecondary}) if $item->{idTrackSecondary};
	$type = $sqldb->fetch_one(table => "RefProgramItemTypes", where => {idProgramItemTypes=> $item->{ProgramItemTypes}}) if $item->{ProgramItemTypes};

	# are we submitting new data?
	if ( $submit_form ) {
	  $item->{idSlots} = $idSlots;
	  WisCon::SQLEngine::log_update( $sqldb, table=>'ProgramItems', key=>'idItems', id=>$idItems, values=>$item );
	  redirect("detail?idItems=$item->{idItems}");
	} elsif ( $clear_form ) {
	  $item->{idSlots} = undef;
	  WisCon::SQLEngine::log_update( $sqldb, table=>'ProgramItems', key=>'idItems', id=>$idItems, values=>$item );
	  redirect("detail?idItems=$item->{idItems}");
	}

	my $current_slot_id = $item->{idSlots};

my $people = $sqldb->fetch_select ( sql => ["select PersonAvailability.*,Persons.*,UserInterestRanking,ModeratorFlag,Assignments.* from Persons join UserAuthority on Persons.idPersons=UserAuthority.idPersons and AuthorityCode='identity' left join PersonAvailability on UserAuthority.idPersons=PersonAvailability.idPersons and PersonAvailability.idConventions=? left join ItemRankings on UserAuthority.idUsers=ItemRankings.idUsers and ItemRankings.idItems=$item->{idItems} join Assignments on Persons.idPersons=Assignments.idPersons and Assignments.idItems=$item->{idItems} order by LastName, FirstName", WisCon::Convention->current->id] );
my $counts = $sqldb->fetch_select( sql => "select count(*) as count,idPersons from Assignments join ProgramItems on ProgramItems.idItems=Assignments.idItems and idConventions=" . WisCon::Convention->current->id . " group by idPersons" );
my %counts;
foreach my $count (@$counts) {
  $counts{$count->{idPersons}}=$count->{count};
}
my $attending = $sqldb->fetch_one( sql => "select count(*) as count from ItemRankings where idItems=$item->{idItems} and AttendFlag=1" );
my $ranking = $sqldb->fetch_one( sql => [ "select count(distinct(idPersons)) as count from ItemRankings join ProgramItems on ItemRankings.idItems=ProgramItems.idItems where idConventions=?", WisCon::Convention->current->id ] );
</%init>

<form id="slot_picker_form" method="get" action="slots">
	<input type="hidden" name="idItems" value="<% $idItems %>">
	<input type="hidden" id="fld_idSlots" name="idSlots" value="<% $current_slot_id %>" />

	<table class="appFormRegion" cellpadding="0" cellspacing="0" border="0">
		<thead class="appRegionHeader">
			<tr>
				<th class="appRegionTitle">Program Item Schedule</th>
				<th class="appRegionButtons" style="margin: -2px 0px">
					<input id="submit_fld" type="submit" name="submit_form" value="Save Changes" class="appButton" />
					<input type="submit" name="clear_form" value="Unschedule Item" class="appButton" />
					<input type="button" onclick="javascript:history.back();" value="Cancel" class="appButton" />
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2" class="appRegionBody">

					<table class="appStandard" border="1" cellpadding="6">

						<tr valign="top">
							<th align="right">
								Item:
							</th>
							<td align="left" style="background: white" style="background: white">
								<strong><a href="detail?idItems=<% $idItems %>"><% $item->{ItemName} %></a></strong>
								<br>
								<% $type->{Code} %> |
								
% if ($item->{idFormat} =~ m/\S/) {
%  my $format = $sqldb->fetch_one(table => "RefProgramFormats", where => { idProgramFormats => $item->{idFormat} });
<% $format->{Title} %> |
% }
<% $primarytrack ? $primarytrack->name . ' | ' : "" %>
<% $secondarytrack ? '<i>' . $secondarytrack->name . '</i> | ' : "" %>

<% ucfirst( $item->{ItemStatus} ) %>
<% $item->{ModeratorSignupFlag} ? '| Moderated' : ''  %>
<% $item->{GeneralSignupFlag} ? '| Open for signup' : '' %>
<br>
% if ($item->{SuggestedModerators} =~ m/\S/) {
<em>Suggested Moderators: <% $item->{SuggestedModerators} %></em>
% }
% if ($item->{SuggestedPanelists} =~ m/\S/) {
<em>Suggested Panelists: <% $item->{SuggestedPanelists} %></em>
% }
% if ($item->{AdminNotes} =~ m/\S/) {
Admin Notes:  <% $item->{AdminNotes} %>
% }
							</td>
						</tr>

						<tr valign="top">
							<th align="right" width="200" style="padding-top: 9px;">
								Location:
							</th>
							<td align="left" style="background: white">
								<span id="current_loc_text">
									<% $current_slot_location %>
								</span>
							</td>
						</tr>
						<tr valign="top">
							<th align="right" width="200" style="padding-top: 9px;">
								Interest:
							</th>
							<td align="left" style="background: white">
								<span>
									<% $attending->{count} %> attending <small>(out of <% $ranking->{count} %> people who ranked any panel</small>)
								</span>
							</td>
						</tr>
						<tr valign="top">
							<th align="right" width="200" style="padding-top: 9px;">
								Schedule:
							</th>
							<td align="left" style="background: white">
								<span id="current_time_text">
									<% $current_slot_time |n %>
								</span>
							</td>
						</tr>
						<tr valign="top">
							<th align="right" width="200" style="padding-top: 9px;">
								Panelists:
							</th>
  <td align="left" style="background: white">
	
	<style type="text/css" media="screen">
		.expanded-mode { 
			background: white; 
			width: 100%;
		}
	</style>

	<div style="margin: -7px;">
<& /template/table.minc, 
 	rows => $people, 
	columns => [ 
		{
			name =>'Person', 
			value=> sub { "$_->{FirstName} $_->{MiddleName} $_->{LastName}" },
			href=> sub { "/admin/people/detail?idPerson=$_->{idPersons}" } 
		}, 
		{
			name =>'Type', 
			html=>sub { ( $_->{AssignmentType} eq 'moderator' ? '<strong>' : '' ) . $_->{AssignmentType} . ( $_->{AssignmentType} eq 'moderator' ? '</strong>' : '' ) . ($_->{AssignmentStatus} eq 'locked' ? '' : " <small><a href=\"modtoggle.mhtml?idItems=$idItems&idPersons=$_->{idPersons}\">" . ( $_->{AssignmentType} eq 'participant' ? 'make mod' : 'not mod' ) . "</small>") }
		}, 
		{
			name =>'Status', 
			value=> 'AssignmentStatus' 
		}, 
		{
			name => 'OK',
			html => sub { ( ( not defined $_->{AssigneeAcceptanceFlag} ) ? 'Unknown' : ( $_->{AssigneeAcceptanceFlag} ? '<font color=green>Yes</font>' : '<font color=red>No</font>') ) . ' ' . $_->{AssigneeComment} },
		},
		{
			name =>'Assign- ments', 
			value=> sub { $counts{$_->{idPersons}} },
			th_style => 'width: 50px',
			td_style => 'text-align: center',
		}, 
		{
			name =>'Comment', 
			value=>'AssignmentComment',
		},
		{
			name =>'Availability', 
			spanner => 'yes',
			html => sub { 
		my $data = $_;
		my @times = WisCon::DateTime::times();
		my @events = WisCon::DateTime::special_events();
		my $exclusions = join(', ', map { $_->{hours} || $_->{name} } (grep { $data->{$_->{flag}} } @times, @events));
		$exclusions = ".  Exclude $exclusions" if $exclusions;

		($data->{ArrivalDate} ? WisCon::DateTime::descriptor( $data->{ArrivalDate}, $data->{DepartureDate}) : "Not provided") . ( $data->{OtherConstraintText} ? ' : ' . $data->{OtherConstraintText} : '' ) . $exclusions },
		} 
	], 
	style=>{ 
		table_class=>"appStandard appWithBorder expanded-mode",
	} ,
&>
	</div>
	<div style="padding-top: <% ( scalar @$people ) ? 10 : 0 %>px">
	<a href="assign?idItems=<% $item->{idItems} %>"><small>Edit Assignments</small></a>
	</div>
							</td>
						</tr>

					</table>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<table style="width: 100%" class="appFormRegion" cellpadding="0" cellspacing="0" border="0">
<thead class="appRegionHeader">
<tr>
  <th class="appRegionTitle">Schedule Options</th>
  <th class="appRegionButtons"></th>
</tr>
</thead>
<tbody>
<tr>
  <td colspan="2" class="appRegionBody">
	
	<div style="background: #eee; padding: 4px 8px; border: 1px solid #ccc; border-bottom: none">
		<% ( $group_by eq 'day-room' ) ? "<b>Group By Day</b>" : qq{<a href="slots?idItems=$idItems&amp;group_by=day-room">Group By Day</a>} |n %>
		|
		<% ( $group_by eq 'room-day' ) ? "<b>Group By Location</b>" : qq{<a href="slots?idItems=$idItems&amp;group_by=room-day">Group By Location</a>} |n %>
	</div>
	<div style="background: white; padding: 0px 8px; border: 1px solid #ccc">
		<& /template/timeline.minc, group_by => $group_by, possible_slots => $possible_slots, item_types => [ map { $_->id }  WisCon::Ref->all('ProgramItemTypes') ], current_slot_id => $item->{idSlots}, click_function => 1, click_empty => 1 &>
	</div>

  </td>
</tr>
</tbody>
</table>
