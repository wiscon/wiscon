<%once> 

	sub show_column {
		my ( $subject, $column ) = @_;
		my $output;
		if  ( $column->{html} ) {
			$output = extractor( $subject, $column->{html} );
		} elsif ( $column->{textile} ) {
			$output = textile( extractor( $subject, $column->{value} ) );
		} else {
			$output = html_escape( extractor( $subject, $column->{value} ) );
		}
		if ( ! length $output and $column->{default} ) {
			$output = html_escape( extractor( $subject, $column->{default} ) );
		}
		if  ( $column->{href} ) {
			my $href = html_escape( extractor( $subject, $column->{href} ) );
			$output = qq{<a href="$href">$output</a>} if ( length $href );
		}
		return $output;
	}

	sub extractor {
		my ( $subject, $key ) = @_;
		if ( ! length $key ) {
			Carp::confess( "Missing extractor key for '$subject'" );
		} elsif ( ref($key) eq 'CODE' ) {
			local $_ = $subject; 
			$key->( $subject );
		} elsif ( ref($key) eq 'SCALAR' ) {
			$key;
		} elsif ( ref($key) ) {
			Carp::confess( "Unsupported extractor key ref '$key' for '$subject'" );
		} elsif ( $key =~ /\$\_/ ) {
			$_[1] = $key = eval "sub { qq{$key} }";
			local $_ = $subject; 
			$key->( $subject );
		} elsif ( ref($subject) eq 'HASH' ) {
			$subject->{ $key };				
		} else {
			Carp::confess( "Unsupported extractor key '$key' for '$subject'" );
		}
	}

	sub attr_unless_empty {
		my ( $attr, $value ) = @_;
		$value ? qq{ $attr="$value"} : ''
	}

</%once>
<%args>
	$columns => []
	$rows => []
	$header => ''
	$footer => ''
	$style => {}
</%args>
<%perl>
  my @columns = @$columns;

  my @groups = grep { $_->{group_by} } @columns;
  @columns = grep { ! $_->{group_by} } @columns;

  my @spanners = grep { $_->{spanner} } @columns;
  @columns = grep { ! $_->{spanner} } @columns;

  my %style = (
		table_class => 'lined',
		table_style => '',
		tr_head_class => 'head',
		tr_foot_class => 'foot',
		tr_classes => [ qw/ odd even / ],
		%$style,
	);

</%perl>
<table class="<% $style{table_class} %>" <% attr_unless_empty( style => $style{table_style} ) |n %>>
% if ( $style{ transpose } ) {
% my @row_classes = @{ $style{tr_classes} };
% my $counter = 0;
%
% my $skip_col_headers;
% foreach my $column ( @columns ) {
%   my $row_class = $row_classes[ ++ $counter % @row_classes ];
%   if ( $style{tr_class} ) { 
%     $row_class .= ' ' . extractor( $column, $style{tr_class} );
%   }
	<tr class="<% $row_class %>">
%   if ( $skip_col_headers ) {
%     $skip_col_headers --;
%   } else {
%   $skip_col_headers = $column->{th_colspan} ? $column->{th_colspan} - 1 : 0;
		<th<% $column->{th_style} ? qq{ style="$column->{th_style}"} : '' |n %><% $column->{th_colspan} ? qq{ rowspan="$column->{th_colspan}"} : '' %>> <% $column->{th_href} ? $column->{th_href} : $column->{ 'name' } %> </th>
%   }
% if ( ! scalar @$rows and $column eq $columns[0] ) {
		<td class="null" rowspan="<% scalar @columns %>"><i>Empty Set</i></td>
% }
% my $last_group;
% foreach my $record ( @$rows ) {
%   if ( scalar @groups ) {
%     my $group_value = join ' &rdquo; ', map { show_column( $record, $_ ) } @groups;
%     if ( $last_group ne $group_value ) {
%     	$last_group = $group_value;
				<tr class="group">
					<td colspan="<% scalar @columns %>">
						<% $group_value|n %>
					</td>
				</tr>
%     }
%   }
		<td<% $column->{td_style} ? qq{ style="} . ( ref($column->{td_style}) ? extractor( $record, $column->{td_style} ) : $column->{td_style} ) . qq{"} : '' |n %><% $column->{td_class} ? qq{ class="} . ( ref($column->{td_class}) ? extractor( $record, $column->{td_class} ) : $column->{td_class} ) . qq{"} : '' |n %>> <% show_column( $record, $column ) |n %> </td>
% }

	</tr>
% }
% if ($footer) {
	<tr class="<% $style{tr_foot_class} %>">
			<th colspan="<% scalar @$rows + 1 %>"><% ref( $footer ) ? $footer->() : $footer |n %></th>
	</tr>
% }
% } else {
  <tr class="<% $style{tr_head_class} %>">
% my $skip_col_headers;
% foreach my $column ( @columns ) {
%   if ( $skip_col_headers ) {
%     $skip_col_headers --;
%     next;  
%   }
%   $skip_col_headers = $column->{th_colspan} ? $column->{th_colspan} - 1 : 0;
		<th<% $column->{th_style} ? qq{ style="$column->{th_style}"} : '' |n %><% $column->{th_colspan} ? qq{ colspan="$column->{th_colspan}"} : '' %>> <% $column->{th_href} ? $column->{th_href} : $column->{ 'name' } %> </th>
% }
	</tr>
% if ( ! scalar @$rows ) {
	<tr class="null">
		<td colspan="<% scalar @columns %>"><i>Empty Set</i></td>
	</tr>
% }
% my @row_classes = @{ $style{tr_classes} };
% my $counter = 0;
% my $last_group;
% foreach my $record ( @$rows ) {
%   if ( scalar @groups ) {
%     my $group_value = join ' &rdquo; ', map { show_column( $record, $_ ) } @groups;
%     if ( $last_group ne $group_value ) {
%     	$last_group = $group_value;
				<tr class="group">
					<td colspan="<% scalar @columns %>">
						<% $group_value|n %>
					</td>
				</tr>
%     }
%   }
%   my $row_class = $row_classes[ ++ $counter % @row_classes ];
%   if ( $style{tr_class} ) { 
%     $row_class .= ' ' . extractor( $record, $style{tr_class} );
%   }
	<tr class="<% $row_class %>">
% foreach my $column ( @columns ) {
		<td<% $column->{td_style} ? qq{ style="} . ( ref($column->{td_style}) ? extractor( $record, $column->{td_style} ) : $column->{td_style} ) . qq{"} : '' |n %><% $column->{td_class} ? qq{ class="} . ( ref($column->{td_class}) ? extractor( $record, $column->{td_class} ) : $column->{td_class} ) . qq{"} : '' |n %>> <% show_column( $record, $column ) |n %> </td>
% }
	</tr>
%   if ( @spanners ) {
%     foreach my $column ( @spanners ) {
		<tr><td colspan="<% scalar @columns %>" <% $column->{td_style} ? qq{ style="} . ( ref($column->{td_style}) ? extractor( $record, $column->{td_style} ) : $column->{td_style} ) . qq{"} : ''|n %><% $column->{td_class} ? qq{ class="} . ( ref($column->{td_class}) ? extractor( $record, $column->{td_class} ) : $column->{td_class} ) . qq{"} : '' |n %>> <% show_column( $record, $column ) |n %> </td></tr>
%     }
%   }
% }
% if ($footer) {
	<tr class="<% $style{tr_foot_class} %>">
			<th colspan="<% scalar @columns %>"><% ref( $footer ) ? $footer->() : $footer |n %></th>
	</tr>
% }
% }
</table>
