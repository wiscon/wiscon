// FunkyStuff gets executed when page is ready
function init() {
	// TEMP
//	$(".timebarItemRevealer").removeClass("timebarSelected");
	
	// Handling click on print button
	$("#printButton").click(function(event) {
		var content = $("#ScheduleInfo").clone().html();
		$("#wiscon-header, #topnav, #wiscon-content").hide();
		$("body").prepend("<div id='SchedulePrintView'></div>");
		$("#SchedulePrintView").prepend(content);
		$(".info").show();
		$("img").hide();
		$("#SchedulePrintView").css("font-size", "14px");
		$("#SchedulePrintView").prepend("<p style='font-weight:bold;font-size:28px;'>"+$("#WisconScheduleTitle").text()+"</p>");	
		$("#SchedulePrintView").prepend("<button id='backButton'>Back</button>");
	});
	
	// Handle "Back" from print view
	$("#backButton").live("click", function(event) {
		$("#SchedulePrintView").remove();
		$("#wiscon-header, #topnav, #wiscon-content").show();
		$("img").show();
		// BUG: Need to hide arrow images from previous popups. Verify that popup is hidded.
		$(".info").hide();
	});
	
	/*
	 * live() also handles newly added elements.
	 * Handles clicks on item Titles and Disclosure triangle
	 */
	$(".revealer").live("click", function(event) {
		var infoBox = $(this).nextAll(".info");
		var navImg = $(this).parent().children("img.revealer");
		// if the box is not showing, show it and change pic to 'expanded'
		if(infoBox.css("display") == "none") {
			infoBox.show();
			navImg.attr("src", "/images/icons/expanded.png");
		} else {
			infoBox.hide();
			navImg.attr("src", "/images/icons/collapsed.png");
		}
		event.preventDefault();
	});
	
	// show or hide the description box in timeline view
	$(".descRevealer").live("click", function(event) {
		$("#masterBox").hide();
		$("#rightArrow").hide();
		$("#leftArrow").hide();
		var words = $(this).attr("id").split("_");
		var scheduleID = words[1];
		var refFrame = $("#wiscon-body").offset();
		var offset = $(this).parent().offset();
		var outline = 16;	// border and padding
		var picSize = 18;	// arrow pic size
		var arrow;
		var y = offset.top - refFrame.top;
		var x = offset.left - refFrame.left;
		

		if ((x + picSize + $(this).width() + $("#masterBox").width()) > ($(document).width()-refFrame.left)) {
			// show popup on left
			x -= picSize;
			$("#leftArrow").css("left", x + "px");
			$("#leftArrow").css("top", y + "px");
			arrow = $("#leftArrow");
			x -= $("#masterBox").width() + outline - 3;
		} else {
			//show popup on right
			x += $(this).width();
			$("#rightArrow").css("left", x + "px");
			$("#rightArrow").css("top", y + "px");
			arrow = $("#rightArrow");
			// for arrow pic
			x += picSize - 3;
		}
		
		$("#masterBox").css("position", "absolute");
		$("#masterBox").css("left", x + "px");
		$("#masterBox").css("top", y + "px");
		$.post("/ajax/returnDesc.mhtml", {itemID: words[0], scheduleID: scheduleID}, function(data){
				$("#masterBox").html(data);
				$("#masterBox").show();
				arrow.show();
		 });
 		event.preventDefault();			
	});
	
	// Click on timeline popup X box.
	$("#closeMasterBox").live("click", function(e) {
		$("#masterBox").hide();
		$("#rightArrow").hide();
		$("#leftArrow").hide();
	});

	
	var eventTypes = ["10", "9", "7", "3", "2", "6", "11", "1", "5"];
	var active_schedule = "schedule_main";
	var numberTypesSelected = 9; // 9 possibilities are hardcoded.
	
	/*
	 * handles program type buttons; show/hide items of the various defined types
	 */
	$(".timebarItemRevealer").click(function(event) {
		var itemsToShow = $(".program_" + $(this).attr("class").split(" ")[0] + "." + active_schedule);		

		$(this).toggleClass("timebarSelected");
		// if all event type button is selected
		if ($("#timeBarItemAll").is(".timebarSelected")) {
			$("." + active_schedule).hide();
			$("#timeBarItemAll").removeClass("timebarSelected");
			itemsToShow.show();
			hideEmptyLines();
			numberTypesSelected = 1;
		} else {
			// if button is selected, show items
			if ($(this).is(".timebarSelected")) {
				showNeededLines(itemsToShow);
				itemsToShow.show();
				numberTypesSelected++;
			} else { // else hide items
				itemsToShow.hide();
				hideEmptyLines();
				numberTypesSelected--;
				if(numberTypesSelected < 1) {
					timeBarItemAllClicked(active_schedule);
				}
			}
		}
		event.preventDefault();
	});
	
	// Click the "All Event Types" button.
	$("#timeBarItemAll").click(function(event) {
		timeBarItemAllClicked(active_schedule);
		event.preventDefault();
	});
	
	// Changing from "Main Schedule" to "My Schedule" and back.
	$(".schedule_selector").change(function(event) {
		// need to hide pop up box
		$("#masterBox").hide();
		$("#rightArrow").hide();
		$("#leftArrow").hide();

		$("#timeBarItemAll").addClass("timebarSelected");
		numberTypesSelected = 9;
		var selectedSchedule = $(".schedule_selector").val();
		active_schedule = selectedSchedule;
		$(".schedule_main").hide();
		hideEmptyLines();
		showNeededLines($("." + selectedSchedule));		
		
		$("."+selectedSchedule).show();
	});
	
	// Add and remove items from the personal schedule.
	$(".modifySchedule").live("click", 
		function(event) {
		// Add Item
		var info = getEventInfo(this.id);
		var thisObj = $(this);
		if ( info.sched != 0) {
			// temporarily disable the button
			this.disabled = true;

			// change the temporary button text accordingly
			if (thisObj.text().search(/add/i) >= 0) {
				thisObj.text("Adding...");
			} else {
				thisObj.text("Removing...");
			}

			$.post("/ajax/addRemoveSchedItem.mhtml", { itemID: info.id, schedID: info.sched }, function(data) {
				updateItemStatus(info, data.result);
			}, "json");
			// reenable the botton
			this.disabled = false;
		} else {
			alert("Please login to add items to your personal schedule.");
		}
	});
	
	// Clicked the "Create Personal Event" button
	$("#createCustomEvent").click(function(event) {
		$("#createEventForm").toggle();
		$(this).toggleClass("timebarSelected");
	});
	
	// BUG: Clicked the "Create Event" button
//	$("#createEvent").click(function(event) {
//
//	});
	
	// Expand and close all -- Block disclosure triangle.
	$("#expandAll").click(function(event){
		var visibleinfos = $(".listItem:visible").find(".info");
		var collapseImages = $("img.revealer:visible");
		if ($("#expandAllText").text() == "Expand All") {
			visibleinfos.show();
			$("#expandAllImage").attr("src", "/images/icons/expanded.png");
			collapseImages.attr("src", "/images/icons/expanded.png");
			$("#expandAllText").text("Collapse All");
		} else if ($("#expandAllText").text() == "Collapse All") {
			visibleinfos.hide();
			$("#expandAllImage").attr("src", "/images/icons/collapsed.png");
			collapseImages.attr("src", "/images/icons/collapsed.png");
			$("#expandAllText").text("Expand All");			
		}
	});
	
	// Help button clicked
	$("#help").click(function(event){
		$("#helpText").toggle();
		$(this).toggleClass("timebarSelected");
	});
	
	
	
	// Buttons to add/remove and added to schedule
	// BUG: Can be simplified because of the simpler UI to add/remove
	$(".addScheduleButton, .removeScheduleButton").live("click", 
		function(event) {
			var info = getEventInfo(this.id);
			var imgid = this.id;
			if ( info.sched != 0) {
			// temporarily disable the button
			this.disabled = true;

			$.post( '/ajax/addRemoveSchedItem.mhtml', { itemID: info.id, schedID: info.sched },
				function(data) {
					// if return string said "added" change button text to Remove, otherwise to Add.
					updateItemStatus(info, data.result);
				}, "json");
			}
	});


}	// Closing the initialization of the click event handlers

function setScheduleId(id) {
	scheduleId = id;
}

// Handling adds/removes from personal schedule. Can be simplified due to the simpler UI.
function updateItemStatus(info, result) {
	if(result == "added") {
		$(info.imageId).attr('src', "/images/icons/delete.png").attr('alt', "Remove").attr('title', "Remove");
		$(info.title).css("font-weight", "bold");
		$(info.buttonId).attr('textContent', "Remove from Schedule").attr('title', "Remove");
	} else if (result == "removed") {
		$(info.imageId).attr('src',"/images/icons/add.png").attr('alt', "Add").attr('title', "Add");
		$(info.title).css("font-weight", "normal");
		$(info.buttonId).attr('textContent', "Add to Schedule").attr('title', "Add");
	}
}

// When the page is ready - call "FunkyStuff" -->
$(document).ready(init);

// Clicked "All Event Types"
function timeBarItemAllClicked(active_schedule) {
	var selectedSchedule = $("." + active_schedule);
	var allSelected = $("#timeBarItemAll").is(".timebarSelected");
	if (!allSelected) {
		showNeededLines($(selectedSchedule));
		$(selectedSchedule).show();
		$(".timebarItemRevealer").removeClass("timebarSelected");
		$("#timeBarItemAll").toggleClass("timebarSelected");
	}
}

// BUG: Once other stuff is updated, this should be deleted.
function getEventInfo(buttonID) {
	var info = buttonID.split("_");
	var id = info[1];
	var schedule = scheduleId;
	var title = "#title_item_" + id;
	var buttonId = "#modItem_" + id + "_" + schedule;
	var imageId = "#addRemoveButton_" + id + "_" + schedule;
	return {id: id, sched: schedule, title: title, buttonId: buttonId, imageId: imageId};
}

/**
 * Call this method to hide empty lines, e.g. nothing at that time; nothing on that day; nothing in that room.
 */
function hideEmptyLines(){
	var outerDivClass;
	var innerDivClass;
	var numouterDiv;
	var numinnerDiv;
	var outerDiv;
	var innerDiv;
	var innerDivsInouterDiv;
	var visibleSlots;
	var visibleinnerDivs;
	// set class names accordingly for different views
	// view is set in newschedule.mhtml
	if (view == "timeline") {
		outerDivClass = ".timebar_toparea";
		innerDivClass = ".timebar_subarea";
	} else if (view == "list"){
		outerDivClass = ".listDay";
		innerDivClass = ".listTime";
	}
	numouterDiv = $(outerDivClass).length;
	for (var i = 0; i < numouterDiv; i++) {
		// for each item with .outerDiv class
		outerDiv = $(outerDivClass).eq(i);
		innerDivsInouterDiv = outerDiv.find(innerDivClass + ":visible");
		numinnerDiv = innerDivsInouterDiv.length;
		for (var j = 0; j < numinnerDiv; j++) {
			// for each item with .innerDiv and visible under this outerDiv
			innerDiv = innerDivsInouterDiv.eq(j);
			// find count of visible items under this innerDiv
			// having if statement here might not be good..
			// should find better way to abstract this out
			if (view == "timeline") {
				// find visible slot in each timebar_row and hide one with no visible slot
				var row = innerDiv.find(".timebar_row");
				for (var k = 0; k < row.length; k++) {
					visibleSlots = row.eq(k).find(".timebar_slot:visible").length;
					if (visibleSlots < 1) {
						row.eq(k).hide();
					}
				}
				// set visibleSlots value to num of timebar_rows visible
				visibleSlots = row.find(":visible").length;
			} else if (view == "list") {
				visibleSlots = innerDiv.find(".locationSlot:visible").length;
			}
			// hide this innerDiv if nothing under this is visible
			if (visibleSlots < 1) {
				innerDiv.hide();
			}
		}
		visibleinnerDivs = outerDiv.find(innerDivClass + ":visible").length;
		// hide this outerDiv if nothing under this is visible
		if (visibleinnerDivs == 0) {
			outerDiv.hide();
		}
	}
}

/*
* itemsToShow should be selection, i.e. something like $(".className")
* Does not include code to show the actual items. Need to show parent divs
* if a child div is being shown.
*/
function showNeededLines(itemsToShow) {
	if (view == "list") {
		for (var i = 0; i < itemsToShow.length; i++) {
			itemsToShow.eq(i).parent().parent().parent().show();
			itemsToShow.eq(i).parent().parent().show();
			itemsToShow.eq(i).parent().show();
		}
	} else if (view == "timeline") {
		for (var i = 0; i < itemsToShow.length; i++) {
			itemsToShow.eq(i).parent().parent().parent().parent().show();
			itemsToShow.eq(i).parent().parent().parent().show();
			itemsToShow.eq(i).parent().parent().show();
			itemsToShow.eq(i).parent().show();
		}	
	}
}