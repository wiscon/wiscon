var nav_equivalent;
$(document).ready(function(){
	if ( $('#nav a.open').length == 0 ) {
		var current = nav_equivalent || 
						( window.location.pathname + window.location.hash );
		
		// console.log('looking for initial nav state;', current)
		var links = $('#nav a[href="' + current + '"]').first();
		if ( ! links.length ) {
			links = $('#nav a[href^="' + current + '"]').first();
		}
		if ( ! links.length ) {
			links = $('#nav a[href^="' + window.location.pathname + '"]').first();
		}
		// console.log('found match for', current, 'links=', links);
		if ( links.length ) {
			links.parent().addClass('current');
			var nav_group = links.closest('.nav-group').find('a.nav-group');
			if ( nav_group.length > 0 ) {				
				nav_group.addClass("open").next('ul').show();
			} else {
				nav_group = $('#nav a.nav-group').first();
				setTimeout( function () {
					nav_group.addClass("open").next('ul').slideDown(500);
				}, 500 );
			}
		} else {
			var nav_group = $('#nav a.nav-group').first();
			if ( current == '/' || current == '/index.php' ) {
				setTimeout( function () {
					nav_group.addClass("open").next('ul').slideDown(1000);
				}, 1000 );
			} else {
				nav_group.addClass("open").next('ul').show();
			}
		}
	}
	$("#nav a.nav-group").on("click", function(e){
		e.preventDefault();

		if(!$(this).hasClass("open")) {
			// hide any open menus and remove all other classes
			$("#nav li ul").slideUp(350);
			$("#nav li a").removeClass("open");

			// open our new menu and add the open class
			$(this).next("ul").slideDown(350);
			$(this).addClass("open");
		}

		else if($(this).hasClass("open")) {
			$(this).removeClass("open");
			$(this).next("ul").slideUp(350);
		}
	});
	$("#nav li li a").on("click", function(e){
		if(!$(this).hasClass("current")) {
			// this will be active
			// console.log('setting current li li a', this);
			$("#nav li li.current").removeClass("current");
			$(this).parent().addClass("current");
		}
	});
});