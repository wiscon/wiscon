DROP TABLE IF EXISTS `ChangeHistory`;
CREATE TABLE `ChangeHistory` (
  `idChangeHistory` int(11) NOT NULL auto_increment,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` int(11) NOT NULL,
  `TableName` varchar(45) NOT NULL,
  `FieldName` varchar(45) NOT NULL,
  `idRecord` int(11) NOT NULL,
  `OldValue` text,
  `NewValue` text,
  PRIMARY KEY  (`idChangeHistory`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `PasswordReset`;
CREATE TABLE `PasswordReset` (
  `idPasswordReset` int(11) NOT NULL auto_increment,
  `idUser` int(11) NOT NULL,
  `mailKey` varchar(45) default NULL,
  `CreateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UseTime` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateIP` varchar(15) default NULL,
  `UseIP` varchar(15) default NULL,
  PRIMARY KEY  (`idPasswordReset`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `ItemRanking`;
CREATE TABLE `ItemRanking` (
  `idRoles` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `UserInterestRanking` varchar(20) default NULL,
  `ModeratorFlag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRoles`),
  UNIQUE KEY `idUsers` (`idUsers`,`idItems`),
  KEY `fkRankingUser` (`idUsers`),
  KEY `fkRankingItems` (`idItems`),
  KEY `fkRankingPersInterest` (`UserInterestRanking`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Ranking done by profiles about items, usually in the program';
