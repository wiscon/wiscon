# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: wiscon.piglet.org (MySQL 5.0.45)
# Database: wiscon
# Generation Time: 2012-09-07 16:02:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table AppParameters
# ------------------------------------------------------------



# Dump of table CatalogItems
# ------------------------------------------------------------

LOCK TABLES `CatalogItems` WRITE;
/*!40000 ALTER TABLE `CatalogItems` DISABLE KEYS */;

INSERT INTO `CatalogItems` (`idCatalog`, `idConventions`, `SaleItemName`, `SaleItemDesc`, `UnitPrice`, `StartDate`, `EndDate`, `CapacityAmt`, `CapacityUnitCode`, `ConventionMembershipFlag`, `ConventionLimitType`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `PublicFlag`, `OrderBy`)
VALUES
	(1,15,'Adult','Adult Membership',45.00,'2009-01-01','2010-05-31',0,NULL,1,'members','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',1,NULL),
	(2,15,'Teen','Youth Membership, ages 13-18',20.00,'2009-01-01','2010-05-31',NULL,NULL,1,'members','2009-08-29 01:00:00','2009-08-29 00:00:00','jfh','jfh',1,NULL),
	(3,15,'ChildCare','Child Care Membership, 0-6',1.00,'2009-01-01','2010-05-31',35,'persons',1,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',1,NULL),
	(4,15,'Dessert','Dessert Ticket',15.00,'2009-01-01','2010-05-31',406,'persons',0,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',1,NULL),
	(5,15,'Guest','Guest of Honor Membership',0.00,'2009-01-01','2010-05-31',2,'persons',1,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(6,15,'Free','Free Membership',0.00,'2009-01-01','2010-05-31',NULL,NULL,1,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(7,15,'PastGuest','Past Guest of Honor',0.00,'2009-01-01','2010-05-31',NULL,NULL,1,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',0,NULL),
	(8,15,'Dealer1','Dealers Table',30.00,'2009-01-01','2010-05-31',NULL,NULL,0,'tables','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(9,15,'Dealer2','Second Dealers Table',50.00,'2009-01-01','2010-05-31',NULL,NULL,0,'tables','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(10,15,'Dealer3','Third Dealers Table',70.00,'2009-01-01','2010-05-31',NULL,NULL,0,'tables','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(11,NULL,'Donation','Donation',0.00,'2009-01-01',NULL,NULL,NULL,0,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',NULL,NULL),
	(12,15,'Supporting','Supporting Membership',15.00,'2009-01-01','2010-05-31',NULL,NULL,0,NULL,'2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh',1,NULL),
	(13,15,'Youth','Youth Membership, ages 7-12',20.00,'2009-01-01','2010-05-31',NULL,NULL,1,NULL,'2010-01-24 09:27:32','0000-00-00 00:00:00','','',1,NULL),
	(14,1,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(15,1,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(16,1,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(17,1,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(18,1,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(19,1,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(20,1,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(21,2,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(22,2,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(23,2,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(24,2,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(25,2,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(26,2,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(27,2,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(28,3,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(29,3,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(30,3,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(31,3,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(32,3,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(33,3,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(34,3,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(35,4,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(36,4,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(37,4,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(38,4,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(39,4,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(40,4,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(41,4,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(42,5,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(43,5,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(44,5,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(45,5,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(46,5,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(47,5,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(48,5,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(49,6,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(50,6,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(51,6,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(52,6,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(53,6,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(54,6,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(55,6,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(56,7,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(57,7,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(58,7,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(59,7,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(60,7,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(61,7,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(62,7,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(63,8,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(64,8,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(65,8,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(66,8,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(67,8,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(68,8,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(69,8,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(70,9,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(71,9,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(72,9,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(73,9,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(74,9,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(75,9,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(76,9,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(77,10,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(78,10,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(79,10,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(80,10,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(81,10,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(82,10,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(83,10,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(84,11,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(85,11,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(86,11,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(87,11,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(88,11,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(89,11,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(90,11,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(91,12,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(92,12,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(93,12,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(94,12,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(95,12,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(96,12,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(97,12,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(98,13,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(99,13,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(100,13,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(101,13,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(102,13,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(103,13,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(104,13,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(105,14,'Adult','Adult Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(106,14,'ChildCare','Child Care Membership, 0-6',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(107,14,'Dessert','Dessert Ticket',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(108,14,'Guest','Guest of Honor Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(109,14,'Free','Free Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(110,14,'Supporting','Supporting Membership',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(111,14,'Youth','Youth Membership, ages 7-12',0.00,'0001-01-01',NULL,NULL,NULL,1,NULL,'2010-02-10 16:59:47','0000-00-00 00:00:00','','',NULL,NULL),
	(112,15,'AdultAtCon','Adult At Con',50.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-11 00:00:00','2010-02-11 00:00:00','2084','2084',NULL,NULL),
	(113,NULL,'',NULL,0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:08:32','0000-00-00 00:00:00','','',NULL,NULL),
	(114,15,'YouthAtCon','Youth At Con',20.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-11 00:00:00','2010-02-11 00:00:00','2084','2084',NULL,NULL),
	(115,15,'AdultFriday','Adult 1 day Friday',20.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-11 00:00:00','2010-02-11 00:00:00','2084','2084',NULL,NULL),
	(116,15,'AdultSaturday','Adult 1 day Saturday',25.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:10:39','0000-00-00 00:00:00','','',NULL,NULL),
	(117,15,'AdultSunday','Adult 1 day Sunday',25.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:13:17','0000-00-00 00:00:00','','',NULL,NULL),
	(118,15,'YouthFriday','Youth 1 day Friday',5.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:13:37','0000-00-00 00:00:00','','',NULL,NULL),
	(119,15,'YouthSaturday','Youth 1 day Saturday',10.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:13:54','0000-00-00 00:00:00','','',NULL,NULL),
	(120,15,'YouthSunday','Youth 1 day Sunday',10.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:14:09','0000-00-00 00:00:00','','',NULL,NULL),
	(121,15,'Monday','Monday admissions',0.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-02-10 17:14:24','0000-00-00 00:00:00','','',NULL,NULL),
	(122,16,'Adult','Adult Membership',50.00,'2010-05-29','2011-05-31',0,NULL,1,'members','2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,1),
	(123,16,'Teen','Teen Membership, ages 13-18',20.00,'2010-05-29','2011-05-31',NULL,NULL,1,'members','2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,30),
	(124,16,'ChildCare','Child Care Membership, 0-6',1.00,'2010-05-29','2011-05-31',35,'persons',1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,50),
	(125,16,'Dessert','Dessert Ticket',15.00,'2010-05-29','2011-05-31',406,'persons',0,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,60),
	(126,16,'Guest','Guest of Honor Membership',0.00,'2010-05-29','2011-05-31',2,'persons',1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',0,21),
	(127,16,'Free','Free Membership',0.00,'2010-05-29','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,22),
	(128,16,'PastGuest','Past Guest of Honor',0.00,'2010-05-29','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',0,20),
	(129,16,'Dealer1','Dealers Table',30.00,'2011-01-01','2011-05-31',NULL,NULL,0,'tables','2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,90),
	(130,16,'Dealer2','Second Dealers Table',50.00,'2011-01-01','2011-05-31',NULL,NULL,0,'tables','2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,91),
	(131,16,'Dealer3','Third Dealers Table',70.00,'2011-01-01','2011-05-31',NULL,NULL,0,'tables','2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,92),
	(132,16,'Supporting','Supporting Membership',15.00,'2010-05-29','2011-05-31',NULL,NULL,0,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,55),
	(133,16,'Youth','Youth Membership, ages 7-12',20.00,'2010-05-29','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',1,40),
	(134,16,'AdultAtCon','Adult At Con',50.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,2),
	(135,16,'YouthAtCon','Youth At Con',25.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,41),
	(136,16,'AdultFriday','Adult 1 day Friday',20.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,3),
	(137,16,'AdultSaturday','Adult 1 day Saturday',25.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,4),
	(138,16,'AdultSunday','Adult 1 day Sunday',25.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,5),
	(139,16,'YouthFriday','Youth 1 day Friday',5.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,42),
	(140,16,'YouthSaturday','Youth 1 day Saturday',10.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,43),
	(141,16,'YouthSunday','Youth 1 day Sunday',10.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,44),
	(142,16,'Monday','Monday admissions',0.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-02-10 17:17:35','0000-00-00 00:00:00','','',NULL,48),
	(143,1,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(144,2,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(145,3,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(146,4,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(147,5,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(148,6,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(149,7,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,1,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(150,8,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(151,9,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(152,10,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(153,11,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(154,12,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(155,13,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(156,14,'PastGuest','Past Guest of Honor',0.00,'0000-00-00',NULL,NULL,NULL,0,NULL,'2010-02-10 17:36:32','0000-00-00 00:00:00','','',0,NULL),
	(157,15,'Ad','Advertising Order',0.00,'2009-01-01',NULL,NULL,NULL,0,NULL,'2010-04-04 11:50:32','0000-00-00 00:00:00','','',NULL,NULL),
	(158,15,'TeenAtCon','Teen At Con',20.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-04-27 05:58:48','0000-00-00 00:00:00','','',NULL,NULL),
	(160,15,'TeenFriday','Teen 1 day Friday',5.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-04-27 05:59:28','0000-00-00 00:00:00','','',NULL,NULL),
	(161,15,'TeenSaturday','Teen 1 day Saturday',10.00,'2010-05-15','2010-05-31',NULL,NULL,1,NULL,'2010-04-27 05:59:49','0000-00-00 00:00:00','','',NULL,NULL),
	(162,15,'TeenSunday','Teen 1 day Sunday',10.00,'2010-05-15','2010-05-15',NULL,NULL,1,NULL,'2010-04-27 06:00:27','0000-00-00 00:00:00','','',NULL,NULL),
	(163,16,'Journalist','Press Pass',0.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-05-19 13:35:57','0000-00-00 00:00:00','','',0,23),
	(164,15,'Journalist','Press Pass',0.00,'2010-05-14','2010-05-31',NULL,NULL,1,NULL,'2010-05-19 13:36:36','0000-00-00 00:00:00','','',0,NULL),
	(165,16,'TeenAtCon','Teen At Con',20.00,'2011-05-15','2011-05-15',NULL,NULL,1,NULL,'2010-05-21 00:00:00','2010-05-21 00:00:00','2084','2084',NULL,31),
	(166,16,'TeenFriday','Teen 1 day Friday',5.00,'2011-05-15','2011-05-15',NULL,NULL,1,NULL,'2010-05-21 00:00:00','2010-05-21 00:00:00','2084','2084',NULL,32),
	(167,16,'TeenSaturday','Teen 1 day Saturday',10.00,'2011-05-15','2011-05-15',NULL,NULL,1,NULL,'2010-05-21 00:00:00','2010-05-21 00:00:00','2084','2084',NULL,33),
	(168,16,'TeenSunday','Teen 1 day Sunday',10.00,'2011-05-15','2011-05-31',NULL,NULL,1,NULL,'2010-05-21 00:00:00','2010-05-21 00:00:00','2084','2084',NULL,34),
	(169,NULL,'SF3Stu2010','SF3 Youth 2010-2011',9.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:19:41','0000-00-00 00:00:00','','',0,90),
	(170,NULL,'SF3Bas2010','SF3 Basic/Student 2010-2011',12.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:20:20','0000-00-00 00:00:00','','',0,91),
	(171,NULL,'SF3Con2010','SF3 Contributing 2010-2011',24.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:20:53','0000-00-00 00:00:00','','',0,92),
	(172,NULL,'SF3Supp2010','SF3 Supporting 2010-2011',36.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:21:24','0000-00-00 00:00:00','','',0,93),
	(173,NULL,'SF3Sust2010','SF3 Sustaining 2010-2011',48.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:22:03','0000-00-00 00:00:00','','',0,94),
	(174,NULL,'SF3Pat2010','SF3 Patron 2010-2011',60.00,'2010-09-01','2011-08-31',NULL,NULL,0,NULL,'2010-10-07 05:22:32','0000-00-00 00:00:00','','',0,95),
	(175,16,'TipFree','Free Tiptree Winner Membership',0.00,'2010-10-14','2011-05-31',NULL,NULL,1,NULL,'2010-10-14 06:26:32','0000-00-00 00:00:00','jfh','jfh',0,71),
	(176,16,'TipDessert','Dessert ticket for Tiptree Winner',0.00,'2010-10-14','2011-05-31',NULL,NULL,0,NULL,'2010-10-14 06:27:11','0000-00-00 00:00:00','jfh','jfh',0,72),
	(177,36,'Adult','Adult Membership',50.00,'2011-05-28','2012-05-29',0,NULL,1,'members','2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,1),
	(178,36,'Teen','Teen Membership, ages 13-18',20.00,'2011-05-28','2012-05-29',NULL,NULL,1,'members','2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,30),
	(179,36,'ChildCare','Child Care Membership, 0-6',1.00,'2011-05-28','2012-05-29',35,'persons',1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,50),
	(180,36,'Dessert','Dessert Ticket',15.00,'2011-05-28','2012-05-29',406,'persons',0,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,60),
	(181,36,'Guest','Guest of Honor Membership',0.00,'2011-05-28','2012-05-29',2,'persons',1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,21),
	(182,36,'Free','Free Membership',0.00,'2011-05-28','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,22),
	(183,36,'PastGuest','Past Guest of Honor',0.00,'2011-05-28','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,20),
	(184,36,'Dealer1','1 Dealers Table',30.00,'2011-12-31','2012-05-29',NULL,NULL,0,'tables','2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,90),
	(185,36,'Dealer2','2 Dealers Tables',80.00,'2011-12-31','2012-05-29',NULL,NULL,0,'tables','2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,91),
	(186,36,'Dealer3','3 Dealers Tables',150.00,'2011-12-31','2012-05-29',NULL,NULL,0,'tables','2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,92),
	(187,36,'Supporting','Supporting Membership',15.00,'2011-05-28','2012-05-29',NULL,NULL,0,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,55),
	(188,36,'Youth','Youth Membership, ages 7-12',20.00,'2011-05-28','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,40),
	(189,36,'AdultAtCon','Adult At Con',50.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,2),
	(190,36,'YouthAtCon','Youth At Con',20.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,41),
	(191,36,'AdultFriday','Adult 1 day Friday',20.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,3),
	(192,36,'AdultSaturday','Adult 1 day Saturday',25.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,4),
	(193,36,'AdultSunday','Adult 1 day Sunday',25.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,5),
	(194,36,'YouthFriday','Youth 1 day Friday',5.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,42),
	(195,36,'YouthSaturday','Youth 1 day Saturday',10.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,43),
	(196,36,'YouthSunday','Youth 1 day Sunday',10.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,44),
	(197,36,'Monday','Monday admissions',0.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,48),
	(198,36,'Journalist','Press Pass',0.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,23),
	(199,36,'TeenAtCon','Teen At Con',20.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',NULL,31),
	(200,36,'TeenFriday','Teen 1 day Friday',5.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,32),
	(201,36,'TeenSaturday','Teen 1 day Saturday',10.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,33),
	(202,36,'TeenSunday','Teen 1 day Sunday',10.00,'2012-05-13','2012-05-29',NULL,NULL,1,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,34),
	(204,36,'FreeDessert','Free Dessert ticket',0.00,'2011-10-13','2012-05-29',NULL,NULL,0,NULL,'2011-01-26 20:09:48','0000-00-00 00:00:00','','',0,72),
	(205,37,'Adult','Adult Membership',50.00,'2012-05-26','2013-05-28',0,NULL,1,'members','2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,1),
	(206,37,'Teen','Teen Membership, ages 13-18',20.00,'2012-05-26','2013-05-28',NULL,NULL,1,'members','2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,30),
	(207,37,'ChildCare','Child Care Membership, 0-6',1.00,'2012-05-26','2013-05-28',35,'persons',1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,50),
	(208,37,'Dessert','Dessert Ticket',15.00,'2012-05-26','2013-05-28',406,'persons',0,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,60),
	(209,37,'Guest','Guest of Honor Membership',0.00,'2012-05-26','2013-05-28',2,'persons',1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',0,21),
	(210,37,'Free','Free Membership',0.00,'2012-05-26','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,22),
	(211,37,'PastGuest','Past Guest of Honor',0.00,'2012-05-26','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',0,20),
	(212,37,'Dealer1','1 Dealers Table',30.00,'2012-12-29','2013-05-28',NULL,NULL,0,'tables','2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,90),
	(213,37,'Dealer2','2 Dealers Tables',80.00,'2012-12-29','2013-05-28',NULL,NULL,0,'tables','2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,91),
	(214,37,'Dealer3','3 Dealers Tables',150.00,'2012-12-29','2013-05-28',NULL,NULL,0,'tables','2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,92),
	(215,37,'Supporting','Supporting Membership',15.00,'2012-05-26','2013-05-28',NULL,NULL,0,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,55),
	(216,37,'Youth','Youth Membership, ages 7-12',20.00,'2012-05-26','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',1,40),
	(217,37,'AdultAtCon','Adult At Con',50.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,2),
	(218,37,'YouthAtCon','Youth At Con',25.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,41),
	(219,37,'AdultFriday','Adult 1 day Friday',20.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,3),
	(220,37,'AdultSaturday','Adult 1 day Saturday',25.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,4),
	(221,37,'AdultSunday','Adult 1 day Sunday',25.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,5),
	(222,37,'YouthFriday','Youth 1 day Friday',5.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,42),
	(223,37,'YouthSaturday','Youth 1 day Saturday',10.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,43),
	(224,37,'YouthSunday','Youth 1 day Sunday',10.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,44),
	(225,37,'Monday','Monday admissions',0.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,48),
	(226,37,'Journalist','Press Pass',0.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',0,23),
	(227,37,'TeenAtCon','Teen At Con',20.00,'2013-05-12','2013-05-12',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,31),
	(228,37,'TeenFriday','Teen 1 day Friday',5.00,'2013-05-12','2013-05-12',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,32),
	(229,37,'TeenSaturday','Teen 1 day Saturday',10.00,'2013-05-12','2013-05-12',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,33),
	(230,37,'TeenSunday','Teen 1 day Sunday',10.00,'2013-05-12','2013-05-28',NULL,NULL,1,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',NULL,34),
	(231,37,'FreeDessert','Free Dessert ticket',0.00,'2012-05-26','2013-05-28',NULL,NULL,0,NULL,'2011-06-15 15:18:04','0000-00-00 00:00:00','','',0,72),
	(232,NULL,'SF3Youth','SF3 Youth',9.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(233,NULL,'SF3Basic','SF3 Basic/Student',12.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(234,NULL,'SF3Contributing','SF3 Contributing',24.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(235,NULL,'SF3Supporting','SF3 Supporting',36.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(236,NULL,'SF3Sustaining','SF3 Sustaining',48.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(237,NULL,'SF3Patron','SF3 Patron',60.00,'2011-09-01','2012-08-30',NULL,NULL,0,NULL,'2011-09-20 12:48:27','0000-00-00 00:00:00','','',NULL,NULL),
	(238,NULL,'SF3 Lifetime','SF3 Lifetime',0.00,'1990-01-01','2099-12-31',NULL,NULL,0,NULL,'2011-09-20 12:55:58','0000-00-00 00:00:00','','',0,NULL),
	(239,36,'Donation','Donation',0.00,'2011-05-29','2012-05-27',NULL,NULL,0,NULL,'2011-09-25 09:12:37','0000-00-00 00:00:00','','',1,NULL),
	(240,37,'Donation','Donation',0.00,'2012-05-28','2013-05-26',NULL,NULL,0,NULL,'2011-09-25 09:13:42','0000-00-00 00:00:00','','',1,NULL),
	(241,NULL,'Donation','Donation',0.00,'1980-01-01','2011-05-28',NULL,NULL,0,NULL,'2011-09-25 09:14:37','0000-00-00 00:00:00','','',NULL,NULL);

/*!40000 ALTER TABLE `CatalogItems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ConventionLimits
# ------------------------------------------------------------

LOCK TABLES `ConventionLimits` WRITE;
/*!40000 ALTER TABLE `ConventionLimits` DISABLE KEYS */;

INSERT INTO `ConventionLimits` (`idConventionLimits`, `idConventions`, `LimitType`, `LimitAmt`, `LimitComment`, `LimitUnits`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,15,'tables',46,'Total dealer tables for WisCon 34','tables','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(2,15,'members',1000,'Total membership of certain types','persons','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh');

/*!40000 ALTER TABLE `ConventionLimits` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Conventions
# ------------------------------------------------------------

LOCK TABLES `Conventions` WRITE;
/*!40000 ALTER TABLE `Conventions` DISABLE KEYS */;

INSERT INTO `Conventions` (`idConventions`, `ConventionName`, `StartDate`, `EndDate`, `CurrentFlag`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `ConventionOrder`)
VALUES
	(1,'WisCon 33','2009-05-22','2009-05-25',0,'2008-09-14 11:48:13','2008-09-14 00:00:00','Migration','Migration',33),
	(2,'WisCon 32','2008-05-23','2008-05-26',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',32),
	(3,'WisCon 31','2007-05-25','2007-05-28',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',31),
	(4,'WisCon 30','2006-05-26','2006-05-29',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',30),
	(5,'WisCon 29','2005-05-27','2005-05-30',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',29),
	(6,'WisCon 28','2004-05-28','2004-05-31',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',28),
	(7,'WisCon 27','2003-05-23','2003-05-26',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',27),
	(8,'WisCon 26','2002-05-24','2002-05-27',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',26),
	(9,'WisCon 25','2001-05-25','2001-05-28',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',25),
	(10,'WisCon 24','2000-05-26','2000-05-29',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',24),
	(11,'WisCon 23','1999-05-28','1999-05-31',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',23),
	(12,'WisCon 22','1998-05-22','1998-05-25',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',22),
	(13,'WisCon 21','1997-05-23','1997-05-26',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',21),
	(14,'WisCon 20','1996-05-24','1996-05-27',0,'2008-09-14 11:51:05','2008-09-14 00:00:00','Migration','Migration',20),
	(15,'WisCon 34','2010-05-28','2010-05-31',0,'2009-05-22 19:28:40','0000-00-00 00:00:00','','',34),
	(16,'WisCon 35','2011-05-27','2011-05-30',0,'2010-02-10 17:01:53','0000-00-00 00:00:00','','',35),
	(17,'WisCon 1','1977-02-11','1977-02-13',0,'2010-04-28 00:00:00','2010-04-28 00:00:00','Migration','Migration',1),
	(18,'WisCon 2','1978-02-17','1978-02-19',0,'2010-04-27 16:56:03','0000-00-00 00:00:00','','',2),
	(19,'WisCon 3','1979-02-02','1979-02-04',0,'2010-04-27 16:57:01','0000-00-00 00:00:00','','',3),
	(20,'WisCon 4','1980-03-07','1980-03-09',0,'2010-04-27 16:58:09','0000-00-00 00:00:00','','',4),
	(21,'WisCon 5','1981-03-06','1981-03-08',0,'2010-04-27 16:58:34','0000-00-00 00:00:00','','',5),
	(22,'WisCon 6','1982-03-05','1982-03-07',0,'2010-04-27 16:58:55','0000-00-00 00:00:00','','',6),
	(23,'WisCon 7','1983-03-04','1983-03-06',0,'2010-04-27 16:59:22','0000-00-00 00:00:00','','',7),
	(24,'WisCon 8','1984-02-24','1984-02-26',0,'2010-04-27 16:59:46','0000-00-00 00:00:00','','',8),
	(25,'WisCon 9','1985-02-22','1985-02-24',0,'2010-04-27 17:00:05','0000-00-00 00:00:00','','',9),
	(26,'WisCon 10','1986-02-21','1986-02-23',0,'2010-04-27 17:00:30','0000-00-00 00:00:00','','',10),
	(27,'WisCon 11','1987-02-20','1987-02-22',0,'2010-04-27 17:01:00','0000-00-00 00:00:00','','',11),
	(28,'WisCon 12','1988-02-19','1988-02-21',0,'2010-04-27 17:02:35','0000-00-00 00:00:00','','',12),
	(29,'WisCon 13','1989-02-17','1989-02-19',0,'2010-04-27 17:02:54','0000-00-00 00:00:00','','',13),
	(30,'WisCon 14','1990-03-09','1990-03-11',0,'2010-04-27 17:03:14','0000-00-00 00:00:00','','',14),
	(31,'WisCon 15','1991-03-01','1991-03-03',0,'2010-04-27 17:03:44','0000-00-00 00:00:00','','',15),
	(32,'WisCon 16','1992-03-06','1992-03-07',0,'2010-04-27 17:04:01','0000-00-00 00:00:00','','',16),
	(33,'WisCon 17','1993-03-05','1993-03-07',0,'2010-04-27 17:04:40','0000-00-00 00:00:00','','',17),
	(34,'WisCon 18','1994-03-04','1994-03-06',0,'2010-04-27 17:04:43','0000-00-00 00:00:00','','',18),
	(35,'WisCon 19','1995-05-26','1995-05-29',0,'2010-04-27 17:06:41','0000-00-00 00:00:00','','',19),
	(36,'WisCon 36','2012-05-25','2012-05-28',1,'2010-06-20 10:22:41','0000-00-00 00:00:00','','',36),
	(37,'WisCon 37','2013-05-24','2013-05-27',0,'2011-06-15 15:13:59','0000-00-00 00:00:00','','',37);

/*!40000 ALTER TABLE `Conventions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Locations
# ------------------------------------------------------------

LOCK TABLES `Locations` WRITE;
/*!40000 ALTER TABLE `Locations` DISABLE KEYS */;

INSERT INTO `Locations` (`idLocations`, `parentidLocations`, `LocationName`, `LocationType`, `PublicFlag`, `CapacityAmt`, `CapacityUnitCode`, `StartDate`, `EndDate`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `GridName`, `ShowOnGridFlag`, `GridOrderNo`)
VALUES
	(1,NULL,'Concourse','Facility',0,NULL,NULL,NULL,NULL,'2008-09-14 11:55:16','2008-09-14 00:00:00','Migration','Migration','',1,NULL),
	(2,NULL,'Room Of Ones Own','Facility',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Readings: Room of One\'s Own',1,190),
	(3,NULL,'Michelangelos','Facility',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Readings: Michelangelo\'s 114 State St.',1,180),
	(4,NULL,'Fair Trade Coffeehouse','Facility',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(5,NULL,'Inn on the Park','Facility',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(6,1,'Concourse 1','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(7,1,'Concourse 2','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(8,1,'Concourse 6','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(9,6,'Assembly','Room',1,1648,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Assembly',1,10),
	(10,6,'Caucus','Room',1,579,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Caucus',1,20),
	(11,6,'Senate','Room',0,1811,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','',0,NULL),
	(12,11,'Senate A','Room',1,886,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Senate A',1,30),
	(13,11,'Senate B','Room',1,924,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Senate B',1,40),
	(14,7,'Concourse Ballroom','Room',0,10201,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','',0,5),
	(15,14,'Madison','Room',0,3206,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(16,14,'Wisconsin','Room',1,2919,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Wisconsin',1,50),
	(17,14,'Capitol','Room',1,4075,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Capitol A/B',0,65),
	(18,17,'Capitol A','Room',1,2032,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Capitol A',1,60),
	(19,17,'Capitol B','Room',1,2043,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Capitol B',1,70),
	(20,14,'Capitol/Wisconsin','Room',1,6994,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2009-04-25 00:00:00','Migration','Migration','Capitol/Wisconsin',1,55),
	(21,7,'VIP Office','Room',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(22,7,'Conference 1','Room',0,604,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(23,7,'Conference 2','Room',1,628,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Conf Rm 2 Reading Track',1,80),
	(24,7,'Conference 3','Room',1,585,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Conf Rm 3 Academic Track',1,90),
	(25,7,'Conference 4','Room',1,588,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Conf Rm 4',1,100),
	(26,7,'Conference 5','Room',1,632,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','Conf Rm 5',1,110),
	(27,7,'University','Room',0,2265,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(28,27,'University A','Room',0,786,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(29,27,'University BCD','Room',0,1486,'sq ft',NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(30,8,'619','Room',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(31,8,'623','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','623',1,140),
	(32,8,'627','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','627',1,150),
	(33,8,'629','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','629',1,160),
	(34,8,'634','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','634',1,170),
	(35,8,'638','Room',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(36,8,'607','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','607 Reading Room',1,120),
	(37,8,'611','Room',1,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration','611 Kids Program Track',1,130),
	(38,8,'610','Room',0,NULL,NULL,NULL,NULL,'2008-09-14 11:58:01','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(39,5,'InnPark G','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(40,5,'InnPark 8','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(41,5,'InnPark 2','Floor',0,NULL,NULL,NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(42,39,'Vilas (Inn on the Park)','Room',1,1530,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration','Readings: Vilas, Inn on the Park',1,185),
	(43,42,'Vilas A','Room',0,750,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(44,42,'Vilas B','Room',0,780,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(45,39,'Doty','Room',0,1243,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(46,45,'Doty A','Room',0,333,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(47,45,'Doty B','Room',0,910,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(48,39,'Fairchild','Room',0,608,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(49,48,'Fairchild A','Room',0,304,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(50,48,'Fairchild B','Room',0,304,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(51,39,'Bascom','Room',0,665,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(52,41,'Hall of Wisconsin','Room',0,5208,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(53,52,'Madison IP','Room',0,3038,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(54,52,'University IP','Room',0,1470,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(55,41,'IotP Capitol','Room',0,1280,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(56,55,'Capitol IP East','Room',0,800,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(57,55,'Capitol IP West','Room',0,480,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(58,40,'Top of the Park','Room',0,2400,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(59,40,'Assembly IP','Room',0,600,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(60,40,'Gallery','Room',0,280,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(61,40,'Senate IP','Room',0,500,'sq ft',NULL,NULL,'2008-09-14 12:01:12','2008-09-14 00:00:00','Migration','Migration',NULL,0,NULL),
	(62,7,'Solitaire','Room',1,NULL,NULL,NULL,NULL,'2011-04-17 11:50:09','0000-00-00 00:00:00','','',NULL,NULL,NULL);

/*!40000 ALTER TABLE `Locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefAlternateDataTypes
# ------------------------------------------------------------

LOCK TABLES `RefAlternateDataTypes` WRITE;
/*!40000 ALTER TABLE `RefAlternateDataTypes` DISABLE KEYS */;

INSERT INTO `RefAlternateDataTypes` (`idDataTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'name','Personal name','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(2,'address','Mailing address','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(3,'email','Email address','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(4,'other','Other','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(5,'phone','Telephone','2011-11-17 08:52:26','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefAlternateDataTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefAlternateScopes
# ------------------------------------------------------------

LOCK TABLES `RefAlternateScopes` WRITE;
/*!40000 ALTER TABLE `RefAlternateScopes` DISABLE KEYS */;

INSERT INTO `RefAlternateScopes` (`idScopes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'all','All uses','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(2,'internal','Committee only','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(3,'curconn','Current convention members','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(4,'public','Internet and publications','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(5,'publications','Paper publications, not internet','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(6,'directory','SF3 directory','2011-11-17 08:53:05','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefAlternateScopes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefAssignmentStatus
# ------------------------------------------------------------

LOCK TABLES `RefAssignmentStatus` WRITE;
/*!40000 ALTER TABLE `RefAssignmentStatus` DISABLE KEYS */;

INSERT INTO `RefAssignmentStatus` (`idAssignmentStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(4,'standard','Standard assignment, manual','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(5,'auto','Automatic assignment','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(6,'locked','Locked, auto may not change','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(7,'other','Other, placeholder','2009-03-18 14:03:44','0000-00-00 00:00:00','2084','2084'),
	(8,'invalid','Invalid assignment, to be corrected','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(9,'suggested','Suggested by proposer','2010-02-25 20:55:33','0000-00-00 00:00:00','',''),
	(10,'suggestedmod','Moderator suggested by proposer','2010-02-25 20:56:15','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefAssignmentStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefAssignmentTypes
# ------------------------------------------------------------

LOCK TABLES `RefAssignmentTypes` WRITE;
/*!40000 ALTER TABLE `RefAssignmentTypes` DISABLE KEYS */;

INSERT INTO `RefAssignmentTypes` (`idAssignmentTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'participant','Participant','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(2,'moderator','Moderator','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(3,'other','Other','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(4,'hidden','Hidden','2009-04-11 12:24:39','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefAssignmentTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefAuthorityCodes
# ------------------------------------------------------------

LOCK TABLES `RefAuthorityCodes` WRITE;
/*!40000 ALTER TABLE `RefAuthorityCodes` DISABLE KEYS */;

INSERT INTO `RefAuthorityCodes` (`idCodes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `ActiveFlag`)
VALUES
	(1,'identity','User is the Person','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson',1),
	(2,'parent','User is parent of the Person','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson',1),
	(3,'purchaser','User bought the Person\'s membership','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson',1),
	(4,'other','Other connection','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson',1),
	(5,'family','Family Member','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson',1),
	(6,'reg','Registration Staff (obsolete role)','2010-05-09 17:05:03','0000-00-00 00:00:00','','',1),
	(7,'admin','Administrative (obsolete role)','2012-02-11 07:44:13','0000-00-00 00:00:00','','',1),
	(8,'chair','Convention Chair (obsolete role)','2012-02-11 07:44:28','0000-00-00 00:00:00','','',1),
	(9,'devel','Developer (obsolete role)','2012-02-11 07:44:51','0000-00-00 00:00:00','','',1),
	(10,'program','Program (obsolete role)','2012-02-11 07:45:10','0000-00-00 00:00:00','','',1),
	(11,'signup','Signup (obsolete role)','2012-02-11 07:45:55','0000-00-00 00:00:00','','',1);

/*!40000 ALTER TABLE `RefAuthorityCodes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefCapacityUnits
# ------------------------------------------------------------

LOCK TABLES `RefCapacityUnits` WRITE;
/*!40000 ALTER TABLE `RefCapacityUnits` DISABLE KEYS */;

INSERT INTO `RefCapacityUnits` (`idUnits`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'sq ft','Square Feet','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(2,'persons','#People','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(3,'items','#Items','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(4,'other','Other','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(5,'tables','Tables','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(6,'artpanels','Art Panels','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration');

/*!40000 ALTER TABLE `RefCapacityUnits` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefConnectionTypes
# ------------------------------------------------------------



# Dump of table RefConventionLimitTypes
# ------------------------------------------------------------

LOCK TABLES `RefConventionLimitTypes` WRITE;
/*!40000 ALTER TABLE `RefConventionLimitTypes` DISABLE KEYS */;

INSERT INTO `RefConventionLimitTypes` (`idLimitTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'tables','Maximum number of dealer tables','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(2,'members','Maximum number of counted members','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh');

/*!40000 ALTER TABLE `RefConventionLimitTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefDistributionStatus
# ------------------------------------------------------------

LOCK TABLES `RefDistributionStatus` WRITE;
/*!40000 ALTER TABLE `RefDistributionStatus` DISABLE KEYS */;

INSERT INTO `RefDistributionStatus` (`idDistributionStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'new','New','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(2,'complete','Complete','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(3,'canceled','Canceled','2012-02-10 15:21:51','0000-00-00 00:00:00','',''),
	(4,'tentative','Tentative','2012-02-10 15:22:01','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefDistributionStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefItemStatus
# ------------------------------------------------------------

LOCK TABLES `RefItemStatus` WRITE;
/*!40000 ALTER TABLE `RefItemStatus` DISABLE KEYS */;

INSERT INTO `RefItemStatus` (`idItemStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'proposed','Proposed item','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(2,'rejected','Rejected, do not use','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(3,'accepted','Accepted','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(4,'scheduled','Accepted and scheduled to a slot','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(5,'complete','Complete, may be published','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(6,'deferred','Deferred to future years','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(7,'approved','Approved','2012-02-11 07:57:04','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefItemStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefLineStatus
# ------------------------------------------------------------

LOCK TABLES `RefLineStatus` WRITE;
/*!40000 ALTER TABLE `RefLineStatus` DISABLE KEYS */;

INSERT INTO `RefLineStatus` (`idLineStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'new','New, not processed','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(2,'complete','Complete','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(3,'cancelled','Cancelled','2011-03-16 17:50:19','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefLineStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefLocationTypes
# ------------------------------------------------------------

LOCK TABLES `RefLocationTypes` WRITE;
/*!40000 ALTER TABLE `RefLocationTypes` DISABLE KEYS */;

INSERT INTO `RefLocationTypes` (`idLocationTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'facility','Facility (Concourse)','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(2,'room','Room','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(3,'floor','Floor','2012-02-10 16:27:34','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefLocationTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefLocTimeRankings
# ------------------------------------------------------------

LOCK TABLES `RefLocTimeRankings` WRITE;
/*!40000 ALTER TABLE `RefLocTimeRankings` DISABLE KEYS */;

INSERT INTO `RefLocTimeRankings` (`idLocTimeRankings`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(4,'required','Item must be in this location/time','2009-03-18 00:00:00','2009-03-18 00:00:00','2084','2084'),
	(5,'preferred','Good location or time for this item','2009-03-18 00:00:00','0000-00-00 00:00:00','2084','2084'),
	(6,'ok','Acceptable location or time for this item','2009-03-18 15:08:14','0000-00-00 00:00:00','2084','2084'),
	(7,'unacceptable','Do not put this item in this location/time','2009-03-18 15:08:39','0000-00-00 00:00:00','2084','2084');

/*!40000 ALTER TABLE `RefLocTimeRankings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefOrderMethod
# ------------------------------------------------------------

LOCK TABLES `RefOrderMethod` WRITE;
/*!40000 ALTER TABLE `RefOrderMethod` DISABLE KEYS */;

INSERT INTO `RefOrderMethod` (`idRef`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'mail','Mail','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(2,'wiscon','In person order at the convention','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(3,'web','Web order','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(4,'inperson','In person order at meeting or other','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(5,'other','Other order','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(6,'Online User','Online User','2012-02-11 07:36:24','0000-00-00 00:00:00','',''),
	(7,'Reg Desk','Reg Desk','2012-02-11 07:36:33','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefOrderMethod` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefOrderStatus
# ------------------------------------------------------------

LOCK TABLES `RefOrderStatus` WRITE;
/*!40000 ALTER TABLE `RefOrderStatus` DISABLE KEYS */;

INSERT INTO `RefOrderStatus` (`idOrderStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'new','New, not yet processed','2009-08-28 00:00:00','2009-08-28 00:00:00','jfh','jfh'),
	(2,'void','Void','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(3,'complete','Completed','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(4,'reserved','Order made, but not paid','2009-08-29 00:00:00','2009-08-29 00:00:00','jfh','jfh'),
	(5,'cancelled','Cancelled and Refunded','2011-03-16 17:53:41','0000-00-00 00:00:00','',''),
	(6,'incomplete','Incomplete','2012-02-11 07:35:02','0000-00-00 00:00:00','',''),
	(7,'refunded','Refunded','2012-02-11 07:35:12','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefOrderStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefPhoneType
# ------------------------------------------------------------

LOCK TABLES `RefPhoneType` WRITE;
/*!40000 ALTER TABLE `RefPhoneType` DISABLE KEYS */;

INSERT INTO `RefPhoneType` (`idRef`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(6,'home','Home','2011-11-17 08:56:00','0000-00-00 00:00:00','',''),
	(7,'work','Work','2011-11-17 08:56:06','0000-00-00 00:00:00','',''),
	(8,'cell','Cell','2011-11-17 08:56:12','0000-00-00 00:00:00','',''),
	(9,'other','Other','2011-11-17 08:56:19','0000-00-00 00:00:00','',''),
	(10,'fax','Fax','2011-11-17 14:10:44','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefPhoneType` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefPreferenceTypes
# ------------------------------------------------------------

LOCK TABLES `RefPreferenceTypes` WRITE;
/*!40000 ALTER TABLE `RefPreferenceTypes` DISABLE KEYS */;

INSERT INTO `RefPreferenceTypes` (`idPreferenceTypes`, `Code`, `Description`, `VisibleToUserFlag`, `EditByUserFlag`, `DisplayPrompt`, `TextFieldFlag`, `OrderBy`, `ConventionFlag`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `TextFieldPrompt`)
VALUES
	(5,'dealers','Dealer Information',1,1,'Receive Dealers Information',0,500,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084',''),
	(6,'artshow','Art Show Information',1,1,'Receive Art Show Information',0,550,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084',''),
	(7,'program','Program Information',1,1,'Receive Program Information',0,10,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084',''),
	(8,'bakesale','Bake Sale Information',1,1,'Receive Bake Sale Information',0,400,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084',''),
	(9,'ecube','Send ecube, if text field is populated that is a special email address for eCube',0,0,'Subscribe to eCube',1,100,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084','eMail address for eCube if different'),
	(10,'directory','Include in SF3 directory -- use start date and end date',0,0,'SF3 Directory',0,901,0,'2009-09-06 00:00:00','2009-09-06 00:00:00','2084','2084',''),
	(11,'refund','Request refund for worker/program. Text field is the method',1,1,'Send my Participant Refund (if eligible)',1,60,0,'2009-09-06 08:41:30','0000-00-00 00:00:00','','','Check or Paypal'),
	(12,'workshop','Writer\'s Workshop Information',1,1,'Receive Writers Workshop Information',0,30,0,'2010-01-17 00:00:00','2010-01-17 00:00:00','2084','2084',NULL),
	(13,'volunteer','Volunteering',1,1,'I would like to volunteer',0,300,0,'2010-01-17 00:00:00','2010-01-17 00:00:00','2084','2084',''),
	(14,'advertising','Advertising Information',1,1,'Receive Advertising Information',0,600,0,'2010-01-17 00:00:00','2010-01-17 00:00:00','2084','2084',NULL),
	(16,'vol_set','Has Volunteered',1,1,'I have volunteered already',1,310,0,'2011-02-15 00:00:00','2011-02-15 00:00:00','2084','2084','Working on'),
	(17,'no_mail','Do not send mailings',1,1,'Do not send me a brochure by mail',0,800,0,'2011-02-15 09:16:33','0000-00-00 00:00:00','','',NULL),
	(18,'access','Access request',1,1,'Request access accommodations',1,200,0,'2011-02-21 00:00:00','2011-02-21 00:00:00','2084','2084','Please describe:'),
	(19,'message_board','Include me on the message board?',1,1,'Include MYNAME on the at-con message board',0,5,0,'2011-05-05 14:23:44','0000-00-00 00:00:00','2087','',NULL);

/*!40000 ALTER TABLE `RefPreferenceTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefProgramFormats
# ------------------------------------------------------------

LOCK TABLES `RefProgramFormats` WRITE;
/*!40000 ALTER TABLE `RefProgramFormats` DISABLE KEYS */;

INSERT INTO `RefProgramFormats` (`idProgramFormats`, `Title`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'Panel','2008-12-22 07:35:21','2008-12-22 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net'),
	(2,'Solo Presentation','2008-12-22 07:35:21','2008-12-22 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net'),
	(3,'Roundtable','2008-12-22 07:35:21','2008-12-22 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net'),
	(4,'Other Format (Describe Below)','2008-12-22 07:35:21','2008-12-22 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net');

/*!40000 ALTER TABLE `RefProgramFormats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefProgramItemTypes
# ------------------------------------------------------------

LOCK TABLES `RefProgramItemTypes` WRITE;
/*!40000 ALTER TABLE `RefProgramItemTypes` DISABLE KEYS */;

INSERT INTO `RefProgramItemTypes` (`idProgramItemTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `ActiveFlag`)
VALUES
	(1,'Party','Scheduled Party','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(2,'Reading','Scheduled Reading','2009-02-28 05:48:18','2009-02-28 00:00:00','Jim','Jim',1),
	(3,'Program','Standard Program Item','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(4,'Private','Private meeting, such as the Tiptree Mother Board','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(5,'Event','Event, such as opening ceremonies','2009-02-28 05:50:39','2009-02-28 00:00:00','Jim','Jim',1),
	(6,'Other','Other type of item','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(7,'Academic','Academic program item (needs special handling)','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(8,'Continuing','Continuing operation, such as registration (may not get into the database for W33)','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(9,'Workshop','Writers Workshop Session','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(10,'Gathering','Part of the Gathering','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1),
	(11,'Kids','Kids and Teens','2009-02-28 00:00:00','2009-02-28 00:00:00','Jim','Jim',1);

/*!40000 ALTER TABLE `RefProgramItemTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefProgramTracks
# ------------------------------------------------------------

LOCK TABLES `RefProgramTracks` WRITE;
/*!40000 ALTER TABLE `RefProgramTracks` DISABLE KEYS */;

INSERT INTO `RefProgramTracks` (`idProgramTracks`, `Name`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`, `PanelistDescription`, `DisplayOrder`)
VALUES
	(1,'Science and Technology','<p>Do you want to give a solo presentation on technologies that could transform our lives, or have already transformed yours?   Do you want to organize a panel of experts on the social, economic, or political implications of current directions in research in science and technology?   Do you want to compare the ways that science and technology are represented in science fiction with the way they&apos;re represented in the press or taught in schools and colleges?   Do you want to lead a discussion on a particular application of science and technology that you think more people should know about and/or take action about?   We&apos;re interested in programming proposals on any of the above and we&apos;re willing and eager to have our horizons expanded further in terms of topic or programming format.</p>\r\n\r\n<p>Whilst your suggestion need not be explicitly focused through the lens of science fiction, we take it for granted that you will recognize that science fiction fans have particular, and perhaps critical, investments in the possibilities offered by science and technology.   Linking your explorations of science and technology in social reality to similar or conflicting visions in science fiction will likely make your session more resonant for your audience.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Are you interested in a panel of experts on the social, economic, or political implications of current directions in research in science and technology, comparisons of the ways that science and technology are represented in science fiction with the way they\'re represented in the press or taught in schools and colleges, or a discussion on a particular application of science and technology? Then this is the track for you.</p>\r\n\r\n<p>This programming may not be explicitly focused through the lens of science fiction, but we take it for granted that science fiction fans have particular, and perhaps critical, investments in the possibilities offered by science and technology. Linking explorations of science and technology in social reality to similar or conflicting visions in science fiction will likely make programs more resonant for the WisCon audience.',4),
	(2,'Spirituality, Organized Religion and Politics','<p>Does the mere title of this track as part of a feminist science fiction convention confuse or concern you?  Are you someone who feels that organized religion has no place in a rational, scientific world?  Are you a person of faith who feels less than welcome in fandom?  Are you concerned that religious conviction has become the litmus test for candidates at all levels of America politics?  Are you a Christian who doesn&apos;t belong to the Christian Right and feels misrepresented by them?  Are you a non-Christian who wants to remind the world that not all people of faith are Christians? Do you want to explore science fiction works that focus on religious themes such as conceptions of an afterlife, sin and morality?  Are you excited about the innovative and creative ways science fiction authors have envisioned new spiritualities or developed twists on current religions that skillfully weave in ancient world mythologies?</p>\r\n\r\n<p>In past years, some of the topics we&apos;ve discussed in this area include: morality in the absence of organized religion; the absence of morality in some organized religions; whether there is a Religious Left and what it&apos;s doing in the world; the state of American politics around religious issues and issues that are being influenced by some religions; the treatment of Muslims in a post-9/11 world; and the growing popularity and acceptance of spirituality sometimes combined with, and sometimes in contrast to organized religion-and,  of course, how all of this is represented, or sometimes not, in science fiction and fantasy, and in fandom. Recognizing how important these issues are to those involved, and the passion involved, we look forward to the opportunity to learn from and share with one another on these and related topics.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Does the mere title of this track as part of a feminist science fiction convention confuse or concern you? Are you someone who feels that organized religion has no place in a rational, scientific world? Are you a person of faith who feels less than welcome in fandom? Are you concerned that religious conviction has become the litmus test for candidates at all levels of America politics? Are you a Christian who doesn\'t belong to the Christian Right and feels misrepresented by them? Are you a non-Christian who wants to remind the world that not all people of faith are Christians? Do you want to explore science fiction works that focus on religious themes such as conceptions of an afterlife, sin and morality? Are you excited about the innovative and creative ways science fiction authors have envisioned new spiritualities or developed twists on current religions that skillfully weave in ancient world mythologies?</p>\r\n\r\n<p>In past years, some of the topics we\'ve discussed in this area include: morality in the absence of organized religion; the absence of morality in some organized religions; whether there is a Religious Left and what it\'s doing in the world; the state of American politics around religious issues and issues that are being influenced by some religions; the treatment of Muslims in a post-9/11 world; and the growing popularity and acceptance of spirituality sometimes combined with, and sometimes in contrast to organized religion-and, of course, how all of this is represented, or sometimes not, in science fiction and fantasy, and in fandom. Recognizing how important these issues are to those involved, and the passion involved, we look forward to the opportunity to learn from and share with one another on these and related topics.',3),
	(3,'Feminism and Other Social Change Movements','<p>Is your first thought on reading this track title, <em>which</em> feminism? Or, <em>whose</em> feminism? Do you think \"feminism and other social change\" is redundant, because to you feminism encompasses all other axes of social change organizing and should be, to borrow a phrase, the Prime Directive? Do you fear that for too many people (at WisCon and beyond), feminism is imagined no more widely or progressively than \"equal pay for equal work (for white middle-class women)?\" Are you energized by the prospect of discussing the various things we all mean when we say \"feminism\" - which perspectives, issues, and values are inherent and which anathema - and likewise with social change movements organized around \"race,\" class, disability, sexuality, nationality, and so on?</p>\r\n\r\n<p>Do you see infinite tentacles of connection and common cause among social change movements such as feminism, anti-racism, disability rights, GLBT equality, and anti-imperialism?</p>\r\n\r\n<p>In previous years, WisCon programming in these areas has examined progressive notions of justice in the context of the U.S. law enforcement system, mined science fiction for ideas about effecting radical social change without the use of violence, examined recent and historical science fiction through the lens of post-colonial theory, and talked directly about the evolution of social change movements in the \"real world\" where science fiction fans live and work. We look forward to your programming ideas that invigorate our understanding of, and commitment to, social change in all its forms.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Is your first thought on reading this track title, which feminism? Or, whose feminism? Do you think \"feminism and other social change\" is redundant, because to you feminism encompasses all other axes of social change organizing and should be, to borrow a phrase, the Prime Directive? Do you fear that for too many people (at WisCon and beyond), feminism is imagined no more widely or progressively than \"equal pay for equal work (for white middle-class women)?\" Are you energized by the prospect of discussing the various things we all mean when we say \"feminism\" – which perspectives, issues, and values are inherent and which anathema – and likewise with social change movements organized around race, class, disability, sexuality, nationality, and so on?</p>\r\n\r\n<p>Do you see infinite tentacles of connection and common cause among social change movements such as feminism, anti-racism, disability rights, GLBT equality, and anti-imperialism?</p>\r\n\r\n<p>In previous years, WisCon programming in this track has examined progressive notions of justice in the context of the U.S. law enforcement system, mined science fiction for ideas about effecting radical social change without the use of violence, examined recent and historical science fiction through the lens of post-colonial theory, and talked directly about the evolution of social change movements in the \"real world\" where science fiction fans live and work.',1),
	(4,'Power, Privilege, and Oppression','<p>Are you excited by the ways science fiction and fantasy can be used to explore social constructs such as race or sexuality? Are you fascinated with science fiction that blows away the attitudinal barriers that currently exist for people with disabilities? Does your favorite science fiction challenge dichotomous thinking on gender? Are you interested in joining the on-going WisCon discussion of cultural appropriation in science fiction and fantasy?</p>\r\n\r\n<p>Science fiction can serve as a tool to challenge the status quo, empower the disenfranchised, and create worlds that redistribute power and rethink privilege.  Many of us have been drawn to science fiction and fantasy because we&apos;ve been marginalized as \"others.\" We want to imagine a world in which we are accepted and respected. We invite programming proposals that discuss these issues, opening up more and increasingly complex dialogues, in the context of science fiction, fandom, and the world at large.</p>\r\n\r\n<p>We are looking for programming ideas that use science fiction and fantasy to examine the ways power and privilege are used to exercise, consolidate, and maintain control over oppressed individuals and groups. How do our identities (whether chosen or imposed) privilege or oppress us and others? Although we recognize the importance of specific programming to explore particular identities, we encourage ideas that promote diverse personal and political perspectives. While the identities and standpoints vary and overlap, the mechanisms of subjugation are often the same. As the t-shirt says, \"same struggle, different difference.\"</p>\r\n\r\n<p>Previous WisCon programs on these topics have used science fiction and fantasy texts, concepts, and ideas to illustrate and explore the axes around which relations of domination and/or identity politics have been organized, such as class, gender, race, ability, sexuality and body image.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Are you excited by the ways science fiction and fantasy can be used to explore social constructs such as race or sexuality? Are you fascinated with science fiction that blows away the attitudinal barriers that currently exist for people with disabilities? Are you interested in joining the passionate and on-going WisCon discussion about cultural appropriation in science fiction and fantasy?</p>\r\n\r\n<p>Science fiction can serve as a tool to challenge the status quo, empower the disenfranchised, and create worlds that redistribute power and rethink privilege. Previous WisCon programs in this track have used science fiction and fantasy texts, concepts, and ideas to illustrate and explore the axes around which relations of domination and/or identity politics have been organized, such as class, gender, race, ability, sexuality and body image. How do our identities (whether chosen or imposed) privilege or oppress us and others? Although we recognize the vital importance of specific programming to explore particular identities, we encourage discussion that promotes diverse personal and political perspectives. While the identities and standpoints vary and overlap, the mechanisms of subjugation are often the same. As the t-shirt says, \"same struggle, different difference.\"',2),
	(5,'The Craft and Business of Writing','<p>Want to be a feminist science fiction writer when you grow up? Are you an editor or agent who wants to share your insight? Do you want to workshop out your writer&apos;s block, or brainstorm plots, or learn how to construct well-rounded characters who do not share your background and experiences? Do you wish to discuss markets, or an aspect of day-to-day practice, or tools, or techniques? Then there&apos;s a place for you in WisCon programming.</p>\r\n\r\n<p>We are interested in programming proposals that address the nuts and bolts of the writing craft. Solo presentations, peer-led workshops, and traditional panels are all possible formats to consider - and if you have another format you want to try, we&apos;re interested! Proposals that wrestle with issues of feminism, gender, class, sexuality, \"race,\" disability, and other related issues in the context of writing practice are especially welcome.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Want to be a feminist science fiction writer when you grow up? Are you an editor or agent who wants to share your insight? Do you want to workshop out your writer\'s block, or brainstorm plots, or learn how to construct well-rounded characters who do not share your background and experiences? Do you wish to discuss markets, or an aspect of day-to-day practice, or tools, or techniques? Then there\'s a place for you in this track.</p>\r\n\r\n<p>Programming in this track addresses the nuts and bolts of the writing craft, and include topics that wrestle with issues of feminism, gender, class, sexuality, race, disability, and other related issues in the context of writing practice.',5),
	(6,'Reading, Viewing, and Critiquing Science Fiction','<p>Is a tight focus on the ins and outs of science fiction texts the main draw for you, whether we&apos;re talking about the feminist science fiction and fantasy novels and short fiction that have been WisCon&apos;s traditional focus, or the proliferation of other F&SF media, including film, television and gaming?   Could you offer WisCon attendees a novel way to read &apos;classic&apos; feminist SF texts, or do you want to focus on the latest works of our current Guests of Honor?  Are you burning to discuss the gender and ethnicity stereotyping that has characterized Season Two of <i>Heroes</i>? Or the more subtle gender dynamics in <i>The Golden Compass</i>? Wondering why spunky YA heroines all seem to be cast from the same mold? Have you found a new feminist novel that you love, and want to spread the word? Want to critique the lack of people of color, or the persistence of normative gender roles, or the class politics in a particular vision of the future, be it written or viewed? This is the place for discussions centered around books, movies, TV shows, and other media. </p>\r\n\r\n<p>Past WisCon panels in this category have discussed the theory and practice of slash fanfic; the lack of diversity in SF/F television; appreciations and discussions of <i>Buffy</i>, <i>The Rocky Horror Picture Show</i>, <i>Battlestar Galactica</i>, and <i>Ginger Snaps</i>; a look at women in horror films and in the X-Men comics; fairy tales and feminism; and an attempt to answer the question, \"what do writers owe their readers?</p>\r\n\r\n<p>WisCon is thrilled to be a convention that disrupts hierarchies amongst writers, fans, academics, fans, editors, fans and other fans!   We hope we&apos;re all fans of some intersection of feminism and science fiction, and that our shared passion and knowledge will enrich the conversations - whatever label we or others choose to apply to our particular location in the field.  So if you&apos;re wondering whether you&apos;re qualified to participate in this track, wonder no longer: you are.</p>','2008-12-18 07:35:21','2008-12-18 07:35:35','jhkim@darkshire.net','jhkim@darkshire.net','Is a tight focus on the ins and outs of science fiction texts the main draw for you, whether we\'re talking about the feminist science fiction and fantasy novels and short fiction that have been WisCon\'s traditional focus, or the proliferation of other F&SF media, including film, television and gaming? This is the place for discussions centered around books, movies, TV shows, and other media.</p>\r\n\r\n<p>Past programming in this track has discussed the theory and practice of slash fanfic; the lack of diversity in SF/F television; appreciations and discussions of Buffy, The Rocky Horror Picture Show, Battlestar Galactica, and Ginger Snaps; a look at women in horror films and in the X-Men comics; fairy tales and feminism; and an attempt to answer the question, \"what do writers owe their readers?\"</p>\r\n\r\n<p>WisCon is thrilled to be a convention that disrupts hierarchies amongst writers, fans, academics, fans, editors, fans and other fans! We hope we\'re all fans of some intersection of feminism and science fiction, and that our shared passion and knowledge will enrich the conversations – whatever label we or others choose to apply to our particular location in the field. So if you\'re wondering whether you\'re qualified to participate in this track, wonder no longer: you are.',6),
	(8,'The SignOut','Come and sign your works, come and get things signed, come and hang out and wind down before you leave.','2009-02-20 21:26:32','0000-00-00 00:00:00','Jim','Jim',NULL,8),
	(10,'Join the Moderator Pool','At WisCon, program participants provide the content, while moderators guide the process. This means moderators don’t require expertise on a particular topic. Some people have moderation expertise from their mundane life and others would like to use and improve their moderating skills. If you want to volunteer to moderate a specific item, please indicate your interest at the individual description.  By selecting one of the two options (which are described below), you\'re volunteering to be part of a general pool. We\'ve got some great tools to help our moderators prepare, including a set of tips and a private discussion group.','2009-02-26 00:00:00','2009-02-26 00:00:00','Jim','Jim',NULL,10),
	(11,'Fandom as a Way of Life','Fandom--the collection of participatory cultures that appreciate, debate, analyze, and interpret various expessions of popular culture--is itself a subculture that has its own jargon, traditions, politics, and folkways.<p>If you\'re interested in discussing fandom as a way of life--that is, the cultural practices that cut across all fandoms as well as the sometimes serious business of producing and running conventions, fan publications, and fannish organizations--this track is for you.','2011-02-02 10:18:28','0000-00-00 00:00:00','piglet','',NULL,7);

/*!40000 ALTER TABLE `RefProgramTracks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefRankingTypes
# ------------------------------------------------------------

LOCK TABLES `RefRankingTypes` WRITE;
/*!40000 ALTER TABLE `RefRankingTypes` DISABLE KEYS */;

INSERT INTO `RefRankingTypes` (`idRankingTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'1','Please!','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson'),
	(2,'2','Interested','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson'),
	(3,'3','OK','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson'),
	(4,'9','Not Interested','2009-01-22 00:00:00','2009-01-22 00:00:00','hudson','hudson');

/*!40000 ALTER TABLE `RefRankingTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefRoleTypes
# ------------------------------------------------------------

LOCK TABLES `RefRoleTypes` WRITE;
/*!40000 ALTER TABLE `RefRoleTypes` DISABLE KEYS */;

INSERT INTO `RefRoleTypes` (`idRoleTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'admin','Application administrator','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(2,'devel','Developer','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(3,'program','Convention program','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(4,'treasury','Treasury/Finance','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(5,'reg','Registration','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(6,'committee','ConCom','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(7,'coordinator','Convention Coordinator','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(8,'chair','Chair','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(9,'dealers','Dealers Room','2012-03-03 11:27:05','0000-00-00 00:00:00','2087',''),
	(10,'volunteer','Volunteer Coordinator','2012-03-03 11:27:14','0000-00-00 00:00:00','2087',''),
	(11,'web','Webmistress','2012-03-03 11:27:22','0000-00-00 00:00:00','2087',''),
	(12,'signup','Programming Signup','2012-03-19 05:40:24','0000-00-00 00:00:00','2087','');

/*!40000 ALTER TABLE `RefRoleTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefSalutations
# ------------------------------------------------------------

LOCK TABLES `RefSalutations` WRITE;
/*!40000 ALTER TABLE `RefSalutations` DISABLE KEYS */;

INSERT INTO `RefSalutations` (`idSalutations`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'Ms','','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(2,'Mr.','','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(3,'Dr.','','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration'),
	(4,'Prof.','','2008-09-14 11:32:20','2008-09-14 00:00:00','Migration','Migration');

/*!40000 ALTER TABLE `RefSalutations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefScheduleStatus
# ------------------------------------------------------------

LOCK TABLES `RefScheduleStatus` WRITE;
/*!40000 ALTER TABLE `RefScheduleStatus` DISABLE KEYS */;

INSERT INTO `RefScheduleStatus` (`idScheduleStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(15,'current','Current','2010-08-08 12:22:13','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefScheduleStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefTransactionSources
# ------------------------------------------------------------

LOCK TABLES `RefTransactionSources` WRITE;
/*!40000 ALTER TABLE `RefTransactionSources` DISABLE KEYS */;

INSERT INTO `RefTransactionSources` (`idTransactionSource`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'other','Other','2009-09-02 15:15:49','0000-00-00 00:00:00','',''),
	(2,'check','Check','2009-09-02 15:15:57','0000-00-00 00:00:00','',''),
	(3,'paypal','Paypal','2009-09-02 15:16:08','0000-00-00 00:00:00','',''),
	(4,'mc','Mastercard','2009-09-02 15:16:19','0000-00-00 00:00:00','',''),
	(5,'visa','Visa','2009-09-02 15:16:28','0000-00-00 00:00:00','',''),
	(6,'cash','Cash','2009-09-02 15:16:38','0000-00-00 00:00:00','',''),
	(7,'scholarship','Transfer from Scholarship Fund','2012-03-28 04:56:54','0000-00-00 00:00:00','',''),
	(8,'tiptree','Charge to the Tiptree Award','2012-03-28 04:57:09','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefTransactionSources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefTransactionStatus
# ------------------------------------------------------------

LOCK TABLES `RefTransactionStatus` WRITE;
/*!40000 ALTER TABLE `RefTransactionStatus` DISABLE KEYS */;

INSERT INTO `RefTransactionStatus` (`idTransactionStatus`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'new','New','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(2,'complete','Complete, funds received','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(3,'void','Void','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(4,'nsf','Not Sufficient Funds','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(5,'refunded','Refunded','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(6,'clearing','Waiting to clear','2008-09-14 11:34:56','2008-09-14 00:00:00','Migration','Migration'),
	(7,'tentative','Tentative','2012-02-10 16:04:07','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefTransactionStatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefTransactionTypes
# ------------------------------------------------------------

LOCK TABLES `RefTransactionTypes` WRITE;
/*!40000 ALTER TABLE `RefTransactionTypes` DISABLE KEYS */;

INSERT INTO `RefTransactionTypes` (`idTransactionTypes`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(1,'payment','Payment','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(2,'refund','Refund','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(3,'debitmemo','Debit Memo','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(4,'creditmemo','Credit Memo','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(5,'other','Other','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration'),
	(6,'void','Void','2008-09-14 11:35:39','2008-09-14 00:00:00','Migration','Migration');

/*!40000 ALTER TABLE `RefTransactionTypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RefURLType
# ------------------------------------------------------------

LOCK TABLES `RefURLType` WRITE;
/*!40000 ALTER TABLE `RefURLType` DISABLE KEYS */;

INSERT INTO `RefURLType` (`idRef`, `Code`, `Description`, `CreateDate`, `UpdateDate`, `CreateUser`, `UpdateUser`)
VALUES
	(6,'url','Web url','2011-11-17 09:37:10','0000-00-00 00:00:00','',''),
	(7,'facebook','Facebook user','2011-11-17 09:37:19','0000-00-00 00:00:00','',''),
	(8,'twitter','Twitter ID','2011-11-17 09:37:28','0000-00-00 00:00:00','',''),
	(9,'livejournal','Live Journal user','2011-11-17 09:37:45','0000-00-00 00:00:00','',''),
	(10,'other','Other user id','2011-11-17 14:11:46','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `RefURLType` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
