CREATE TABLE `AlternateNames` (
  `idAlternateNames` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL default '0',
  `StartDate` date NOT NULL default '0000-00-00' COMMENT 'Start date for using this alternate name',
  `EndDate` date default NULL COMMENT 'End date for using this alternate data',
  `AlternateScope` varchar(20) NOT NULL default '' COMMENT 'Public, current convention, all conventions, secondary email, etc.',
  `SeasonalRepeatsFlag` tinyint(1) NOT NULL default '0' COMMENT 'If true, dates repeat each year, for example for a seasonal address',
  `AlternateDataType` varchar(20) NOT NULL default '' COMMENT 'Name, email, address',
  `FullName` varchar(200) default NULL,
  `Line1Address` varchar(200) default NULL,
  `Line2Address` varchar(200) default NULL,
  `City` varchar(45) default NULL,
  `StateProvince` varchar(45) default NULL,
  `ZipPostalCode` varchar(45) default NULL,
  `Country` varchar(45) default NULL,
  `PersonEmail` varchar(200) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idAlternateNames`),
  KEY `fkAltNamesPersons` (`idPersons`),
  KEY `fkAltNamesScope` (`AlternateScope`),
  KEY `fkAltNamesDataType` (`AlternateDataType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='The idea here is that the primary name, email, and address a';

CREATE TABLE `AppParameters` (
  `idParameters` int(11) NOT NULL auto_increment,
  `Environment` varchar(20) NOT NULL default '',
  `Build` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idParameters`),
  UNIQUE KEY `RefCode` (`Environment`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Single Row table containing basic useful information, such a';

CREATE TABLE `Assignments` (
  `idAssignments` int(11) NOT NULL auto_increment,
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL,
  PRIMARY KEY  (`idAssignments`),
  KEY `fkAssignmentItem` (`idItems`),
  KEY `fkAssignmentPersons` (`idPersons`),
  KEY `fkAssignmentType` (`AssignmentType`),
  KEY `iAssignmentsTemp` (`idPersons`,`idItems`)
) ENGINE=MyISAM AUTO_INCREMENT=7037 DEFAULT CHARSET=latin1 COMMENT='Person will be doing something for the item. For example, pa';

CREATE TABLE `AssignmentSuggested` (
  `idAssignments` int(11) NOT NULL default '0',
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `AutoAssignmentParameters` (
  `idParam` int(11) NOT NULL auto_increment,
  `Name` varchar(45) NOT NULL default '',
  `Value` varchar(255) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idParam`),
  UNIQUE KEY `RefCode` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE `BadgeSorts` (
  `idBadgeSorts` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `BadgeOrder` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idBadgeSorts`)
) ENGINE=MyISAM AUTO_INCREMENT=9141 DEFAULT CHARSET=latin1 COMMENT='Persist the badge sort. Refresh by badge printing, used for ';

CREATE TABLE `CatalogItems` (
  `idCatalog` int(11) NOT NULL auto_increment,
  `idConventions` int(11) default NULL COMMENT 'Optional foreign key. Some sales items may not be for a particular convention.',
  `SaleItemName` varchar(45) NOT NULL COMMENT 'Name, such as "Adult Membership"',
  `SaleItemDesc` varchar(4000) default NULL COMMENT 'Description of what this sales item is',
  `UnitPrice` decimal(6,2) NOT NULL COMMENT 'What it costs',
  `StartDate` date NOT NULL COMMENT 'First sale date',
  `EndDate` date default NULL COMMENT 'Last sale date',
  `CapacityAmt` int(11) default NULL COMMENT 'Used for single capacity limits, like child care memberships or dealers tables',
  `CapacityUnitCode` varchar(20) default NULL,
  `ConventionMembershipFlag` tinyint(1) NOT NULL default '0' COMMENT 'Is this a convention membership?',
  `ConventionLimitType` varchar(20) default NULL COMMENT 'Does this count towards an overall convention limit? If so, which one.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PublicFlag` tinyint(1) default NULL,
  `OrderBy` int(3) default NULL,
  PRIMARY KEY  (`idCatalog`),
  KEY `fkCatalogConvention` (`idConventions`),
  KEY `fkCatalogCapacityUnits` (`CapacityUnitCode`),
  KEY `fkCatalogConventionLimit` (`ConventionLimitType`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=latin1 COMMENT='What we''re selling, most tied to a particular convention';

CREATE TABLE `ChangeHistory` (
  `idChangeHistory` int(11) NOT NULL auto_increment,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` int(11) NOT NULL,
  `TableName` varchar(45) NOT NULL,
  `FieldName` varchar(45) NOT NULL,
  `idRecord` int(11) NOT NULL,
  `OldValue` text,
  `NewValue` text,
  PRIMARY KEY  (`idChangeHistory`)
) ENGINE=MyISAM AUTO_INCREMENT=35805 DEFAULT CHARSET=latin1;

CREATE TABLE `ConventionLimits` (
  `idConventionLimits` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `LimitType` varchar(20) NOT NULL,
  `LimitAmt` int(11) NOT NULL,
  `LimitComment` varchar(4000) default NULL,
  `LimitUnits` varchar(20) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idConventionLimits`),
  KEY `fkLimitConvention` (`idConventions`),
  KEY `fkLimitUnits` (`LimitUnits`),
  KEY `fkLimitType` (`LimitType`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Capacity limits for a convention. Enforced by programmatic c';

CREATE TABLE `Conventions` (
  `idConventions` int(11) NOT NULL auto_increment,
  `ConventionName` varchar(45) NOT NULL default '',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date NOT NULL default '0000-00-00',
  `CurrentFlag` tinyint(1) NOT NULL default '1' COMMENT 'Is this the current convention? Defaults to true because we''ll generally be creating a new one.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `ConventionOrder` int(3) default NULL,
  PRIMARY KEY  (`idConventions`),
  UNIQUE KEY `iConventionName` (`ConventionName`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 COMMENT='Identifies each convention and provides the start and end da';

CREATE TABLE `Distributions` (
  `idDistributions` int(11) NOT NULL auto_increment,
  `idOrderLines` int(11) NOT NULL,
  `idFinancialTransactions` int(11) NOT NULL,
  `DistributionDate` datetime NOT NULL,
  `DistributionAmt` decimal(8,2) NOT NULL,
  `DistributionStatus` varchar(20) NOT NULL default 'new',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idDistributions`),
  KEY `idDistributionOrderLine` (`idOrderLines`),
  KEY `idDistributionFinancial` (`idFinancialTransactions`),
  KEY `idDistributionStatus` (`DistributionStatus`)
) ENGINE=MyISAM AUTO_INCREMENT=1117 DEFAULT CHARSET=latin1 COMMENT='Application of a financial transaction to an order line.';

CREATE TABLE `EmailLog` (
  `idEmailLog` int(11) NOT NULL auto_increment,
  `EmailSubject` varchar(100) NOT NULL default '',
  `Sender` varchar(100) NOT NULL default '',
  `EmailContents` blob NOT NULL,
  `SentDate` datetime default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idEmailLog`),
  UNIQUE KEY `RefCode` (`EmailSubject`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Log of e-mails sent by the system. Could add types as well. ';

CREATE TABLE `EmailLogAddressees` (
  `idEmailLogAddressees` int(11) NOT NULL auto_increment,
  `idEmailLog` int(11) NOT NULL default '0',
  `BounceFlag` tinyint(1) NOT NULL default '0',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idEmailLogAddressees`),
  UNIQUE KEY `RefCode` (`idEmailLog`),
  KEY `fkAddressEmailLog` (`idEmailLog`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Who was sent the email listed in the log. Status is to track';

CREATE TABLE `FinancialTransactions` (
  `idFinancialTransactions` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `TransactionDate` date NOT NULL,
  `TransactionType` varchar(20) NOT NULL COMMENT 'Payment, Refund, Debit Memo, Credit Memo',
  `TransactionAmt` decimal(8,2) NOT NULL,
  `TransactionStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will be COMPLETE but can be VOID, NSF, etc.',
  `TransactionComment` varchar(4000) default NULL,
  `TransactionSource` varchar(20) NOT NULL default 'other',
  `TransactionId` varchar(45) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idFinancialTransactions`),
  KEY `fkTransPerson` (`idPersons`),
  KEY `fkTransStatus` (`TransactionStatus`),
  KEY `fkTransType` (`TransactionType`),
  KEY `fkTransactionSource` (`TransactionSource`)
) ENGINE=MyISAM AUTO_INCREMENT=940 DEFAULT CHARSET=latin1 COMMENT='Payments or refunds';

CREATE TABLE `Guests` (
  `idGuests` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `GuestType` varchar(20) NOT NULL default 'Guest of Honor',
  `QualifiesAsPastGuestFlag` tinyint(1) default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idGuests`)
) ENGINE=MyISAM AUTO_INCREMENT=775 DEFAULT CHARSET=latin1 COMMENT='Guests of Honor for a convention';

CREATE TABLE `ItemLocationRankings` (
  `idItemLocRankings` int(11) NOT NULL auto_increment,
  `idLocations` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `Ranking` varchar(20) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idItemLocRankings`),
  UNIQUE KEY `idLocations` (`idLocations`,`idItems`),
  KEY `fkRankingLocations` (`idLocations`),
  KEY `fkRankingItemsL` (`idItems`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='Ranking of items to Locations';

CREATE TABLE `ItemRankings` (
  `idRankings` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `UserInterestRanking` varchar(20) default NULL,
  `ModeratorFlag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `idPersons` int(11) default NULL,
  `AttendFlag` int(11) default NULL,
  PRIMARY KEY  (`idRankings`),
  UNIQUE KEY `idUsers` (`idUsers`,`idItems`),
  KEY `fkRankingUser` (`idUsers`),
  KEY `fkRankingItems` (`idItems`),
  KEY `fkRankingPersInterest` (`UserInterestRanking`),
  KEY `iItemRankingsTemp` (`idPersons`,`idItems`)
) ENGINE=MyISAM AUTO_INCREMENT=27478 DEFAULT CHARSET=latin1 COMMENT='Ranking done by profiles about items, usually in the program';

CREATE TABLE `ItemTimePeriodRankings` (
  `idItemTimeRank` int(11) NOT NULL auto_increment,
  `idTimePeriods` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `Ranking` varchar(20) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idItemTimeRank`),
  UNIQUE KEY `idTimePeriods` (`idTimePeriods`,`idItems`),
  KEY `fkRankingTimePeriods` (`idTimePeriods`),
  KEY `fkRankingItemsTP` (`idItems`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Ranking of items to time periods';

CREATE TABLE `Locations` (
  `idLocations` int(11) NOT NULL auto_increment,
  `parentidLocations` int(11) default NULL,
  `LocationName` varchar(45) NOT NULL default '',
  `LocationType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '0',
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `StartDate` date default NULL,
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `GridName` varchar(45) default NULL,
  `ShowOnGridFlag` tinyint(1) default NULL,
  `GridOrderNo` smallint(6) default NULL,
  PRIMARY KEY  (`idLocations`),
  UNIQUE KEY `LocationName` (`LocationName`),
  KEY `LocationParent` (`parentidLocations`),
  KEY `fkLocationParent` (`parentidLocations`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`),
  KEY `fkLocationType` (`LocationType`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1 COMMENT='Where will the items of the convention be held?';

CREATE TABLE `Log` (
  `idLog` int(11) NOT NULL auto_increment,
  `LogEntry` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLog`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='General table to log information, nothing fancy yet. Could a';

CREATE TABLE `OldRegMigration` (
  `OldKey` int(11) default NULL,
  `DessertTickets` int(3) default NULL,
  `MembType` varchar(40) default NULL,
  `ConNo` int(3) default NULL,
  KEY `IOldReg` (`OldKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `OrderLines` (
  `idOrderLines` int(11) NOT NULL auto_increment,
  `idOrders` int(11) NOT NULL,
  `idCatalog` int(11) NOT NULL,
  `idPersons` int(11) default NULL COMMENT 'Line applies to person',
  `LineStatus` varchar(20) NOT NULL default 'new',
  `LineAmt` decimal(8,2) default NULL,
  `LineComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PersonAge` int(2) default NULL,
  PRIMARY KEY  (`idOrderLines`),
  KEY `idLineOrder` (`idOrders`),
  KEY `idLineCatalog` (`idCatalog`),
  KEY `idLinePerson` (`idPersons`),
  KEY `idLineStatus` (`LineStatus`)
) ENGINE=MyISAM AUTO_INCREMENT=12969 DEFAULT CHARSET=latin1;

CREATE TABLE `Orders` (
  `idOrders` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'Person making order',
  `OrderDate` date NOT NULL,
  `OrderMethodCode` varchar(20) default NULL COMMENT 'How did this order come in? Paypal, paper, etc.',
  `SourceTransactionNo` varchar(45) default NULL COMMENT 'Transaction identifier in the source system (for example, paypal identifier)',
  `OrderStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will get to COMPLETE quickly',
  `OrderComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idOrders`),
  KEY `fkOrderPerson` (`idPersons`),
  KEY `fkOrderMethod` (`OrderMethodCode`),
  KEY `fkOrderStatus` (`OrderStatus`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1 COMMENT='Somebody buys something or gives us something';

CREATE TABLE `PasswordReset` (
  `idPasswordReset` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `MailKey` varchar(45) default NULL,
  `CreateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UseTime` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateIP` varchar(15) default NULL,
  `UseIP` varchar(15) default NULL,
  PRIMARY KEY  (`idPasswordReset`)
) ENGINE=MyISAM AUTO_INCREMENT=264 DEFAULT CHARSET=latin1;

CREATE TABLE `PeopleConnections` (
  `idPeopleConnections` int(11) NOT NULL auto_increment,
  `idPersonsFrom` int(11) NOT NULL default '0',
  `idPersonsTo` int(11) NOT NULL default '0',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date default NULL,
  `ConnectionType` varchar(20) default NULL,
  `ConnectionComment` text,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idPeopleConnections`),
  KEY `fkConnectionFrom` (`idPersonsFrom`),
  KEY `fkConnectionTo` (`idPersonsTo`),
  KEY `fkConnectionType` (`ConnectionType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='People-people relationship related to the convention. Exampl';

CREATE TABLE `PersonalPreferences` (
  `idPersonalPreferences` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `PreferenceType` varchar(20) NOT NULL,
  `PreferenceValue` tinyint(1) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PreferenceText` varchar(4000) default NULL COMMENT 'web address, name, or other text as defined for the type',
  `idConventions` int(11) default NULL COMMENT 'If not null, applies only to this convention',
  `start_date` date default NULL,
  `end_date` date default NULL,
  PRIMARY KEY  (`idPersonalPreferences`),
  KEY `fkPreferencePerson` (`idPersons`),
  KEY `fkPreferenceType` (`PreferenceType`),
  KEY `fkPreferenceConvention` (`idConventions`)
) ENGINE=MyISAM AUTO_INCREMENT=487 DEFAULT CHARSET=latin1 COMMENT='Preferences attached to a particular person.';

CREATE TABLE `PersonalScheduleItems` (
  `idScheduleItems` int(11) NOT NULL auto_increment,
  `idSchedules` int(11) NOT NULL,
  `PersonalItemText` text,
  `ItemLocation` varchar(45) default NULL,
  `ItemStartTime` datetime default NULL,
  `ItemEndTime` datetime default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScheduleItems`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Items added by a person for their own Personal Schedule';

CREATE TABLE `PersonalSchedules` (
  `idSchedules` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `idConventions` int(11) NOT NULL,
  `idScheduleStatus` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSchedules`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Personal Schedules';

CREATE TABLE `PersonAvailability` (
  `idPersonAvailability` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'FK to the person',
  `idConventions` int(11) NOT NULL COMMENT 'FK to the convention',
  `ArrivalDate` datetime default NULL,
  `DepartureDate` datetime default NULL COMMENT 'Earliest time available each day',
  `Exclude830Flag` tinyint(1) default NULL COMMENT 'Latest time available each day',
  `Exclude1pmFlag` tinyint(1) default NULL,
  `Exclude4pmFlag` tinyint(1) default NULL,
  `Exclude7pmFlag` tinyint(1) default NULL,
  `Exclude9pmFlag` tinyint(1) default NULL,
  `Exclude1030Flag` tinyint(1) default NULL,
  `ExcludeMidnightFlag` tinyint(1) default NULL,
  `ExcludeGatheringFlag` tinyint(1) default NULL,
  `ExcludeWorkshopFlag` tinyint(1) default NULL,
  `ExcludeAuctionflag` tinyint(1) default NULL,
  `ExcludeSignoutFlag` tinyint(1) default NULL,
  `ExcludeGOHFlag` tinyint(1) default NULL,
  `OtherConstraintText` varchar(4000) default NULL COMMENT 'Description of other availability constraints, to be checked by hand',
  `Exclude10amFlag` tinyint(1) default NULL,
  `Exclude230Flag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateUser` varchar(45) NOT NULL,
  `MaxPanels` int(11) default NULL,
  PRIMARY KEY  (`idPersonAvailability`),
  KEY `fkAvailabilityPersons` (`idPersons`),
  KEY `fkAvailiabilityConvention` (`idConventions`)
) ENGINE=MyISAM AUTO_INCREMENT=499 DEFAULT CHARSET=latin1;

CREATE TABLE `PersonMatched` (
  `idPersons` int(11) default NULL,
  `MatchedFlag` tinyint(1) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `Persons` (
  `idPersons` int(11) NOT NULL auto_increment,
  `PersonEmail` varchar(200) default NULL,
  `LastName` varchar(45) NOT NULL default '',
  `FirstName` varchar(45) default NULL,
  `MiddleName` varchar(45) default NULL,
  `Salutation` varchar(20) default NULL,
  `Line1Address` varchar(200) default NULL,
  `Line2Address` varchar(200) default NULL,
  `City` varchar(45) default NULL,
  `StateProvince` varchar(45) default NULL,
  `ZipPostalCode` varchar(45) default NULL,
  `Country` varchar(45) default NULL,
  `Title` varchar(45) default NULL,
  `Company` varchar(45) default NULL,
  `ActiveFlag` tinyint(1) default NULL,
  `LongBio` text,
  `ShortBio` text,
  `PublicName` varchar(100) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `OldKey` bigint(20) default NULL,
  `suffix` varchar(10) default NULL,
  `PersonUrl` varchar(200) default NULL,
  `AlternateProgramName` varchar(200) default NULL,
  `PublicAnonymousFlag` tinyint(1) default NULL,
  `ReportAlphaSort` varchar(45) default NULL,
  `BadgeName` varchar(45) default NULL,
  `BadAddressFlag` tinyint(1) default NULL,
  PRIMARY KEY  (`idPersons`),
  KEY `fkSalutation` (`Salutation`)
) ENGINE=MyISAM AUTO_INCREMENT=3363 DEFAULT CHARSET=latin1 COMMENT='People involved in the convention. May suggest items or be a';

CREATE TABLE `PersonTimeExclusions` (
  `idPersonExclusion` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'FK to the person',
  `idTimePeriods` int(11) NOT NULL COMMENT 'FK to the time period',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idPersonExclusion`),
  KEY `fkExclusionPersons` (`idPersons`),
  KEY `fkExclusionTimePeriod` (`idTimePeriods`)
) ENGINE=MyISAM AUTO_INCREMENT=14188 DEFAULT CHARSET=latin1;

CREATE TABLE `ProgramItems` (
  `idItems` int(11) NOT NULL auto_increment,
  `ItemName` varchar(200) default NULL,
  `ItemDesc` text,
  `idConventions` int(11) default NULL,
  `RecurringFlag` tinyint(1) NOT NULL default '0',
  `idItemsParent` int(11) default NULL,
  `idTrackPrimary` int(11) default NULL,
  `idTrackSecondary` int(11) default NULL,
  `idFormats` int(11) default NULL,
  `idSlots` int(11) default NULL,
  `PublicDesc` text,
  `EquipmentDesc` text,
  `SuggestedPanelists` text,
  `SuggestedModerators` text,
  `AdminNotes` text,
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `ItemStatus` varchar(20) NOT NULL default 'proposed',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `PersonInfo` text,
  `idUserProposer` int(11) default NULL,
  `ProgramItemType` varchar(20) NOT NULL default 'unassigned',
  `GeneralSignupFlag` tinyint(1) default NULL,
  `ModeratorSignupFlag` tinyint(1) default NULL,
  `GridDesc` varchar(45) default NULL,
  `GridNumber` int(11) default NULL,
  PRIMARY KEY  (`idItems`),
  KEY `fkItemsParent` (`idItemsParent`),
  KEY `fkItemsSlot` (`idSlots`),
  KEY `fkItemsConventions` (`idConventions`),
  KEY `fkItemsStatus` (`ItemStatus`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`),
  KEY `fkItemsProposer` (`idUserProposer`),
  KEY `fkItemType` (`ProgramItemType`),
  KEY `fkItemTrackP` (`idTrackPrimary`),
  KEY `fkItemTrackS` (`idTrackSecondary`),
  KEY `fkItemFormat` (`idFormats`)
) ENGINE=MyISAM AUTO_INCREMENT=973 DEFAULT CHARSET=latin1 COMMENT='A single item held at the convention. Panel, party, etc. Par';

CREATE TABLE `ProgramScheduleItems` (
  `idProgramScheduleItems` int(11) NOT NULL auto_increment,
  `idSchedules` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idProgramScheduleItems`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1 COMMENT='Program Items included by a person on their Personal Schedul';

CREATE TABLE `RefAlternateDataTypes` (
  `idDataTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idDataTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAlternateScopes` (
  `idScopes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScopes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAssignmentStatus` (
  `idAssignmentStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idAssignmentStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAssignmentTypes` (
  `idAssignmentTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idAssignmentTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAuthorityCodes` (
  `idCodes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `ActiveFlag` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idCodes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefCapacityUnits` (
  `idUnits` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idUnits`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefConnectionTypes` (
  `idConnectionTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idConnectionTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `RefConventionLimitTypes` (
  `idLimitTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idLimitTypes`),
  UNIQUE KEY `RefLimitTypeUK` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `RefDistributionStatus` (
  `idDistributionStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idDistributionStatus`),
  UNIQUE KEY `RefDistributionStatusUK` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `RefItemStatus` (
  `idItemStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idItemStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLineStatus` (
  `idLineStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idLineStatus`),
  UNIQUE KEY `RefLineStatusUK` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLocationTypes` (
  `idLocationTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLocationTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLocTimeRankings` (
  `idLocTimeRankings` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLocTimeRankings`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefOrderMethod` (
  `idRef` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRef`),
  UNIQUE KEY `RefOrderMethodsUK` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `RefOrderStatus` (
  `idOrderStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idOrderStatus`),
  UNIQUE KEY `RefOrderStatusUK` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefPreferenceTypes` (
  `idPreferenceTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` varchar(2000) NOT NULL,
  `VisibleToUserFlag` tinyint(1) NOT NULL default '1' COMMENT 'Can the person see this preference, or is it hidden for admin use?',
  `EditByUserFlag` tinyint(1) NOT NULL default '1' COMMENT 'Can this person edit this preference, or is it admin only?',
  `DisplayPrompt` varchar(45) NOT NULL COMMENT 'Prompt to show for this preference in the UI',
  `TextFieldFlag` tinyint(1) NOT NULL default '0' COMMENT 'Display a text field for entry with this preference?',
  `OrderBy` int(11) default NULL COMMENT 'Order preference information by this, then by prompt',
  `ConventionFlag` tinyint(1) NOT NULL default '0' COMMENT 'If true, preference only applies to a single convention and idConventions should be set.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `TextFieldPrompt` varchar(45) default NULL,
  PRIMARY KEY  (`idPreferenceTypes`),
  UNIQUE KEY `RefPreferenceTypes` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Types of Personal Preferences';

CREATE TABLE `RefProgramFormats` (
  `idProgramFormats` int(11) NOT NULL auto_increment,
  `Title` varchar(80) NOT NULL default '',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idProgramFormats`),
  UNIQUE KEY `RefTitle` (`Title`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefProgramItemTypes` (
  `idProgramItemTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `ActiveFlag` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idProgramItemTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `RefProgramTracks` (
  `idProgramTracks` int(11) NOT NULL auto_increment,
  `Name` varchar(80) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `PanelistDescription` text,
  PRIMARY KEY  (`idProgramTracks`),
  UNIQUE KEY `TrackName` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `RefRankingTypes` (
  `idRankingTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idRankingTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefRoleTypes` (
  `idRoleTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idRoleTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

CREATE TABLE `RefSalutations` (
  `idSalutations` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSalutations`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefScheduleStatus` (
  `idScheduleStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` varchar(2000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScheduleStatus`),
  UNIQUE KEY `RefScheduleStatus` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='Types of Personal Preferences';

CREATE TABLE `RefTransactionSources` (
  `idTransactionSource` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idTransactionSource`),
  UNIQUE KEY `RefTransSource` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefTransactionStatus` (
  `idTransactionStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idTransactionStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefTransactionTypes` (
  `idTransactionTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idTransactionTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `RoleType` varchar(20) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRoles`),
  KEY `fkRoleUsers` (`idUsers`),
  KEY `fkRoleType` (`RoleType`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COMMENT='What is the owner of this profile allowed to do?';

CREATE TABLE `Slots` (
  `idSlots` int(11) NOT NULL auto_increment,
  `idLocations` int(11) NOT NULL default '0',
  `idTimePeriods` int(11) NOT NULL default '0',
  `SlotDesc` text,
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSlots`),
  KEY `fkLocations` (`idLocations`),
  KEY `fkSlotPeriod` (`idTimePeriods`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`)
) ENGINE=MyISAM AUTO_INCREMENT=2856 DEFAULT CHARSET=latin1 COMMENT='Combination of convention, location, and time period. Did no';

CREATE TABLE `SpecialBadges` (
  `idSpecialBadges` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `SpecialType` varchar(20) NOT NULL default 'Ask Me',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idSpecialBadges`)
) ENGINE=MyISAM AUTO_INCREMENT=801 DEFAULT CHARSET=latin1 COMMENT='Special Badges for a convention';

CREATE TABLE `TimePeriods` (
  `idTimePeriods` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL default '0',
  `StartDateTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `EndDateTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `PeriodComment` text,
  `StandardTimeFlag` tinyint(1) NOT NULL default '0' COMMENT 'Is this a normal time that goes in the grid?',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `WarnOnSecondUseFlag` tinyint(1) NOT NULL default '0' COMMENT 'Warn users when trying to use this time period more than once?',
  `PublicStartTime` datetime default NULL,
  `PublicEndTime` datetime default NULL,
  PRIMARY KEY  (`idTimePeriods`),
  KEY `fkLimitConvention` (`idConventions`),
  KEY `fkLimitUnits` (`StandardTimeFlag`),
  KEY `fkLimitType` (`StartDateTime`)
) ENGINE=MyISAM AUTO_INCREMENT=494 DEFAULT CHARSET=latin1 COMMENT='For a particular convention, when do things happen. There ar';

CREATE TABLE `UserAuthority` (
  `idUserAuthority` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `StartTimestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `EndTimestamp` datetime default NULL,
  `AuthorityCode` varchar(20) NOT NULL default 'owner',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idUserAuthority`),
  KEY `fkAuthorityProfile` (`idUsers`),
  KEY `fkAuthorityPersons` (`idPersons`)
) ENGINE=MyISAM AUTO_INCREMENT=3817 DEFAULT CHARSET=latin1 COMMENT='This user has row-level authority for this person record, fo';

CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL auto_increment,
  `ProfileEmail` varchar(100) NOT NULL default '',
  `ProfilePassword` varchar(45) NOT NULL default '',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `ForceChangeFlag` tinyint(1) default NULL,
  PRIMARY KEY  (`idUsers`),
  UNIQUE KEY `iProfilesEmail` (`ProfileEmail`)
) ENGINE=MyISAM AUTO_INCREMENT=2845 DEFAULT CHARSET=latin1 COMMENT='Account on the application. ProfileEmail address is the user';


