CREATE TABLE `AlternateNames` (
  `idAlternateNames` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL default '0',
  `StartDate` date NOT NULL default '0000-00-00' COMMENT 'Start date for using this alternate name',
  `EndDate` date default NULL COMMENT 'End date for using this alternate data',
  `AlternateScope` varchar(20) NOT NULL default '' COMMENT 'Public, current convention, all conventions, secondary email, etc.',
  `SeasonalRepeatsFlag` tinyint(1) NOT NULL default '0' COMMENT 'If true, dates repeat each year, for example for a seasonal address',
  `AlternateDataType` varchar(20) NOT NULL default '' COMMENT 'Name, email, address',
  `FullName` varchar(200) default NULL,
  `Line1Address` varchar(200) default NULL,
  `Line2Address` varchar(200) default NULL,
  `City` varchar(45) default NULL,
  `StateProvince` varchar(45) default NULL,
  `ZipPostalCode` varchar(45) default NULL,
  `Country` varchar(45) default NULL,
  `PersonEmail` varchar(200) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `PhoneType` varchar(20) default NULL,
  `PhoneNumber` varchar(20) default NULL,
  `URLType` varchar(20) default NULL,
  `URL` varchar(200) default NULL,
  `PublishDirectoryFlag` tinyint(1) default '1',
  PRIMARY KEY  (`idAlternateNames`),
  KEY `fkAltNamesPersons` (`idPersons`),
  KEY `fkAltNamesScope` (`AlternateScope`),
  KEY `fkAltNamesDataType` (`AlternateDataType`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=latin1 COMMENT='The idea here is that the primary name, email, and address a';

CREATE TABLE `AppParameters` (
  `idParameters` int(11) NOT NULL auto_increment,
  `Environment` varchar(20) NOT NULL default '',
  `Build` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idParameters`),
  UNIQUE KEY `RefCode` (`Environment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Single Row table containing basic useful information, such a';

CREATE TABLE `Assignments` (
  `idAssignments` int(11) NOT NULL auto_increment,
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL,
  PRIMARY KEY  (`idAssignments`),
  KEY `fkAssignmentItem` (`idItems`),
  KEY `fkAssignmentPersons` (`idPersons`),
  KEY `fkAssignmentType` (`AssignmentType`),
  KEY `iAssignmentsTemp` (`idPersons`,`idItems`),
  KEY `fkAssignmentsStatus` (`AssignmentStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=9819 DEFAULT CHARSET=latin1 COMMENT='Person will be doing something for the item. For example, pa';

CREATE TABLE `AssignmentsBackup` (
  `idAssignments` int(11) NOT NULL default '0',
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `AssignmentSuggested` (
  `idAssignments` int(11) NOT NULL default '0',
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL,
  PRIMARY KEY  (`idAssignments`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `AssignmentSuggestedBackup` (
  `idAssignments` int(11) NOT NULL default '0',
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `AutoAssignmentParameters` (
  `idParam` int(11) NOT NULL auto_increment,
  `Name` varchar(45) NOT NULL default '',
  `Value` varchar(255) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idParam`),
  UNIQUE KEY `RefCode` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE `BadgeSorts` (
  `idBadgeSorts` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `BadgeOrder` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idBadgeSorts`),
  KEY `fkBadgeSortsPersons` (`idPersons`),
  KEY `fkBadgeSortsConventions` (`idConventions`)
) ENGINE=InnoDB AUTO_INCREMENT=40553 DEFAULT CHARSET=latin1 COMMENT='Persist the badge sort. Refresh by badge printing, used for ';

CREATE TABLE `BadgesToPrint` (
  `idPersons` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `CatalogItems` (
  `idCatalog` int(11) NOT NULL auto_increment,
  `idConventions` int(11) default NULL COMMENT 'Optional foreign key. Some sales items may not be for a particular convention.',
  `SaleItemName` varchar(45) NOT NULL COMMENT 'Name, such as "Adult Membership"',
  `SaleItemDesc` varchar(4000) default NULL COMMENT 'Description of what this sales item is',
  `UnitPrice` decimal(6,2) NOT NULL COMMENT 'What it costs',
  `StartDate` date NOT NULL COMMENT 'First sale date',
  `EndDate` date default NULL COMMENT 'Last sale date',
  `CapacityAmt` int(11) default NULL COMMENT 'Used for single capacity limits, like child care memberships or dealers tables',
  `CapacityUnitCode` varchar(20) default NULL,
  `ConventionMembershipFlag` tinyint(1) NOT NULL default '0' COMMENT 'Is this a convention membership?',
  `ConventionLimitType` varchar(20) default NULL COMMENT 'Does this count towards an overall convention limit? If so, which one.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PublicFlag` tinyint(1) default NULL,
  `OrderBy` int(3) default NULL,
  PRIMARY KEY  (`idCatalog`),
  KEY `fkCatalogConvention` (`idConventions`),
  KEY `fkCatalogCapacityUnits` (`CapacityUnitCode`),
  KEY `fkCatalogConventionLimit` (`ConventionLimitType`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1 COMMENT='What we''re selling, most tied to a particular convention';

CREATE TABLE `ChangeHistory` (
  `idChangeHistory` int(11) NOT NULL auto_increment,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` int(11) NOT NULL,
  `TableName` varchar(45) NOT NULL,
  `FieldName` varchar(45) NOT NULL,
  `idRecord` int(11) NOT NULL,
  `OldValue` text,
  `NewValue` text,
  PRIMARY KEY  (`idChangeHistory`)
) ENGINE=InnoDB AUTO_INCREMENT=98716 DEFAULT CHARSET=latin1;

CREATE TABLE `ConventionLimits` (
  `idConventionLimits` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `LimitType` varchar(20) NOT NULL,
  `LimitAmt` int(11) NOT NULL,
  `LimitComment` varchar(4000) default NULL,
  `LimitUnits` varchar(20) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idConventionLimits`),
  KEY `fkLimitConvention` (`idConventions`),
  KEY `fkLimitUnits` (`LimitUnits`),
  KEY `fkLimitType` (`LimitType`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Capacity limits for a convention. Enforced by programmatic c';

CREATE TABLE `Conventions` (
  `idConventions` int(11) NOT NULL auto_increment,
  `ConventionName` varchar(45) NOT NULL default '',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date NOT NULL default '0000-00-00',
  `CurrentFlag` tinyint(1) NOT NULL default '1' COMMENT 'Is this the current convention? Defaults to true because we''ll generally be creating a new one.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `ConventionOrder` int(3) default NULL,
  PRIMARY KEY  (`idConventions`),
  UNIQUE KEY `iConventionName` (`ConventionName`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1 COMMENT='Identifies each convention and provides the start and end da';

CREATE TABLE `DealerApplications` (
  `idDealerApplications` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `idConventions` int(11) NOT NULL,
  `NumTables` tinyint(1) NOT NULL,
  `Background` text,
  `ThirdTableReason` text,
  `CompanyPhone` varchar(45) default NULL,
  `ApplicationStatus` varchar(20) default NULL,
  `AdminComment` varchar(200) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idDealerApplications`),
  KEY `fkPersons` (`idPersons`),
  KEY `fkConventions` (`idConventions`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

CREATE TABLE `Distributions` (
  `idDistributions` int(11) NOT NULL auto_increment,
  `idFinancialTransactions` int(11) NOT NULL,
  `idOrderLines` int(11) NOT NULL,
  `DistributionDate` datetime NOT NULL,
  `DistributionAmt` decimal(8,2) NOT NULL,
  `DistributionStatus` varchar(20) NOT NULL default 'new',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idDistributions`),
  KEY `idDistributionOrderLine` (`idOrderLines`),
  KEY `idDistributionFinancial` (`idFinancialTransactions`),
  KEY `idDistributionStatus` (`DistributionStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=4166 DEFAULT CHARSET=latin1 COMMENT='Application of a financial transaction to an order line.';

CREATE TABLE `EmailLog` (
  `idEmailLog` int(11) NOT NULL auto_increment,
  `EmailSubject` varchar(100) NOT NULL default '',
  `Sender` varchar(100) NOT NULL default '',
  `EmailContents` blob NOT NULL,
  `SentDate` datetime default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idEmailLog`),
  UNIQUE KEY `RefCode` (`EmailSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Log of e-mails sent by the system. Could add types as well. ';

CREATE TABLE `EmailLogAddressees` (
  `idEmailLogAddressees` int(11) NOT NULL auto_increment,
  `idEmailLog` int(11) NOT NULL default '0',
  `BounceFlag` tinyint(1) NOT NULL default '0',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idEmailLogAddressees`),
  UNIQUE KEY `RefCode` (`idEmailLog`),
  KEY `fkAddressEmailLog` (`idEmailLog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Who was sent the email listed in the log. Status is to track';

CREATE TABLE `FinancialTransactions` (
  `idFinancialTransactions` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `TransactionDate` date NOT NULL,
  `TransactionType` varchar(20) NOT NULL COMMENT 'Payment, Refund, Debit Memo, Credit Memo',
  `TransactionAmt` decimal(8,2) NOT NULL,
  `TransactionStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will be COMPLETE but can be VOID, NSF, etc.',
  `TransactionComment` varchar(4000) default NULL,
  `TransactionSource` varchar(20) NOT NULL default 'other',
  `TransactionId` varchar(45) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idFinancialTransactions`),
  KEY `fkTransPerson` (`idPersons`),
  KEY `fkTransStatus` (`TransactionStatus`),
  KEY `fkTransType` (`TransactionType`),
  KEY `fkTransactionSource` (`TransactionSource`)
) ENGINE=InnoDB AUTO_INCREMENT=2589 DEFAULT CHARSET=latin1 COMMENT='Payments or refunds';

CREATE TABLE `FinancialTransactionsBackup` (
  `idFinancialTransactions` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL,
  `TransactionDate` date NOT NULL,
  `TransactionType` varchar(20) NOT NULL COMMENT 'Payment, Refund, Debit Memo, Credit Memo',
  `TransactionAmt` decimal(8,2) NOT NULL,
  `TransactionStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will be COMPLETE but can be VOID, NSF, etc.',
  `TransactionComment` varchar(4000) default NULL,
  `TransactionSource` varchar(20) NOT NULL default 'other',
  `TransactionId` varchar(45) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Guests` (
  `idGuests` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `GuestType` varchar(20) NOT NULL default 'Guest of Honor',
  `QualifiesAsPastGuestFlag` tinyint(1) default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idGuests`),
  KEY `fkGuestsPersons` (`idPersons`),
  KEY `fkGuestsConvention` (`idConventions`)
) ENGINE=InnoDB AUTO_INCREMENT=813 DEFAULT CHARSET=latin1 COMMENT='Guests of Honor for a convention';

CREATE TABLE `GuestsBackup` (
  `idGuests` int(11) NOT NULL default '0',
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `GuestType` varchar(20) NOT NULL default 'Guest of Honor',
  `QualifiesAsPastGuestFlag` tinyint(1) default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ItemLocationRankings` (
  `idItemLocRankings` int(11) NOT NULL auto_increment,
  `idLocations` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `Ranking` varchar(20) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idItemLocRankings`),
  UNIQUE KEY `idLocations` (`idLocations`,`idItems`),
  KEY `fkRankingLocations` (`idLocations`),
  KEY `fkRankingItemsL` (`idItems`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='Ranking of items to Locations';

CREATE TABLE `ItemRankings` (
  `idRankings` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `UserInterestRanking` varchar(20) default NULL,
  `ModeratorFlag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `idPersons` int(11) default NULL,
  `AttendFlag` int(11) default NULL,
  PRIMARY KEY  (`idRankings`),
  UNIQUE KEY `idUsers` (`idUsers`,`idItems`),
  KEY `fkRankingUser` (`idUsers`),
  KEY `fkRankingItems` (`idItems`),
  KEY `fkRankingPersInterest` (`UserInterestRanking`),
  KEY `iItemRankingsTemp` (`idPersons`,`idItems`)
) ENGINE=InnoDB AUTO_INCREMENT=68532 DEFAULT CHARSET=latin1 COMMENT='Ranking done by profiles about items, usually in the program';

CREATE TABLE `ItemRankingsBackup` (
  `idRankings` int(11) NOT NULL default '0',
  `idUsers` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `UserInterestRanking` varchar(20) default NULL,
  `ModeratorFlag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `idPersons` int(11) default NULL,
  `AttendFlag` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ItemTimePeriodRankings` (
  `idItemTimeRank` int(11) NOT NULL auto_increment,
  `idTimePeriods` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `RankingDate` date NOT NULL,
  `Ranking` varchar(20) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idItemTimeRank`),
  UNIQUE KEY `idTimePeriods` (`idTimePeriods`,`idItems`),
  KEY `fkRankingTimePeriods` (`idTimePeriods`),
  KEY `fkRankingItemsTP` (`idItems`),
  KEY `fkITPRRanking` (`Ranking`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Ranking of items to time periods';

CREATE TABLE `Locations` (
  `idLocations` int(11) NOT NULL auto_increment,
  `parentidLocations` int(11) default NULL,
  `LocationName` varchar(45) NOT NULL default '',
  `LocationType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '0',
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `StartDate` date default NULL,
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `GridName` varchar(45) default NULL,
  `ShowOnGridFlag` tinyint(1) default NULL,
  `GridOrderNo` smallint(6) default NULL,
  PRIMARY KEY  (`idLocations`),
  UNIQUE KEY `LocationName` (`LocationName`),
  KEY `LocationParent` (`parentidLocations`),
  KEY `fkLocationParent` (`parentidLocations`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`),
  KEY `fkLocationType` (`LocationType`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1 COMMENT='Where will the items of the convention be held?';

CREATE TABLE `Log` (
  `idLog` int(11) NOT NULL auto_increment,
  `LogEntry` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='General table to log information, nothing fancy yet. Could a';

CREATE TABLE `OldRegMigration` (
  `OldKey` int(11) default NULL,
  `DessertTickets` int(3) default NULL,
  `MembType` varchar(40) default NULL,
  `ConNo` int(3) default NULL,
  KEY `IOldReg` (`OldKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `OrderLines` (
  `idOrderLines` int(11) NOT NULL auto_increment,
  `idOrders` int(11) NOT NULL,
  `idCatalog` int(11) NOT NULL,
  `idPersons` int(11) default NULL COMMENT 'Line applies to person',
  `LineStatus` varchar(20) NOT NULL default 'new',
  `LineAmt` decimal(8,2) default NULL,
  `LineComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PersonAge` int(2) default NULL,
  PRIMARY KEY  (`idOrderLines`),
  KEY `idLineOrder` (`idOrders`),
  KEY `idLineCatalog` (`idCatalog`),
  KEY `idLinePerson` (`idPersons`),
  KEY `idLineStatus` (`LineStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=16701 DEFAULT CHARSET=latin1;

CREATE TABLE `OrderLinesBackup` (
  `idOrderLines` int(11) NOT NULL default '0',
  `idOrders` int(11) NOT NULL,
  `idCatalog` int(11) NOT NULL,
  `idPersons` int(11) default NULL COMMENT 'Line applies to person',
  `LineStatus` varchar(20) NOT NULL default 'new',
  `LineAmt` decimal(8,2) default NULL,
  `LineComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PersonAge` int(2) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Orders` (
  `idOrders` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'Person making order',
  `OrderDate` date NOT NULL,
  `OrderMethodCode` varchar(20) default NULL COMMENT 'How did this order come in? Paypal, paper, etc.',
  `SourceTransactionNo` varchar(45) default NULL COMMENT 'Transaction identifier in the source system (for example, paypal identifier)',
  `OrderStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will get to COMPLETE quickly',
  `OrderComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idOrders`),
  KEY `fkOrderPerson` (`idPersons`),
  KEY `fkOrderMethod` (`OrderMethodCode`),
  KEY `fkOrderStatus` (`OrderStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=2748 DEFAULT CHARSET=latin1 COMMENT='Somebody buys something or gives us something';

CREATE TABLE `OrdersBackup` (
  `idOrders` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL COMMENT 'Person making order',
  `OrderDate` date NOT NULL,
  `OrderMethodCode` varchar(20) default NULL COMMENT 'How did this order come in? Paypal, paper, etc.',
  `SourceTransactionNo` varchar(45) default NULL COMMENT 'Transaction identifier in the source system (for example, paypal identifier)',
  `OrderStatus` varchar(20) NOT NULL default 'new' COMMENT 'Usually will get to COMPLETE quickly',
  `OrderComment` varchar(4000) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `OrphanAssignments` (
  `idAssignments` int(11) NOT NULL default '0',
  `idItems` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `AssignmentComment` text,
  `AssignmentType` varchar(20) NOT NULL default '',
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `AssignmentStatus` varchar(20) NOT NULL,
  `AssigneeAcceptanceFlag` tinyint(1) default NULL,
  `AssigneeComment` varchar(4000) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `OrphanDistributions` (
  `idDistributions` int(11) NOT NULL default '0',
  `idOrderLines` int(11) NOT NULL,
  `idFinancialTransactions` int(11) NOT NULL,
  `DistributionDate` datetime NOT NULL,
  `DistributionAmt` decimal(8,2) NOT NULL,
  `DistributionStatus` varchar(20) NOT NULL default 'new',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `PasswordReset` (
  `idPasswordReset` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `MailKey` varchar(45) default NULL,
  `CreateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UseTime` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateIP` varchar(15) default NULL,
  `UseIP` varchar(15) default NULL,
  PRIMARY KEY  (`idPasswordReset`)
) ENGINE=InnoDB AUTO_INCREMENT=649 DEFAULT CHARSET=latin1;

CREATE TABLE `PeopleConnections` (
  `idPeopleConnections` int(11) NOT NULL auto_increment,
  `idPersonsFrom` int(11) NOT NULL default '0',
  `idPersonsTo` int(11) NOT NULL default '0',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date default NULL,
  `ConnectionType` varchar(20) default NULL,
  `ConnectionComment` text,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idPeopleConnections`),
  KEY `fkConnectionFrom` (`idPersonsFrom`),
  KEY `fkConnectionTo` (`idPersonsTo`),
  KEY `fkConnectionType` (`ConnectionType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='People-people relationship related to the convention. Exampl';

CREATE TABLE `PersonalPreferences` (
  `idPersonalPreferences` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `PreferenceType` int(11) NOT NULL,
  `PreferenceValue` tinyint(1) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PreferenceText` varchar(4000) default NULL COMMENT 'web address, name, or other text as defined for the type',
  `idConventions` int(11) default NULL COMMENT 'If not null, applies only to this convention',
  `start_date` date default NULL,
  `end_date` date default NULL,
  PRIMARY KEY  (`idPersonalPreferences`),
  KEY `fkPreferencePerson` (`idPersons`),
  KEY `fkPreferenceType` (`PreferenceType`),
  KEY `fkPreferenceConvention` (`idConventions`)
) ENGINE=InnoDB AUTO_INCREMENT=1876 DEFAULT CHARSET=latin1 COMMENT='Preferences attached to a particular person.';

CREATE TABLE `PersonalPreferencesBackup` (
  `idPersonalPreferences` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL,
  `PreferenceType` varchar(20) NOT NULL,
  `PreferenceValue` tinyint(1) NOT NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `PreferenceText` varchar(4000) default NULL COMMENT 'web address, name, or other text as defined for the type',
  `idConventions` int(11) default NULL COMMENT 'If not null, applies only to this convention',
  `start_date` date default NULL,
  `end_date` date default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `PersonalScheduleItems` (
  `idScheduleItems` int(11) NOT NULL auto_increment,
  `idSchedules` int(11) NOT NULL,
  `PersonalItemText` text,
  `ItemLocation` varchar(45) default NULL,
  `ItemStartTime` datetime default NULL,
  `ItemEndTime` datetime default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScheduleItems`),
  KEY `fkPersSchedItemSchedule` (`idSchedules`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Items added by a person for their own Personal Schedule';

CREATE TABLE `PersonalSchedules` (
  `idSchedules` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL,
  `idConventions` int(11) NOT NULL,
  `idScheduleStatus` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSchedules`),
  KEY `fkPersonalSchedulesStatus` (`idScheduleStatus`),
  KEY `fkPersonalSchedulesConventions` (`idConventions`),
  KEY `fkPersonalSchedulesPersons` (`idPersons`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Personal Schedules';

CREATE TABLE `PersonAvailability` (
  `idPersonAvailability` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'FK to the person',
  `idConventions` int(11) NOT NULL COMMENT 'FK to the convention',
  `ArrivalDate` datetime default NULL,
  `DepartureDate` datetime default NULL COMMENT 'Earliest time available each day',
  `Exclude830Flag` tinyint(1) default NULL COMMENT 'Latest time available each day',
  `Exclude1pmFlag` tinyint(1) default NULL,
  `Exclude4pmFlag` tinyint(1) default NULL,
  `Exclude7pmFlag` tinyint(1) default NULL,
  `Exclude9pmFlag` tinyint(1) default NULL,
  `Exclude1030Flag` tinyint(1) default NULL,
  `ExcludeMidnightFlag` tinyint(1) default NULL,
  `ExcludeGatheringFlag` tinyint(1) default NULL,
  `ExcludeWorkshopFlag` tinyint(1) default NULL,
  `ExcludeAuctionflag` tinyint(1) default NULL,
  `ExcludeSignoutFlag` tinyint(1) default NULL,
  `ExcludeGOHFlag` tinyint(1) default NULL,
  `OtherConstraintText` varchar(4000) default NULL COMMENT 'Description of other availability constraints, to be checked by hand',
  `Exclude10amFlag` tinyint(1) default NULL,
  `Exclude230Flag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateUser` varchar(45) NOT NULL,
  `MaxPanels` int(11) default NULL,
  PRIMARY KEY  (`idPersonAvailability`),
  KEY `fkAvailabilityPersons` (`idPersons`),
  KEY `fkAvailiabilityConvention` (`idConventions`)
) ENGINE=InnoDB AUTO_INCREMENT=1054 DEFAULT CHARSET=latin1;

CREATE TABLE `PersonAvailabilityBackup` (
  `idPersonAvailability` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL COMMENT 'FK to the person',
  `idConventions` int(11) NOT NULL COMMENT 'FK to the convention',
  `ArrivalDate` datetime default NULL,
  `DepartureDate` datetime default NULL COMMENT 'Earliest time available each day',
  `Exclude830Flag` tinyint(1) default NULL COMMENT 'Latest time available each day',
  `Exclude1pmFlag` tinyint(1) default NULL,
  `Exclude4pmFlag` tinyint(1) default NULL,
  `Exclude7pmFlag` tinyint(1) default NULL,
  `Exclude9pmFlag` tinyint(1) default NULL,
  `Exclude1030Flag` tinyint(1) default NULL,
  `ExcludeMidnightFlag` tinyint(1) default NULL,
  `ExcludeGatheringFlag` tinyint(1) default NULL,
  `ExcludeWorkshopFlag` tinyint(1) default NULL,
  `ExcludeAuctionflag` tinyint(1) default NULL,
  `ExcludeSignoutFlag` tinyint(1) default NULL,
  `ExcludeGOHFlag` tinyint(1) default NULL,
  `OtherConstraintText` varchar(4000) default NULL COMMENT 'Description of other availability constraints, to be checked by hand',
  `Exclude10amFlag` tinyint(1) default NULL,
  `Exclude230Flag` tinyint(1) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateUser` varchar(45) NOT NULL,
  `MaxPanels` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `PersonDupJFHWork` (
  `idGood` int(11) NOT NULL default '0',
  `idBad` int(11) NOT NULL default '0',
  PRIMARY KEY  (`idGood`,`idBad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Work table to point idPerson = idBad records to idGood';

CREATE TABLE `PersonDupJFHWorkBkup` (
  `idGood` int(11) default NULL,
  `idBad` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `PersonDupsRemoved` (
  `idPersons` int(11) NOT NULL default '0',
  `PersonEmail` varchar(200) default NULL,
  `LastName` varchar(45) NOT NULL default '',
  `FirstName` varchar(45) default NULL,
  `MiddleName` varchar(45) default NULL,
  `Salutation` varchar(20) default NULL,
  `Line1Address` varchar(200) default NULL,
  `Line2Address` varchar(200) default NULL,
  `City` varchar(45) default NULL,
  `StateProvince` varchar(45) default NULL,
  `ZipPostalCode` varchar(45) default NULL,
  `Country` varchar(45) default NULL,
  `Title` varchar(45) default NULL,
  `Company` varchar(45) default NULL,
  `ActiveFlag` tinyint(1) default NULL,
  `LongBio` text,
  `ShortBio` text,
  `PublicName` varchar(100) default NULL,
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `OldKey` bigint(20) default NULL,
  `suffix` varchar(10) default NULL,
  `PersonUrl` varchar(200) default NULL,
  `AlternateProgramName` varchar(200) default NULL,
  `PublicAnonymousFlag` tinyint(1) default NULL,
  `ReportAlphaSort` varchar(45) default NULL,
  `BadgeName` varchar(45) default NULL,
  `BadAddressFlag` tinyint(1) default NULL,
  `DirectoryComment` varchar(4000) default NULL,
  PRIMARY KEY  (`idPersons`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `PersonMatched` (
  `idPersons` int(11) NOT NULL default '0',
  `MatchedFlag` tinyint(1) default NULL,
  PRIMARY KEY  (`idPersons`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Persons` (
  `idPersons` int(11) NOT NULL auto_increment,
  `PersonEmail` varchar(200) default NULL,
  `LastName` varchar(45) NOT NULL default '',
  `FirstName` varchar(45) default NULL,
  `MiddleName` varchar(45) default NULL,
  `Salutation` varchar(20) default NULL,
  `Line1Address` varchar(200) default NULL,
  `Line2Address` varchar(200) default NULL,
  `City` varchar(45) default NULL,
  `StateProvince` varchar(45) default NULL,
  `ZipPostalCode` varchar(45) default NULL,
  `Country` varchar(45) default NULL,
  `Title` varchar(45) default NULL,
  `Company` varchar(45) default NULL,
  `ActiveFlag` tinyint(1) default NULL,
  `LongBio` text,
  `ShortBio` text,
  `PublicName` varchar(100) default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `OldKey` bigint(20) default NULL,
  `suffix` varchar(10) default NULL,
  `PersonUrl` varchar(200) default NULL,
  `AlternateProgramName` varchar(200) default NULL,
  `PublicAnonymousFlag` tinyint(1) default NULL,
  `ReportAlphaSort` varchar(45) default NULL,
  `BadgeName` varchar(45) default NULL,
  `BadAddressFlag` tinyint(1) default NULL,
  `DirectoryComment` varchar(4000) default NULL,
  PRIMARY KEY  (`idPersons`),
  KEY `fkSalutation` (`Salutation`)
) ENGINE=InnoDB AUTO_INCREMENT=4328 DEFAULT CHARSET=latin1 COMMENT='People involved in the convention. May suggest items or be a';

CREATE TABLE `PersonTimeExclusions` (
  `idPersonExclusion` int(11) NOT NULL auto_increment,
  `idPersons` int(11) NOT NULL COMMENT 'FK to the person',
  `idTimePeriods` int(11) NOT NULL COMMENT 'FK to the time period',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `CreateUser` varchar(45) NOT NULL,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idPersonExclusion`),
  KEY `fkExclusionPersons` (`idPersons`),
  KEY `fkExclusionTimePeriod` (`idTimePeriods`)
) ENGINE=InnoDB AUTO_INCREMENT=38695 DEFAULT CHARSET=latin1;

CREATE TABLE `ProgramItems` (
  `idItems` int(11) NOT NULL auto_increment,
  `ItemName` varchar(200) default NULL,
  `ItemDesc` text,
  `idConventions` int(11) default NULL,
  `RecurringFlag` tinyint(1) NOT NULL default '0',
  `idItemsParent` int(11) default NULL,
  `idTrackPrimary` int(11) default NULL,
  `idTrackSecondary` int(11) default NULL,
  `idFormats` int(11) default NULL,
  `idSlots` int(11) default NULL,
  `PublicDesc` text,
  `EquipmentDesc` text,
  `SuggestedPanelists` text,
  `SuggestedModerators` text,
  `AdminNotes` text,
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `ItemStatus` varchar(20) NOT NULL default 'proposed',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `PersonInfo` text,
  `idUserProposer` int(11) default NULL,
  `ProgramItemType` varchar(20) NOT NULL default 'unassigned',
  `GeneralSignupFlag` tinyint(1) default NULL,
  `ModeratorSignupFlag` tinyint(1) default NULL,
  `GridDesc` varchar(45) default NULL,
  `GridNumber` varchar(8) default NULL,
  `HashTag` varchar(30) default NULL,
  PRIMARY KEY  (`idItems`),
  KEY `fkItemsParent` (`idItemsParent`),
  KEY `fkItemsSlot` (`idSlots`),
  KEY `fkItemsConventions` (`idConventions`),
  KEY `fkItemsStatus` (`ItemStatus`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`),
  KEY `fkItemsProposer` (`idUserProposer`),
  KEY `fkItemType` (`ProgramItemType`),
  KEY `fkItemTrackP` (`idTrackPrimary`),
  KEY `fkItemTrackS` (`idTrackSecondary`),
  KEY `fkItemFormat` (`idFormats`)
) ENGINE=InnoDB AUTO_INCREMENT=1831 DEFAULT CHARSET=latin1 COMMENT='A single item held at the convention. Panel, party, etc. Par';

CREATE TABLE `ProgramScheduleItems` (
  `idProgramScheduleItems` int(11) NOT NULL auto_increment,
  `idSchedules` int(11) NOT NULL,
  `idItems` int(11) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idProgramScheduleItems`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Program Items included by a person on their Personal Schedul';

CREATE TABLE `RefAlternateDataTypes` (
  `idDataTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idDataTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAlternateScopes` (
  `idScopes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScopes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAssignmentStatus` (
  `idAssignmentStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idAssignmentStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAssignmentTypes` (
  `idAssignmentTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idAssignmentTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefAuthorityCodes` (
  `idCodes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `ActiveFlag` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idCodes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `RefCapacityUnits` (
  `idUnits` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idUnits`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefConnectionTypes` (
  `idConnectionTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idConnectionTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RefConventionLimitTypes` (
  `idLimitTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idLimitTypes`),
  UNIQUE KEY `RefLimitTypeUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `RefDistributionStatus` (
  `idDistributionStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idDistributionStatus`),
  UNIQUE KEY `RefDistributionStatusUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefItemStatus` (
  `idItemStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idItemStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLineStatus` (
  `idLineStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idLineStatus`),
  UNIQUE KEY `RefLineStatusUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLocationTypes` (
  `idLocationTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLocationTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `RefLocTimeRankings` (
  `idLocTimeRankings` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idLocTimeRankings`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefOrderMethod` (
  `idRef` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRef`),
  UNIQUE KEY `RefOrderMethodsUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefOrderStatus` (
  `idOrderStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idOrderStatus`),
  UNIQUE KEY `RefOrderStatusUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefPhoneType` (
  `idRef` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRef`),
  UNIQUE KEY `RefPhoneType` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `RefPreferenceTypes` (
  `idPreferenceTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` varchar(2000) NOT NULL,
  `VisibleToUserFlag` tinyint(1) NOT NULL default '1' COMMENT 'Can the person see this preference, or is it hidden for admin use?',
  `EditByUserFlag` tinyint(1) NOT NULL default '1' COMMENT 'Can this person edit this preference, or is it admin only?',
  `DisplayPrompt` varchar(45) NOT NULL COMMENT 'Prompt to show for this preference in the UI',
  `TextFieldFlag` tinyint(1) NOT NULL default '0' COMMENT 'Display a text field for entry with this preference?',
  `OrderBy` int(11) default NULL COMMENT 'Order preference information by this, then by prompt',
  `ConventionFlag` tinyint(1) NOT NULL default '0' COMMENT 'If true, preference only applies to a single convention and idConventions should be set.',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `TextFieldPrompt` varchar(45) default NULL,
  PRIMARY KEY  (`idPreferenceTypes`),
  UNIQUE KEY `RefPreferenceTypes` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COMMENT='Types of Personal Preferences';

CREATE TABLE `RefProgramFormats` (
  `idProgramFormats` int(11) NOT NULL auto_increment,
  `Title` varchar(80) NOT NULL default '',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idProgramFormats`),
  UNIQUE KEY `RefTitle` (`Title`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefProgramItemTypes` (
  `idProgramItemTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  `ActiveFlag` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idProgramItemTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `RefProgramTracks` (
  `idProgramTracks` int(11) NOT NULL auto_increment,
  `Name` varchar(80) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `PanelistDescription` text,
  `DisplayOrder` int(11) default NULL,
  PRIMARY KEY  (`idProgramTracks`),
  UNIQUE KEY `TrackName` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `RefRankingTypes` (
  `idRankingTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idRankingTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefRoleTypes` (
  `idRoleTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idRoleTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

CREATE TABLE `RefSalutations` (
  `idSalutations` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSalutations`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `RefScheduleStatus` (
  `idScheduleStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` varchar(2000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idScheduleStatus`),
  UNIQUE KEY `RefScheduleStatus` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Types of Personal Preferences';

CREATE TABLE `RefTransactionSources` (
  `idTransactionSource` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idTransactionSource`),
  UNIQUE KEY `RefTransSource` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

CREATE TABLE `RefTransactionStatus` (
  `idTransactionStatus` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idTransactionStatus`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `RefTransactionTypes` (
  `idTransactionTypes` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idTransactionTypes`),
  UNIQUE KEY `RefCode` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `RefURLType` (
  `idRef` int(11) NOT NULL auto_increment,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRef`),
  UNIQUE KEY `RefURLTypeUK` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL,
  `RoleType` varchar(20) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRoles`),
  KEY `fkRoleUsers` (`idUsers`),
  KEY `fkRoleType` (`RoleType`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1 COMMENT='What is the owner of this profile allowed to do?';

CREATE TABLE `Slots` (
  `idSlots` int(11) NOT NULL auto_increment,
  `idLocations` int(11) NOT NULL default '0',
  `idTimePeriods` int(11) NOT NULL default '0',
  `SlotDesc` text,
  `CapacityAmt` int(11) default NULL,
  `CapacityUnitCode` varchar(20) default NULL,
  `PublicFlag` tinyint(1) NOT NULL default '1',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idSlots`),
  KEY `fkLocations` (`idLocations`),
  KEY `fkSlotPeriod` (`idTimePeriods`),
  KEY `fkCapacityUnits` (`CapacityUnitCode`)
) ENGINE=InnoDB AUTO_INCREMENT=3609 DEFAULT CHARSET=latin1 COMMENT='Combination of convention, location, and time period. Did no';

CREATE TABLE `SpecialBadges` (
  `idSpecialBadges` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `SpecialType` varchar(20) NOT NULL default 'Ask Me',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idSpecialBadges`),
  KEY `fkSpecialBadgesConventions` (`idConventions`),
  KEY `fkSpecialBadgesPersons` (`idPersons`)
) ENGINE=InnoDB AUTO_INCREMENT=865 DEFAULT CHARSET=latin1 COMMENT='Special Badges for a convention';

CREATE TABLE `SpecialBadgesBackup` (
  `idSpecialBadges` int(11) NOT NULL default '0',
  `idConventions` int(11) NOT NULL,
  `idPersons` int(11) NOT NULL,
  `SpecialType` varchar(20) NOT NULL default 'Ask Me',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `TimePeriods` (
  `idTimePeriods` int(11) NOT NULL auto_increment,
  `idConventions` int(11) NOT NULL default '0',
  `StartDateTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `EndDateTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `PeriodComment` text,
  `StandardTimeFlag` tinyint(1) NOT NULL default '0' COMMENT 'Is this a normal time that goes in the grid?',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `WarnOnSecondUseFlag` tinyint(1) NOT NULL default '0' COMMENT 'Warn users when trying to use this time period more than once?',
  `PublicStartTime` datetime default NULL,
  `PublicEndTime` datetime default NULL,
  PRIMARY KEY  (`idTimePeriods`),
  KEY `fkLimitConvention` (`idConventions`),
  KEY `fkLimitUnits` (`StandardTimeFlag`),
  KEY `fkLimitType` (`StartDateTime`)
) ENGINE=InnoDB AUTO_INCREMENT=654 DEFAULT CHARSET=latin1 COMMENT='For a particular convention, when do things happen. There ar';

CREATE TABLE `UserAuthority` (
  `idUserAuthority` int(11) NOT NULL auto_increment,
  `idUsers` int(11) NOT NULL default '0',
  `idPersons` int(11) default '0',
  `StartTimestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `EndTimestamp` datetime default NULL,
  `AuthorityCode` varchar(20) NOT NULL default 'owner',
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idUserAuthority`),
  KEY `fkAuthorityProfile` (`idUsers`),
  KEY `fkAuthorityPersons` (`idPersons`),
  KEY `fkUserAuthorityCode` (`AuthorityCode`)
) ENGINE=InnoDB AUTO_INCREMENT=4584 DEFAULT CHARSET=latin1 COMMENT='This user has row-level authority for this person record, fo';

CREATE TABLE `UserAuthorityBackup` (
  `idUserAuthority` int(11) NOT NULL default '0',
  `idUsers` int(11) NOT NULL default '0',
  `idPersons` int(11) NOT NULL default '0',
  `StartTimestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `EndTimestamp` datetime default NULL,
  `AuthorityCode` varchar(20) NOT NULL default 'owner',
  `CreateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL auto_increment,
  `ProfileEmail` varchar(100) NOT NULL default '',
  `ProfilePassword` varchar(45) NOT NULL default '',
  `StartDate` date NOT NULL default '0000-00-00',
  `EndDate` date default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  `ForceChangeFlag` tinyint(1) default NULL,
  PRIMARY KEY  (`idUsers`),
  UNIQUE KEY `iProfilesEmail` (`ProfileEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=3536 DEFAULT CHARSET=latin1 COMMENT='Account on the application. ProfileEmail address is the user';

CREATE TABLE `WebPage` (
  `idWebPage` int(10) unsigned NOT NULL auto_increment,
  `PageName` varchar(64) NOT NULL,
  `PageTitle` varchar(255) NOT NULL,
  `PageContent` text,
  PRIMARY KEY  (`idWebPage`),
  UNIQUE KEY `PageName` (`PageName`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

alter table AlternateNames add (
  CONSTRAINT `fkAlternateNamesDataType` FOREIGN KEY (`AlternateDataType`) REFERENCES `RefAlternateDataTypes` (`Code`),
  CONSTRAINT `fkAlternateNamesPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkAlternateNamesScope` FOREIGN KEY (`AlternateScope`) REFERENCES `RefAlternateScopes` (`Code`)
);
alter table  Assignments add (
  CONSTRAINT `fkAssignmentsItems` FOREIGN KEY (`idItems`) REFERENCES `ProgramItems` (`idItems`),
  CONSTRAINT `fkAssignmentsPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkAssignmentsStatus` FOREIGN KEY (`AssignmentStatus`) REFERENCES `RefAssignmentStatus` (`Code`),
  CONSTRAINT `fkAssignmentsTypes` FOREIGN KEY (`AssignmentType`) REFERENCES `RefAssignmentTypes` (`Code`)
);
alter table BadgeSorts add (
  CONSTRAINT `fkBadgeSortsConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkBadgeSortsPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`)
);

alter table CatalogItems add (
  CONSTRAINT `fkCatalogConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkCatalogItemsCapacityUnits` FOREIGN KEY (`CapacityUnitCode`) REFERENCES `RefCapacityUnits` (`Code`),
  CONSTRAINT `fkCatalogItemsLimitType` FOREIGN KEY (`ConventionLimitType`) REFERENCES `RefConventionLimitTypes` (`Code`)
);

alter table ConventionLimits add (
  CONSTRAINT `fkConventionLimitsConvention` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkConventionLimitsLimit` FOREIGN KEY (`LimitType`) REFERENCES `RefConventionLimitTypes` (`Code`),
  CONSTRAINT `fkConventionLimitsUnits` FOREIGN KEY (`LimitUnits`) REFERENCES `RefCapacityUnits` (`Code`)
);

alter table Distributions add (
  CONSTRAINT `fkDistributionsFinanTrans` FOREIGN KEY (`idFinancialTransactions`) REFERENCES `FinancialTransactions` (`idFinancialTransactions`),
  CONSTRAINT `fkDistributionsOrderLines` FOREIGN KEY (`idOrderLines`) REFERENCES `OrderLines` (`idOrderLines`),
  CONSTRAINT `fkDistributionsStatus` FOREIGN KEY (`DistributionStatus`) REFERENCES `RefDistributionStatus` (`Code`)
);

alter table FinancialTransactions add (
  CONSTRAINT `fkFinancialTransactionSource` FOREIGN KEY (`TransactionSource`) REFERENCES `RefTransactionSources` (`Code`),
  CONSTRAINT `fkFinancialTransactionsPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkFinancialTransactionStatus` FOREIGN KEY (`TransactionStatus`) REFERENCES `RefTransactionStatus` (`Code`),
  CONSTRAINT `fkFinancialTransactionsType` FOREIGN KEY (`TransactionType`) REFERENCES `RefTransactionTypes` (`Code`)
);

alter table Guests add (
  CONSTRAINT `fkGuestsConvention` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkGuestsPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`)
);

alter table ItemLocationRankings add (
  CONSTRAINT `fkItemLocationRankingsItem` FOREIGN KEY (`idItems`) REFERENCES `ProgramItems` (`idItems`),
  CONSTRAINT `fkItemLocationRankingsLocation` FOREIGN KEY (`idLocations`) REFERENCES `Locations` (`idLocations`)
);

alter table ItemRankings add (
  CONSTRAINT `fkItemRankingsItem` FOREIGN KEY (`idItems`) REFERENCES `ProgramItems` (`idItems`),
  CONSTRAINT `fkItemRankingsPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkItemRankingsRanking` FOREIGN KEY (`UserInterestRanking`) REFERENCES `RefRankingTypes` (`Code`)
);

alter table ItemTimePeriodRankings add (
  CONSTRAINT `fkITPRItem` FOREIGN KEY (`idItems`) REFERENCES `ProgramItems` (`idItems`),
  CONSTRAINT `fkITPRRanking` FOREIGN KEY (`Ranking`) REFERENCES `RefRankingTypes` (`Code`),
  CONSTRAINT `fkITPRTimePeriod` FOREIGN KEY (`idTimePeriods`) REFERENCES `TimePeriods` (`idTimePeriods`)
);

alter table Locations add (
  CONSTRAINT `fkLocationsCapacityUnits` FOREIGN KEY (`CapacityUnitCode`) REFERENCES `RefCapacityUnits` (`Code`),
  CONSTRAINT `fkLocationsParent` FOREIGN KEY (`parentidLocations`) REFERENCES `Locations` (`idLocations`),
  CONSTRAINT `fkLocationsTypes` FOREIGN KEY (`LocationType`) REFERENCES `RefLocationTypes` (`Code`)
);

alter table OrderLines add (
  CONSTRAINT `fkOrderLinesCatalog` FOREIGN KEY (`idCatalog`) REFERENCES `CatalogItems` (`idCatalog`),
  CONSTRAINT `fkOrderLinesOrders` FOREIGN KEY (`idOrders`) REFERENCES `Orders` (`idOrders`),
  CONSTRAINT `fkOrderLinesPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`)
);

alter table Orders add (
  CONSTRAINT `fkOrdersMethod` FOREIGN KEY (`OrderMethodCode`) REFERENCES `RefOrderMethod` (`Code`),
  CONSTRAINT `fkOrdersPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkOrdersStatus` FOREIGN KEY (`OrderStatus`) REFERENCES `RefOrderStatus` (`Code`)
);

alter table PeopleConnections add (
  CONSTRAINT `fkPeopleConnectionsFrom` FOREIGN KEY (`idPersonsFrom`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkPeopleConnectionsTo` FOREIGN KEY (`idPersonsTo`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkPeopleConnectionsType` FOREIGN KEY (`ConnectionType`) REFERENCES `RefConnectionTypes` (`Code`)
);

alter table PersonalPreferences add (
  CONSTRAINT `fkPreferencesConvention` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkPreferencesPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkPreferencesType` FOREIGN KEY (`PreferenceType`) REFERENCES `RefPreferenceTypes` (`idPreferenceTypes`)
);

alter table PersonalScheduleItems add (
  CONSTRAINT `fkPersSchedItemSchedule` FOREIGN KEY (`idSchedules`) REFERENCES `PersonalSchedules` (`idSchedules`)
);

alter table PersonalSchedules add (
  CONSTRAINT `fkPersonalSchedulesConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkPersonalSchedulesPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkPersonalSchedulesStatus` FOREIGN KEY (`idScheduleStatus`) REFERENCES `RefScheduleStatus` (`idScheduleStatus`)
);

alter table PersonAvailability add (
  CONSTRAINT `fkPersonAvailabilityConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkPersonAvailabilityPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`)
);

alter table Roles add (
  CONSTRAINT `fkRolesType` FOREIGN KEY (`RoleType`) REFERENCES `RefRoleTypes` (`Code`),
  CONSTRAINT `fkRolesUsers` FOREIGN KEY (`idUsers`) REFERENCES `Users` (`idUsers`)
);

alter table Slots add (
  CONSTRAINT `fkSlotsCapacityUnits` FOREIGN KEY (`CapacityUnitCode`) REFERENCES `RefCapacityUnits` (`Code`),
  CONSTRAINT `fkSlotsLocations` FOREIGN KEY (`idLocations`) REFERENCES `Locations` (`idLocations`),
  CONSTRAINT `fkSlotsTimePeriods` FOREIGN KEY (`idTimePeriods`) REFERENCES `TimePeriods` (`idTimePeriods`)
);

alter table SpecialBadges add (
  CONSTRAINT `fkSpecialBadgesConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`),
  CONSTRAINT `fkSpecialBadgesPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`)
);

alter table TimePeriods add (
  CONSTRAINT `fkTimePeriodsConventions` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`)
);

alter table UserAuthority add (
  CONSTRAINT `fkUserAuthorityCode` FOREIGN KEY (`AuthorityCode`) REFERENCES `RefAuthorityCodes` (`Code`),
  CONSTRAINT `fkUserAuthorityPersons` FOREIGN KEY (`idPersons`) REFERENCES `Persons` (`idPersons`),
  CONSTRAINT `fkUserAuthorityUsers` FOREIGN KEY (`idUsers`) REFERENCES `Users` (`idUsers`)
);

alter table  add (
);

alter table  add (
);

alter table  add (
);


