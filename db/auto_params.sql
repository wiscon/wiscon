DROP TABLE IF EXISTS `AutoAssignmentParameters`;
CREATE TABLE `AutoAssignmentParameters` (
  `idParam` int(11) NOT NULL auto_increment,
  `Name` varchar(45) NOT NULL default '',
  `Value` varchar(255) NOT NULL default '',
  `Description` text NOT NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL default '',
  `UpdateUser` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`idParam`),
  UNIQUE KEY `RefCode` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `AutoAssignmentParameters` VALUES 
(1,'status','accepted','Status of items to be auto-assigned',now(),'','jhkim@darkshire.net',''),
(2,'min_total_interest','0','Minimum total interest expressed by users in items to be auto-assigned',now(),'','jhkim@darkshire.net',''),
(3,'num_interest_ranks','3','Maximum relative weight of top interest compared to lowest interest',now(),'','jhkim@darkshire.net',''),
(4,'max_interest_weight','5','Maximum relative weight of top interest compared to lowest interest',now(),'','jhkim@darkshire.net',''),
(5,'default_num_participants','4','Default number of participants per item',now(),'','jhkim@darkshire.net',''),
(6,'max_assignments','3','Maximum number of items a person may be assigned to',now(),'','jhkim@darkshire.net',''),
(7,'weight_item_interest','10','Weight for participant priority given to the user ranking for this particular item',now(),'','jhkim@darkshire.net',''),
(8,'weight_num_assignments','5','Weight for participant priority given to the assignments the person already has',now(),'','jhkim@darkshire.net',''),
(9,'weight_total_interest','1','Weight for participant priority given to the total interest the person has expressed',now(),'','jhkim@darkshire.net',''),
(10,'mod_weight_num_assignments','5','Weight for moderator priority given to the number of assignments the person already has',now(),'','jhkim@darkshire.net',''),
(11,'mod_weight_num_flags','2','Weight for moderator priority given to the number of items the person is interested in moderating',now(),'','jhkim@darkshire.net',''),
(12,'mod_weight_item_interest','2','Negative weight for moderator priority given to the total interest the person has expressed',now(),'','jhkim@darkshire.net',''),
(13,'assignment_order','mod-par','Order of assigning participants and moderators to items',now(),'','jhkim@darkshire.net',''),
(14,'freelance_moderator_item_id','342','Item ID number that users expressed interest in to do freelance moderating',now(),'','jhkim@darkshire.net','')
;

