

--
-- Table structure for table `Config`
--

DROP TABLE IF EXISTS `Config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Config` (
  `idConfig` int(11) NOT NULL auto_increment,
  `idConventions` int(11) default NULL,
  `ConfigCode` varchar(45) default NULL,
  `ConfigComment` varchar(4000) default NULL,
  `StartDatetime` datetime NOT NULL,
  `EndDatetime` datetime default NULL,
  `CreateDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `UpdateDate` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CreateUser` varchar(45) NOT NULL,
  `UpdateUser` varchar(45) NOT NULL,
  PRIMARY KEY  (`idConfig`),
  KEY `fkConfigConvention` (`idConventions`),
  CONSTRAINT `fkConfigConvention` FOREIGN KEY (`idConventions`) REFERENCES `Conventions` (`idConventions`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='configuration guidance to the application';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Config`
--

LOCK TABLES `Config` WRITE;
/*!40000 ALTER TABLE `Config` DISABLE KEYS */;
INSERT INTO `Config` VALUES 
(1,40,'party','Submit Party Applications','2016-01-16 00:00:00','2016-03-15 23:59:59','2016-01-25 20:45:55','2016-02-22 20:29:09','2539','2539'),
(2,40,'artshow','Submit ArtShow Applications','2016-01-16 00:00:00','2016-03-15 23:59:59','2016-01-25 23:00:13','2016-01-25 23:00:13','2539','2539'),
(3,40,'dealer','Submit Dealer Applications','2016-01-16 00:00:00','2016-03-01 11:59:59','2016-01-25 23:00:25','2016-02-22 20:02:03','2539','2539'),
(4,40,'idea','Submit Ideas','2015-06-01 00:00:00','2016-01-29 23:59:59','2016-01-25 23:01:36','2016-01-25 23:01:36','2539','2539'),
(5,40,'reading','Submit Reading Proposals','2016-01-02 00:00:00','2016-03-30 23:59:59','2016-01-25 23:02:16','2016-02-22 20:33:10','2539','2539'),
(6,40,'paper','Submit Paper Proposals','2015-11-01 00:00:00','2016-02-01 23:59:59','2016-01-25 23:02:43','2016-02-22 20:28:53','2539','2539'),
(7,40,'registration','Registration Available','2015-05-24 00:00:00','2016-05-30 23:59:59','2016-01-25 23:02:51','2016-01-25 23:02:51','2539','2539'),
(8,40,'programSignup','Signup for Programming','2016-02-18 00:00:00','2016-03-18 23:59:59','2016-01-25 23:03:12','2016-02-21 23:47:50','2539','2539'),
(9,40,'programTentative','Programming Tentative Schedule','2016-03-25 00:00:00','2016-04-15 23:59:59','2016-01-25 23:03:34','2016-01-25 23:03:34','2539','2539'),
(10,40,'programApprove','Programming Approve Schedule','2016-04-16 00:00:00','2016-05-01 23:59:59','2016-01-25 23:04:03','2016-01-25 23:04:03','2539','2539'),
(11,40,'programFinal','Programming Final Schedule ','2016-05-02 00:00:00','2017-02-25 23:59:59','2016-01-25 23:04:52','2016-01-25 23:04:52','2539','2539');
/*!40000 ALTER TABLE `Config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-04  9:13:54
