INSERT INTO wiscon.RefAlternateDataTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefAlternateScopes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefAssignmentStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefAssignmentTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefAuthorityCodes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser, ActiveFlag) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser', ActiveFlag);
INSERT INTO wiscon.RefCapacityUnits
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefConnectionTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefConventionLimitTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefDistributionStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefItemStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefLineStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefLocationTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefLocTimeRankings
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefOrderMethod
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefOrderStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefPreferenceTypes
(Code, Description, VisibleToUserFlag, EditByUserFlag, DisplayPrompt, TextFieldFlag, OrderBy, ConventionFlag, CreateDate, UpdateDate, CreateUser, UpdateUser, TextFieldPrompt) 
VALUES ('Code', 'Description', VisibleToUserFlag, EditByUserFlag, 'DisplayPrompt', TextFieldFlag, OrderBy, ConventionFlag, 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser', 'TextFieldPrompt');
INSERT INTO wiscon.RefProgramFormats
(Title, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Title', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefProgramItemTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser, ActiveFlag) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser', ActiveFlag);
INSERT INTO wiscon.RefProgramTracks
(Name, Description, CreateDate, UpdateDate, CreateUser, UpdateUser, PanelistDescription) 
VALUES ('Name', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser', 'PanelistDescription');
INSERT INTO wiscon.RefRankingTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefRoleTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefSalutations
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefScheduleStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefTransactionSources
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefTransactionStatus
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');
INSERT INTO wiscon.RefTransactionTypes
(Code, Description, CreateDate, UpdateDate, CreateUser, UpdateUser) 
VALUES ('Code', 'Description', 'CreateDate', 'UpdateDate', 'CreateUser', 'UpdateUser');

