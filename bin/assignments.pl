#!/usr/bin/perl
use strict;  
use lib "../lib";
use Data::Dumper;
use WisCon::Assignment;
use WisCon::Person;
use WisCon::ProgramItem;
use WisCon::SQLEngine;
use FileHandle;
use Getopt::Long;
use strict;

# Set default options
my $help = undef;
my $verbose = undef;
my $testmode = undef;
my $max_report = 10;

sub usage {
    return <<"EndUsage";
 $0

 This reports on the status of assignments, particularly auto-assignment, 
 to help diagnose what could be done better.  

 Optional arguments:
 -h        Print this message
 -v        Verbose output
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose);
if ($help) { print usage(); exit(); }

my $sqldb = WisCon::SQLEngine::sqldb(); 

# reload the items from the database to find how many assignments each has
my @items = WisCon::ProgramItem->all_auto_assigned();
my %item_of_id;
my $num_missing_mods = 0;
my @num_missing_par;
for my $item (@items) {
	$item_of_id{$item->id} = $item;
	$num_missing_mods++ if ($item->open_moderator_slots);
	$num_missing_par[ $item->open_participant_slots ]++;
	print "id=".$item->id." missing 4\n" if ($item->open_participant_slots > 2);
}
print "Among auto-assignable items, there are:\n";
print " * $num_missing_mods items with no moderator.\n";
for (1 .. $#num_missing_par) {
	print " * ".($num_missing_par[$_] || 0)." items with $_ missing participants.\n";
}
my @users = grep { $_->total_interest > 0 } WisCon::Person->fetch_all_users();
my @num_assignments;
my $max_assignments = WisCon::ProgramItem->param('max_assignments');
for (@users) {
	$num_assignments[$_->assignments]++;
}
print "Among " . scalar(@users) . " persons who expressed interest, there are:\n";
for (0 .. $#num_assignments) {
	print " * ".($num_assignments[$_] || 0)." persons with $_ assignments.\n";
}
print "Among persons with less than the max of $max_assignments assignments,\n";
my @unmet_interests;
for my $person (grep { $_->assignments < $max_assignments } @users) {
	my $assignments = $sqldb->fetch_select(sql => ['select idItems from Assignments where idPersons=? and AssignmentType="participant"', $person->id()]);
	my %is_assigned;
	for (@$assignments) { $is_assigned{ $_->{idItems} }++; }

	my $unmet = 0;
	my $interests = $sqldb->fetch_select(sql => ['select idItems from ItemRanking where UserInterestRanking < 9 and idUsers=?', $person->user_id()]);
	for (@$interests) {
		my $id = $_->{idItems};
		$unmet++ if ($item_of_id{$id} and not $is_assigned{$id});
	}
	# max out unmet with the number of open slots a person has
	my $unmet_max = $max_assignments - $person->assignments;
	if ($unmet > $unmet_max) { $unmet = $unmet_max; }

	$unmet_interests[$unmet] = [] unless ($unmet_interests[$unmet]);
	push(@{ $unmet_interests[$unmet] }, $person->id);
}
print "there are the following counts of unmet interests:\n";
print "(i.e. accepted items they had interest in but were not assigned to)\n";
for (0 .. $#unmet_interests) {
	my $aref = $unmet_interests[$_] or next;
	print " * ".scalar(@$aref)." with $_ unmet interests\n";
	print "   idPersons ".join(",",@$aref)."\n" if ($verbose or @$aref < $max_report);
}

if ($verbose) {
	for my $missing (1 .. $#num_missing_par) {
		print "Items missing $missing participants:\n";
		for (grep { $_->open_participant_slots == $missing } @items) {
			print "  (ID".$_->id.") ".$_->name."\n";
		}
	}

	for my $n (0 .. $#num_assignments) {
		next if ($n == $max_assignments);
		print "Persons with $n assignments:\n";
		for (grep { $_->assignments == $n } @users) {
			print "  (ID".$_->id.") ".$_->full_name."\n";
		}
	}
}
