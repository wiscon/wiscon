#!/usr/bin/perl
use strict;  
use lib "../lib";
use Data::Dumper;
use WisCon::Assignment;
use WisCon::Person;
use WisCon::ProgramItem;
use WisCon::SQLEngine;
use FileHandle;
use Getopt::Long;
use Time::Local;
use strict;

# Set default options
my $help = undef;
my $verbose = undef;
my $testmode = undef;
my $add_only = undef;
my $confirmmode = undef;
my $max_items = 0;
my $userhome = ($ENV{'HOME'} || $ENV{'LOGDIR'} || (getpwuid($>))[7]);

sub usage {
    return <<"EndUsage";
 $0

 This tests the magic button code for assigning people to panels 
 and panels to rooms and time slots. 

 Optional arguments:
 -h        Print this message
 -v        Verbose output
 -t        Test mode
 -a        Add only - do not delete prior auto-assignments
 -i=<n>    Maximum number of items to process
 -s=<id>   Stop at the specified item id number
 -c        Interactive confirmation for each item
           Use c=incomplete to confirm only after highly incomplete items
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose, 
	   "t" => \$testmode, "a" => \$add_only, "i=i" => \$max_items, 
	   "c:s" => \$confirmmode);
if ($help) { print usage(); exit(); }

my $sqldb = WisCon::SQLEngine::sqldb(); 

# Delete the old auto-assignments
if ($add_only) {
	print "Not deleting old auto-assignments\n";
} else {
	print "Deleting old auto-assignments\n";
	$sqldb->do_delete(table=>'Assignments',where=>{AssignmentStatus=>'auto'});
}

# Cache the names for each ranking code (i.e. 1=>Please, etc.)
my %rank_of_code;
my $ranks = $sqldb->fetch_select(table=>'RefRankingType');
for (@$ranks) { $rank_of_code{ $_->{Code} } = $_->{Description}; }

# Order items by how much interest is in them, least interest first
my $order = WisCon::ProgramItem->param('assignment_order');
my @items = WisCon::ProgramItem->all_auto_assigned();
my @assignments;


for my $item (@items) {
	print "Processing item ".$item->id." : ".$item->name."\n";

	my @moderators;
	my @participants;
	if ($order eq "par-mod") {
		@participants = WisCon::Assignment->auto_assign_participants(item=>$item);
		@moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
		push(@assignments, @participants, @moderators);
	} elsif ($order eq "mod-par") {
		@moderators = WisCon::Assignment->auto_assign_moderators(item=>$item);
		@participants = WisCon::Assignment->auto_assign_participants(item=>$item);
		push(@assignments, @moderators, @participants);
	} else {
		die "Order '$order' not allowed";
	}

	for (@participants) {
		if ($_->success and $_->person) {
			print " - Assigning participant ".$_->person->full_name." (idPersons=".$_->person->id.", assignments=".$_->person->assignments.")\n";
		} else {
			print " - Empty participant assignment\n";
		}
	}
	for (@moderators) {
		if ($_->success and $_->person) {
			print " - Assigning moderator ".$_->person->full_name." (idPersons=".$_->person->id.", assignments=".$_->person->assignments.")\n";
		} else {
			print " - Empty participant assignment\n";
		}
	}
	my $nfailed = scalar(grep { not $_->success} @participants,@moderators);
	if ($confirmmode and ($confirmmode ne "incomplete" or $nfailed > 2)) {
		print "Continue ? (Y/N) ";
		my $response = <STDIN>;
		last unless ($response !~ m/^y/i);
	}

	print "End item ".$item->id." : ".$item->name."\n";
} # end of for my $item (@items)

WisCon::Assignment->assign_freelance_moderators(@assignments);

my $num_incomplete = 0;
my %incomplete_of_id;
for (@assignments) {
	if (not $_->success) {
		$num_incomplete++;
		$incomplete_of_id{$_->item->id}++;
	}
}

print "\n\nThere are $num_incomplete missed assignments in ".scalar(keys %incomplete_of_id)." program items.\n";

my @unused = grep { $_->assignments < 1 and $_->total_interest > 0 } WisCon::Person->fetch_all_users();
print "There are ".scalar(@unused)." users who expressed interest that have no assignments.\n";
if ($verbose) {
	for (@unused) {
		print " - " . $_->full_name . " (idPersons=" . $_->id . ")\n";
	}
}

# reload the items from the database to find how many assignments each has
@items = WisCon::ProgramItem->all_auto_assigned();
my $num_missing_mods = 0;
my @num_missing_par;
for my $item (@items) {
	$num_missing_mods++ if ($item->open_moderator_slots);
	$num_missing_par[ $item->open_participant_slots ]++;
}
print "Among auto-assignable items, there are:\n";
print " * $num_missing_mods items with no moderator.\n";
for (1 .. $#num_missing_par) {
	print " * ".($num_missing_par[$_] || 0)." items with $_ missing participants.\n";
}
my @users = grep { $_->total_interest > 0 } WisCon::Person->fetch_all_users();
my @num_assignments;
for (@users) {
	$num_assignments[$_->assignments]++;
}
print "Among " . scalar(@users) . " persons who expressed interest, there are:\n";
for (0 .. $#num_assignments) {
	print " * ".($num_assignments[$_] || 0)." persons with $_ assignments.\n";
}
