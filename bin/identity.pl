#!/usr/bin/perl
use strict;  
use lib "../lib";
use Data::Dumper;
use WisCon::SQLEngine;
use WisCon::Person;
use FileHandle;
use Getopt::Long;
use Time::Local;
use strict;

# Set default options
my $help;
my $verbose;
my $do_delete;
my $userhome = ($ENV{'HOME'} || $ENV{'LOGDIR'} || (getpwuid($>))[7]);

sub usage {
    return <<"EndUsage";
 $0

 This identifies users with multiple identities in the database, 
 and regularizes them.  

 Optional arguments:
 -h        Print this message
 -v        Verbose output, reporting detailed differences
 -d        Delete bad references to non-existent Person IDs, and 
           redundant later identities
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose, 
	   "d" => \$do_delete);
if ($help) { print usage(); exit(); }

my $sqldb = WisCon::SQLEngine::sqldb(); 
my $data = $sqldb->fetch_select(sql => "select idUsers,count(*) as count from UserAuthority where AuthorityCode = \"identity\" group by idUsers"); 

my $cols = $sqldb->fetch_select(sql => "show columns from Persons");
my @fields;
for my $col (@$cols) { push(@fields,$col->{Field}); }

my @multid;
for (@$data) { push(@multid,$_->{idUsers}) if ($_->{count} > 1); } 

print "Found ".scalar(@multid)." users with multiple Person identities\n";

for my $user_id (@multid) { 
	my $default = WisCon::Person->find_user($user_id);

	my $user = $sqldb->fetch_one(table => "Users", where => {idUsers => $user_id});
	print "User $user_id (ProfileEmail $user->{ProfileEmail})\n";

	my $identities = $sqldb->fetch_select(sql => "select idPersons from UserAuthority where idUsers=$user_id and AuthorityCode=\"identity\" order by idPersons"); 
	my @person_ids = map { $_->{idPersons} } @$identities;

	for my $person_id (@person_ids) {
		my $person = WisCon::Person->fetch($person_id);
		my $exists = $sqldb->fetch_select(table=>"Persons",where=>{idPersons=>$person_id});
		my $default = ($person_id == $default->id) ? "(default)" : "";
		my $assigned = ($person->num_assignments) ? "(".$person->num_assignments." assignments)" : "";
		if ($exists and @$exists) {
			print " - Person ID $person_id (exists) $assigned $default\n";
		} else {
			print " - Non-existent person ID $person_id $assigned $default\n";
			$sqldb->do_delete( table=>'UserAuthority', where=>{ idPersons=>$person_id } ) if ($do_delete);
		}
	}

	my $persons = $sqldb->fetch_select(sql => "select * from Persons where idPersons in (".join(",",@person_ids).")");
	my $n_ident = scalar(@$persons);
	for my $field (@fields) {
		# track differences in each field
		next if ($field =~ m/^(idPersons|(Create|Update)(User|Date))$/);
		my %ids_of_val;
		for (0 .. ($n_ident-1)) { 
			my $id = $persons->[$_]->{idPersons};
			my $val = $persons->[$_]->{$field};
			$ids_of_val{$val} .= "(id$id)" if ($val);
		}
		my $nvals = scalar(keys %ids_of_val);
		print " - $nvals different values for $field\n" if ($nvals > 1);
		if ($nvals > 1 and $verbose) {
			for (keys %ids_of_val) {
				print "   Value in $ids_of_val{$_}\n";
				print "   '$_'\n";
			}
		}
	} # end of for my $field (@fields)
	if ($do_delete) {
		my @sorted = sort { $a<=>$b } @person_ids;
		my $to_keep = shift(@sorted);
		my $to_delete = join(",",@sorted);
		print " - Keeping $to_keep, deleting $to_delete\n";
		$sqldb->do_delete( table => 'UserAuthority', where => "idPersons in ($to_delete)" );
	}
	print "End of user $user_id (ProfileEmail $user->{ProfileEmail})\n\n";
}

### Now also check for 

print "\n";
my $usercount = $sqldb->fetch_select(sql=>"select idPersons,count(*) as count from UserAuthority where AuthorityCode  = 'identity' group by idPersons order by count");
my @multuser = ();
for (@$usercount) {
	push(@multuser, $_->{idPersons}) if ($_->{count} > 1);
}

print "Found ".scalar(@multuser)." persons that are the identity for multiple Users\n";
for my $person_id (@multuser) {
	my $person = WisCon::Person->fetch($person_id);
	my $users = $sqldb->fetch_select(sql=>["select idUsers from UserAuthority where AuthorityCode  = 'identity' and idPersons = ?",$person_id]);
	print "Person $person_id (Name ".$person->full_name.", PersonEmail ".$person->get_val('PersonEmail')."\n";
	for (@$users) {
		my $user_id = $_->{idUsers};
		my $user = $sqldb->fetch_one(table=>'Users',where=>{idUsers=>$user_id});
		my $rankings = $sqldb->fetch_one(sql=>["select count(*) as count from ItemRanking where idUsers=?",$user_id]);
		my $count = ($rankings->{count}) ? "($rankings->{count} rankings)" : "";
		if ($user) {
			print " - User ID $user_id (exists) $count ProfileEmail ".$user->{ProfileEmail}."\n";
		} else {
			print " - Non-existant User ID $user_id $count\n";
		}
	}
	print "End person $person_id (Name ".$person->full_name.", PersonEmail ".$person->get_val('PersonEmail')."\n";
}
