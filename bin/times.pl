#!/usr/bin/perl
use strict;  
use lib "../lib";
use Data::Dumper;
use WisCon::DateTime;
use WisCon::SQLEngine;
use FileHandle;
use Getopt::Long;
use strict;

# Set default options
my $help = undef;
my $verbose = undef;
my $testmode = undef;
my $tmin = '05:00:00';

sub usage {
    return <<"EndUsage";
 $0

 This reports on the status of assignments, particularly auto-assignment, 
 to help diagnose what could be done better.  

 Optional arguments:
 -h        Print this message
 -v        Verbose output
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose);
if ($help) { print usage(); exit(); }

my $sqldb = WisCon::SQLEngine::sqldb(); 

my @times = WisCon::DateTime->times();
my @periods = @{ $sqldb->fetch_select(sql=>'select *,time(StartDateTime) as start,time(EndDateTime) as end from TimePeriods order by StartDateTime') };
for my $period (@periods) {
	$period->{flags} = [];
	my ($p_start,$p_end) = ($period->{start},$period->{end});
	$p_start =~ s/^(\d\d):/($1+24).":"/e if (($p_start cmp $tmin) < 0);
	$p_end =~ s/^(\d\d):/($1+24).":"/e if (($p_end cmp $tmin) < 0);
	for my $time (@times) {
		my ($t_start,$t_end) = ($time->{start},$time->{end});
		$t_start =~ s/^(\d\d):/($1+24).":"/e if (($t_start cmp $tmin) < 0);
		$t_end =~ s/^(\d\d):/($1+24).":"/e if (($t_end cmp $tmin) < 0);
		unless ( (($t_start cmp $p_end) >= 0) or (($t_end cmp $p_start) <= 0) ) {
			push(@{ $period->{flags} }, $time->{flag});
		}
	}
}

# Now report on the matchup
if ($verbose) {
	print "Hard-coded Times:\n";
	for my $time (@times) {
		my ($t_start,$t_end) = ($time->{start},$time->{end});
		$t_start =~ s/^(\d\d):/($1+24).":"/e if (($t_start cmp $tmin) < 0);
		$t_end =~ s/^(\d\d):/($1+24).":"/e if (($t_end cmp $tmin) < 0);
		print " - $time->{hours} => $t_start to $t_end\n";
	}
	print "\n";
}
my $nsuccess = 0;
for my $period (@periods) {
	my ($p_start,$p_end) = ($period->{start},$period->{end});
	$p_start =~ s/^(\d\d):/($1+24).":"/e if (($p_start cmp $tmin) < 0);
	$p_end =~ s/^(\d\d):/($1+24).":"/e if (($p_end cmp $tmin) < 0);
	my $type = ($period->{StandardTimeFlag}) ? "Standard TimePeriod" : "TimePeriod";
	my $matches = join(";",@{ $period->{flags} }) || "none";
	if ($verbose) {
		print "$type from $period->{StartDateTime} to $period->{EndDateTime} matches $matches\n";
	} elsif (not @{ $period->{flags} }) {
		print "No match for $type from $period->{StartDateTime} to $period->{EndDateTime}\n";
	}
	$nsuccess++ if (@{ $period->{flags} });
}
print "There are $nsuccess matched periods out of ".scalar(@periods)." TimePeriods in the database\n";
