#!/usr/bin/perl
use strict;  
use lib "../lib";
use Data::Dumper;
use WisCon::SQLEngine;
use WisCon::Person;
use WisCon::ProgramItem;
use FileHandle;
use Getopt::Long;
use Time::Local;
use strict;

# Set default options
my $help = undef;
my $verbose = undef;
my $person_id = undef;
my $user_id = undef;

sub usage {
    return <<"EndUsage";
 $0 <idPersons>

 This reports on person details.  

 Optional arguments:
 -h        Print this message
 -v        Verbose output
 -p=<id>   Specify a person ID
 -u=<id>   Specify a user ID
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose, 
	   "p=i" => \$person_id, "u=i" => \$user_id);
if ($help) { print usage(); exit(); }

my $sqldb = WisCon::SQLEngine::sqldb(); 

my $person;
if ($person_id) {
	$person = WisCon::Person->fetch($person_id);
} elsif ($user_id) {
	$person = WisCon::Person->find_user($user_id);
} else {
	die "No person/user specifier";
}

print "Name: ".$person->full_name."\n";
print "Person ID: ".$person->id."\n";
print "User ID: ".$person->user_id."\n" if ($person->user_id());

# Cache the names for each ranking code (i.e. 1=>Please, etc.)
my %rank_of_code;
my $ranks = $sqldb->fetch_select(table=>'RefRankingType');
for (@$ranks) { $rank_of_code{ $_->{Code} } = $_->{Description}; }

print "\nRankings:\n";
my $rankings = $sqldb->fetch_select(sql => ["select * from ItemRanking where idUsers=? order by UserInterestRanking,idItems",$person->user_id]);
for (@$rankings) {
	my $item = WisCon::ProgramItem->fetch(id=>$_->{idItems});
	my $open_slots = "";
	if ($item->is_auto_assigned and $item->open_slots) {
		$open_slots = "(open_slots=" . $item->open_slots . ")";
	}
	if (($_->{UserInterestRanking} < 9) or ($_->{ModeratorFlag})) {
		my $mod = ($_->{ModeratorFlag}) ? " (+mod)" : "";
		print " * " . $rank_of_code{ $_->{UserInterestRanking} } . "$mod : (Item" . $item->id . ") " . $item->name . " (status=" . $item->get_val('ItemStatus') . ") $open_slots\n";
	}
}

print "\nAssignments:\n";
my $assignments = $sqldb->fetch_select(sql => ["select * from Assignments where idPersons=?",$person->id]);
for (@$assignments) {
	my $item = WisCon::ProgramItem->fetch(id=>$_->{idItems});
	print " * $_->{AssignmentType},$_->{AssignmentStatus} (Item" . $item->id . ") " . $item->name . "\n";
}

