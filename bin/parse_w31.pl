#!/usr/bin/perl
use DateTime;
use DateTime::Duration;
use DateTime::Format::MySQL;
use DBIx::DWIW;
use FileHandle;
use Getopt::Long;
use Time::Local;
use strict;

# Set default options
my $help;
my $verbose;
my $testdate;
my $sqlout;
my $userhome = ($ENV{'HOME'} || $ENV{'LOGDIR'} || (getpwuid($>))[7]);
my $dir = "$userhome/wiscondb/local/secure/w31";
sub usage {
    return <<"EndUsage";
 $0

 This parses WisCon31 HTML data files and enters them into the new 
 WisCon database.  

 Optional arguments:
 -h        Print this message
 -v        Verbose output
 -dt=<s>   Test datetime parsing, with input like "Friday 8:00-12:00 a.m."
 -con      Only fill the Conventions table
 -sql      SQL output
 -dir=<s>  Sets the data file directory (default $dir)
EndUsage
}
GetOptions("h" => \$help, "help" => \$help, "v" => \$verbose, 
	   "dt=s" => \$testdate, "sql" => \$sqlout, "dir=s" => \$dir);
if ($help) { print usage(); exit(); }

if ($testdate) {
    $testdate =~ m/^(\w+day), (1?\d:\d\d [ap]\.m\.)-(1?\d:\d\d [ap]\.m\.)$/s or die "Bad timeslot '$testdate'";
    my ($day,$start_time,$end_time) = ($1,$2,$3);
    # convert into MySQL datetime format
    my $start = parsetime($day,$start_time);
    my $end = parsetime($day,$end_time);
    if ($end < $start) {
	$end += DateTime::Duration->new("days" => 1);
    }
    print "$start\n$end\n";
    exit();
}

my $infh = new FileHandle;
my $db;
my $idPersonsCount = 0;
if (not $sqlout) {
    $db = DBIx::DWIW->Connect(DB => "wiscon", User => "root", Pass => "");
}

sub db_do {
    my $cmd = shift;
    print "$cmd;\n" if ($sqlout or $verbose);
    $db->Do($cmd) if (not $sqlout);
}

sub read_file { 
    my $file = shift;
    open($infh,"$dir/$file") or die "Can't open '$file'";
    my $text = join('',<$infh>);
    close($infh);
    return $text;
}


db_do("insert into Conventions (idConventions,ConventionName,StartDate,EndDate) values (31,\"WisCon 31\",\"2008-05-22\",\"2008-05-26\"");

my $conid = 31;
if (not $sqlout) {
    ($conid) = $db->FlatArray("select idConventions from Conventions where ConventionName = \"WisCon 31\"");
}
die "No convention ID" unless ($conid);

my %id_of_fullname;

my $text = read_file("Short Bios.html");
while ($text =~ m|<p class='peoplename'>\s*(.*?)\s*</p>\s*<p class='peoplebio'>\s*(.*?)\s*</p>|gs) {
    my ($fullname,$shortbio) = ($1,$2);
    my ($firstname,$middlename,$lastname) = qw(_ _ _);
    if ($fullname =~ m/^([A-Za-z&;][A-Za-z\-\'&;]*\.?)\s+((Dal )?[A-Za-z][A-Za-z\-\'&;]+)\s*$/) {
	($firstname,$middlename,$lastname) = ($1,"",$2);
    } elsif ($fullname =~ m/^([A-Za-z&][A-Za-z\-\'&;]*\.?)\s+([A-Za-z&][A-Za-z\-\'&;]*\.?)\s+((Dal )?[A-Za-z&][A-Za-z\-\'&;]+)\s*$/) {
	($firstname,$middlename,$lastname) = ($1,$2,$3);
    } elsif ($fullname =~ m/^([A-Za-z]\.)([A-Za-z]\.)\s+([A-Za-z&][A-Za-z\-\'&;]+)\s*$/) {
	($firstname,$middlename,$lastname) = ($1,$2,$3);
    } elsif ($fullname =~ m/^(.*)\|(.*)\|(.*)$/) {
	($firstname,$middlename,$lastname) = ($1,$2,$3);
    }
    die "Quotes in name!" if ($fullname =~ m/\"/s);
    die "Quotes in bio!" if ($shortbio =~ m/\"/s);
    my $sql = "insert ignore into Persons (FirstName,MiddleName,LastName,ShortBio) values (\"$firstname\",\"$middlename\",\"$lastname\",\"$shortbio\")";
    db_do($sql);

    my $id;
    if ($sqlout) {
	$id = ++$idPersonsCount;
    } else {
	($id) = $db->FlatArray("select idPersons from persons where FirstName=\"$firstname\" and MiddleName=\"$middlename\" and LastName=\"$lastname\"");
    }
    die "No id" unless ($id);
    
    $id_of_fullname{$fullname} = $id;
}

########################################################################

my $text = read_file("WisCon 31 Program Schedule.html");
my @blocks;
while ($text =~ m|((<p class='\w+'>.*?</p>\s*)+)<br>|gs) {
    push(@blocks,$1);
}
print "Found ".scalar(@blocks)." item blocks\n";
for my $block (@blocks) {
    my %info;
    while ($block =~ m|<p class='(\w+)'>\s*(.*?)\s*</p>|gs) { $info{$1}=$2; }
    for (keys %info) { die "Quotes in $_=$info{$_}" if ($info{$_} =~ m/\"/); }
    my ($number,$name,$desc,$category,$room,$timeslot);
    my @panelists;
    for (keys %info) {
	if (m/^panelname$/) {
	    $info{$_} =~ m/^((\d+)&\#09;\s*)?(\w.*)$/s or die "Bad $_ $info{$_}";
	    ($number,$name) = ($1,$2);
	} elsif (m/^CategoryRoom$/) {
	    $info{$_} =~ m/^(.*)&bull;(.*)&bull;\s+(.*)/s or die "Bad $_ $info{$_}, name part";
	    ($category,$room,$timeslot) = ($1,$2,$3);
	} elsif (m/^dayname$/) {
	    # ignore - this is just for display
	} elsif (m/^daytimestring$/) {
	    $timeslot = $info{$_};
	} elsif (m/^paneldescribe$/) {
	    $desc = $info{$_};
	} elsif (m/^panelists$/) {
	    @panelists = split(/\s*,\s*/,$info{$_});
	    for (@panelists) {
		s/^M: //;
		$id_of_fullname{$_} or die "Bad panelist name '$_'";
	    }
	} else {
	    die "Unknown schedule info type '$_'";
	}
    }
    if (defined($timeslot)) {
	$timeslot =~ s/a\.m$/a.m./;
	$timeslot =~ s/(1?\d:\d\d)\s*([ap])(m)\s*/$1 $2.$3./g;
	$timeslot =~ s/Midnight/12:00 a.m./;
	# ignore spaces around dash
	$timeslot =~ s/\s*-\s+/-/;
	# 3a.m. -> 3:00 a.m.
	$timeslot =~ s/([\- ])(\d)\s*([ap]\.m\.)/$1$2:00 $3/;
	# 
	$timeslot =~ s/(1?\d:\d\d)-(1?\d:\d\d) ([ap]\.m\.)$/$1 $3-$2 $3/s;

	$timeslot =~ m/^(\w+day), (1?\d:\d\d [ap]\.m\.)-(1?\d:\d\d [ap]\.m\.)$/s or die "Bad timeslot $timeslot";
	my ($day,$start_time,$end_time) = ($1,$2,$3);
	# convert into MySQL datetime format
	my $start = parsetime($day,$start_time);
	my $end = parsetime($day,$end_time);
	if ($end < $start) {
	    $end += DateTime::Duration->new("days" => 1);
	}
	my $sql = "insert ignore into TimePeriods (StartDateTime,EndDateTime,idConventions,CreateUser,CreateDate) values (\"$start\",\"$end\",$conid,\"$user\",now())";
	db_do($sql);
    }
    if ($room) {
	my $sql = "insert ignore into Locations (LocationName,LocationType) values (\"$room\",\"\")"
    }

    my $sql = "insert into items (ItemName,ItemDesc) values (\"$name\",\"$desc\")";
    db_do($sql);
}

exit();

########################################################################

open($infh,"Grid text 07 05 07.html") or die;
close($infh);

########################################################################

open($infh,"Panelist Index.html") or die;
close($infh);

########################################################################

open($infh,"WisCon 31 Program Panelist Tent Cards.html") or die;
close($infh);

########################################################################

open($infh,"WisCon 31 Program Room Cards.html") or die;
close($infh);

########################################################################

sub parsetime {
    my $wday = shift;
    my $time = shift;
    my %mday_of_wday = ("Thursday" => 22, "Friday" => 23, "Saturday" => 24,
			"Sunday" => 25, "Monday" => 26);
    my $mday = $mday_of_wday{$wday} or die "Bad weekday '$wday'";

    $time =~ m/^(1?\d):(\d\d) ([ap])\.m\.$/ or die "Bad time '$time'";
    my ($hr,$mn,$ap) = ($1,$2,$3);
    $hr += 12 if (($ap eq "p") and ($hr != 12));
    $hr -= 12 if (($ap eq "a") and ($hr == 12));
    my $dt = DateTime->new( year   => 2008, month  => 5, day    => 16, 
			    hour   => $hr,  minute => $mn );
    my $format = DateTime::Format::MySQL->new();
    $dt->set_formatter($format);
    return $dt;
}
